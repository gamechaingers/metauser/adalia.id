#
# This file is autogenerated by pip-compile with python 3.9
# To update, run:
#
#    pip-compile --output-file=requirements-test.txt requirements-test.in requirements.txt
#
aiohttp==3.8.1
    # via
    #   -c requirements.txt
    #   -r requirements.txt
    #   web3
aiosignal==1.2.0
    # via
    #   -c requirements.txt
    #   -r requirements.txt
    #   aiohttp
amqp==5.1.0
    # via
    #   -c requirements.txt
    #   -r requirements.txt
    #   kombu
appdirs==1.4.4
    # via
    #   -c requirements.txt
    #   -r requirements.txt
    #   requests-cache
asgiref==3.5.0
    # via
    #   -c requirements.txt
    #   -r requirements.txt
    #   django
async-timeout==4.0.2
    # via
    #   -c requirements.txt
    #   -r requirements.txt
    #   aiohttp
attrs==21.4.0
    # via
    #   -c requirements.txt
    #   -r requirements.txt
    #   aiohttp
    #   cattrs
    #   jsonschema
    #   pytest
    #   requests-cache
authlib==1.0.1
    # via
    #   -c requirements.txt
    #   -r requirements.txt
base58==2.1.1
    # via
    #   -c requirements.txt
    #   -r requirements.txt
    #   multiaddr
beautifulsoup4==4.11.1
    # via
    #   -c requirements.txt
    #   -r requirements.txt
    #   django-bootstrap4
billiard==3.6.4.0
    # via
    #   -c requirements.txt
    #   -r requirements.txt
    #   celery
bitarray==1.2.2
    # via
    #   -c requirements.txt
    #   -r requirements.txt
    #   eth-account
bleach==5.0.0
    # via
    #   -c requirements.txt
    #   -r requirements.txt
bounded-pool-executor==0.0.3
    # via
    #   -c requirements.txt
    #   -r requirements.txt
    #   pqdm
cattrs==1.10.0
    # via
    #   -c requirements.txt
    #   -r requirements.txt
    #   requests-cache
celery==5.2.6
    # via
    #   -c requirements.txt
    #   -r requirements.txt
    #   celery-once
    #   django-celery-beat
celery-once==3.0.1
    # via
    #   -c requirements.txt
    #   -r requirements.txt
certifi==2021.10.8
    # via
    #   -c requirements.txt
    #   -r requirements.txt
    #   requests
    #   sentry-sdk
cffi==1.15.0
    # via
    #   -c requirements.txt
    #   -r requirements.txt
    #   cryptography
charset-normalizer==2.0.12
    # via
    #   -c requirements.txt
    #   -r requirements.txt
    #   aiohttp
    #   requests
click==8.1.2
    # via
    #   -c requirements.txt
    #   -r requirements.txt
    #   celery
    #   click-didyoumean
    #   click-plugins
    #   click-repl
    #   itertable
    #   pip-tools
click-didyoumean==0.3.0
    # via
    #   -c requirements.txt
    #   -r requirements.txt
    #   celery
click-plugins==1.1.1
    # via
    #   -c requirements.txt
    #   -r requirements.txt
    #   celery
click-repl==0.2.0
    # via
    #   -c requirements.txt
    #   -r requirements.txt
    #   celery
colorlog==6.6.0
    # via
    #   -c requirements.txt
    #   -r requirements.txt
coverage[toml]==6.3.2
    # via pytest-cov
cryptography==36.0.2
    # via
    #   -c requirements.txt
    #   -r requirements.txt
    #   authlib
    #   jwcrypto
    #   pyjwt
cytoolz==0.11.2
    # via
    #   -c requirements.txt
    #   -r requirements.txt
    #   eth-keyfile
    #   eth-utils
data-wizard==1.3.0
    # via
    #   -c requirements.txt
    #   -r requirements.txt
ddtrace==0.46.0
    # via
    #   -c requirements.txt
    #   -r requirements.txt
    #   opentelemetry-exporter-datadog
defusedxml==0.7.1
    # via
    #   -c requirements.txt
    #   -r requirements.txt
    #   python3-openid
deprecated==1.2.13
    # via
    #   -c requirements.txt
    #   -r requirements.txt
    #   jwcrypto
    #   opentelemetry-api
distlib==0.3.4
    # via
    #   -c requirements.txt
    #   -r requirements.txt
    #   virtualenv
dj-database-url==0.5.0
    # via
    #   -c requirements.txt
    #   -r requirements.txt
django==3.2.13
    # via
    #   -c requirements.txt
    #   -r requirements.txt
    #   django-activeurl
    #   django-allauth
    #   django-allauth-2fa
    #   django-anymail
    #   django-appconf
    #   django-bootstrap4
    #   django-cacheops
    #   django-celery-beat
    #   django-ckeditor
    #   django-classy-tags
    #   django-extensions
    #   django-filter
    #   django-js-asset
    #   django-js-reverse
    #   django-model-utils
    #   django-notifications-hq
    #   django-oauth-toolkit
    #   django-organizations
    #   django-otp
    #   django-polymorphic
    #   django-pwa
    #   django-rest-polymorphic
    #   django-timezone-field
    #   djangorestframework
    #   drf-jwt
    #   drf-spectacular
    #   drf-spectacular-sidecar
    #   jsonfield
django-activeurl==0.2.0
    # via
    #   -c requirements.txt
    #   -r requirements.txt
django-allauth==0.50.0
    # via
    #   -c requirements.txt
    #   -r requirements.txt
    #   django-allauth-2fa
django-allauth-2fa==0.9
    # via
    #   -c requirements.txt
    #   -r requirements.txt
django-anymail[mailgun]==8.5
    # via
    #   -c requirements.txt
    #   -r requirements.txt
django-appconf==1.0.5
    # via
    #   -c requirements.txt
    #   -r requirements.txt
    #   django-activeurl
django-autoslug==1.9.8
    # via
    #   -c requirements.txt
    #   -r requirements.txt
django-bootstrap4==22.1
    # via
    #   -c requirements.txt
    #   -r requirements.txt
django-cacheops==6.0
    # via
    #   -c requirements.txt
    #   -r requirements.txt
django-celery-beat==2.2.1
    # via
    #   -c requirements.txt
    #   -r requirements.txt
django-ckeditor==6.4.0
    # via
    #   -c requirements.txt
    #   -r requirements.txt
django-classy-tags==3.0.1
    # via
    #   -c requirements.txt
    #   -r requirements.txt
    #   django-activeurl
django-crispy-forms==1.14.0
    # via
    #   -c requirements.txt
    #   -r requirements.txt
django-extensions==3.1.5
    # via
    #   -c requirements.txt
    #   -r requirements.txt
django-filter==21.1
    # via
    #   -c requirements.txt
    #   -r requirements.txt
django-invitations==1.9.3
    # via
    #   -c requirements.txt
    #   -r requirements.txt
django-js-asset==2.0.0
    # via
    #   -c requirements.txt
    #   -r requirements.txt
    #   django-ckeditor
django-js-reverse==0.9.1
    # via
    #   -c requirements.txt
    #   -r requirements.txt
django-model-utils==4.2.0
    # via
    #   -c requirements.txt
    #   -r requirements.txt
    #   django-notifications-hq
django-notifications-hq==1.7.0
    # via
    #   -c requirements.txt
    #   -r requirements.txt
django-oauth-toolkit==1.7.1
    # via
    #   -c requirements.txt
    #   -r requirements.txt
django-organizations==2.0.2
    # via
    #   -c requirements.txt
    #   -r requirements.txt
django-otp==1.1.3
    # via
    #   -c requirements.txt
    #   -r requirements.txt
    #   django-allauth-2fa
django-pipeline==2.0.8
    # via
    #   -c requirements.txt
    #   -r requirements.txt
django-polymorphic==3.1.0
    # via
    #   -c requirements.txt
    #   -r requirements.txt
    #   django-rest-polymorphic
django-pwa==1.0.10
    # via
    #   -c requirements.txt
    #   -r requirements.txt
django-redis-cache==3.0.1
    # via
    #   -c requirements.txt
    #   -r requirements.txt
django-rest-polymorphic==0.1.9
    # via
    #   -c requirements.txt
    #   -r requirements.txt
django-split-settings==1.1.0
    # via
    #   -c requirements.txt
    #   -r requirements.txt
django-tag-parser==3.2
    # via
    #   -c requirements.txt
    #   -r requirements.txt
django-timezone-field==4.2.3
    # via
    #   -c requirements.txt
    #   -r requirements.txt
    #   django-celery-beat
djangorestframework==3.13.1
    # via
    #   -c requirements.txt
    #   -r requirements.txt
    #   data-wizard
    #   django-rest-polymorphic
    #   djangorestframework-csv
    #   djangorestframework-datatables
    #   drf-jwt
    #   drf-spectacular
djangorestframework-csv==2.1.1
    # via
    #   -c requirements.txt
    #   -r requirements.txt
djangorestframework-datatables==0.7.0
    # via
    #   -c requirements.txt
    #   -r requirements.txt
drf-jwt==1.19.2
    # via
    #   -c requirements.txt
    #   -r requirements.txt
drf-spectacular[sidecar]==0.22.0
    # via
    #   -c requirements.txt
    #   -r requirements.txt
drf-spectacular-sidecar==2022.4.1
    # via
    #   -c requirements.txt
    #   -r requirements.txt
    #   drf-spectacular
eth-abi==2.1.1
    # via
    #   -c requirements.txt
    #   -r requirements.txt
    #   eth-account
    #   web3
eth-account==0.5.7
    # via
    #   -c requirements.txt
    #   -r requirements.txt
    #   web3
eth-hash[pycryptodome]==0.3.2
    # via
    #   -c requirements.txt
    #   -r requirements.txt
    #   eth-utils
    #   web3
eth-keyfile==0.5.1
    # via
    #   -c requirements.txt
    #   -r requirements.txt
    #   eth-account
eth-keys==0.3.4
    # via
    #   -c requirements.txt
    #   -r requirements.txt
    #   eth-account
    #   eth-keyfile
eth-rlp==0.2.1
    # via
    #   -c requirements.txt
    #   -r requirements.txt
    #   eth-account
eth-typing==2.3.0
    # via
    #   -c requirements.txt
    #   -r requirements.txt
    #   eth-abi
    #   eth-keys
    #   eth-utils
    #   web3
eth-utils==1.10.0
    # via
    #   -c requirements.txt
    #   -r requirements.txt
    #   eth-abi
    #   eth-account
    #   eth-keyfile
    #   eth-keys
    #   eth-rlp
    #   rlp
    #   web3
execnet==1.9.0
    # via pytest-xdist
filelock==3.6.0
    # via
    #   -c requirements.txt
    #   -r requirements.txt
    #   tox
    #   virtualenv
frozenlist==1.3.0
    # via
    #   -c requirements.txt
    #   -r requirements.txt
    #   aiohttp
    #   aiosignal
funcy==1.17
    # via
    #   -c requirements.txt
    #   -r requirements.txt
    #   django-cacheops
gevent==21.12.0
    # via
    #   -c requirements.txt
    #   -r requirements.txt
gitdb==4.0.9
    # via gitpython
gitpython==3.1.27
    # via -r requirements-test.in
googleapis-common-protos==1.56.0
    # via
    #   -c requirements.txt
    #   -r requirements.txt
    #   opentelemetry-exporter-jaeger-proto-grpc
greenlet==1.1.2
    # via
    #   -c requirements.txt
    #   -r requirements.txt
    #   gevent
grpcio==1.44.0
    # via
    #   -c requirements.txt
    #   -r requirements.txt
    #   opentelemetry-exporter-jaeger-proto-grpc
gunicorn==20.1.0
    # via
    #   -c requirements.txt
    #   -r requirements.txt
hexbytes==0.2.2
    # via
    #   -c requirements.txt
    #   -r requirements.txt
    #   eth-account
    #   eth-rlp
    #   web3
html-json-forms==1.1.1
    # via
    #   -c requirements.txt
    #   -r requirements.txt
    #   data-wizard
    #   natural-keys
idna==3.3
    # via
    #   -c requirements.txt
    #   -r requirements.txt
    #   requests
    #   yarl
importlib-metadata==4.11.3
    # via
    #   -c requirements.txt
    #   -r requirements.txt
    #   markdown
inflection==0.5.1
    # via
    #   -c requirements.txt
    #   -r requirements.txt
    #   drf-spectacular
influence-api==0.1.1
    # via
    #   -c requirements.txt
    #   -r requirements.txt
iniconfig==1.1.1
    # via pytest
intervaltree==3.1.0
    # via
    #   -c requirements.txt
    #   -r requirements.txt
    #   ddtrace
ipfshttpclient==0.8.0a2
    # via
    #   -c requirements.txt
    #   -r requirements.txt
    #   web3
itertable==2.0.0
    # via
    #   -c requirements.txt
    #   -r requirements.txt
    #   data-wizard
jsonfield==3.1.0
    # via
    #   -c requirements.txt
    #   -r requirements.txt
    #   django-notifications-hq
jsonschema==4.4.0
    # via
    #   -c requirements.txt
    #   -r requirements.txt
    #   drf-spectacular
    #   web3
jwcrypto==1.0
    # via
    #   -c requirements.txt
    #   -r requirements.txt
    #   django-oauth-toolkit
kombu==5.2.4
    # via
    #   -c requirements.txt
    #   -r requirements.txt
    #   celery
libgravatar==1.0.0
    # via
    #   -c requirements.txt
    #   -r requirements.txt
lru-dict==1.1.7
    # via
    #   -c requirements.txt
    #   -r requirements.txt
    #   web3
lxml==4.8.0
    # via
    #   -c requirements.txt
    #   -r requirements.txt
    #   django-activeurl
markdown==3.3.6
    # via
    #   -c requirements.txt
    #   -r requirements.txt
multiaddr==0.0.9
    # via
    #   -c requirements.txt
    #   -r requirements.txt
    #   ipfshttpclient
multidict==6.0.2
    # via
    #   -c requirements.txt
    #   -r requirements.txt
    #   aiohttp
    #   yarl
mypy-extensions==0.4.3
    # via pyannotate
natural-keys==2.0.0
    # via
    #   -c requirements.txt
    #   -r requirements.txt
    #   data-wizard
netaddr==0.8.0
    # via
    #   -c requirements.txt
    #   -r requirements.txt
    #   multiaddr
oauthlib==3.2.0
    # via
    #   -c requirements.txt
    #   -r requirements.txt
    #   django-oauth-toolkit
    #   requests-oauthlib
opensea-api==0.1.7
    # via
    #   -c requirements.txt
    #   -r requirements.txt
opentelemetry-api==1.10.0
    # via
    #   -c requirements.txt
    #   -r requirements.txt
    #   opentelemetry-exporter-datadog
    #   opentelemetry-exporter-jaeger-proto-grpc
    #   opentelemetry-exporter-jaeger-thrift
    #   opentelemetry-instrumentation
    #   opentelemetry-instrumentation-celery
    #   opentelemetry-instrumentation-django
    #   opentelemetry-instrumentation-requests
    #   opentelemetry-instrumentation-wsgi
    #   opentelemetry-sdk
opentelemetry-exporter-datadog==0.29b0
    # via
    #   -c requirements.txt
    #   -r requirements.txt
opentelemetry-exporter-jaeger==1.10.0
    # via
    #   -c requirements.txt
    #   -r requirements.txt
opentelemetry-exporter-jaeger-proto-grpc==1.10.0
    # via
    #   -c requirements.txt
    #   -r requirements.txt
    #   opentelemetry-exporter-jaeger
opentelemetry-exporter-jaeger-thrift==1.10.0
    # via
    #   -c requirements.txt
    #   -r requirements.txt
    #   opentelemetry-exporter-jaeger
opentelemetry-instrumentation==0.29b0
    # via
    #   -c requirements.txt
    #   -r requirements.txt
    #   opentelemetry-instrumentation-celery
    #   opentelemetry-instrumentation-django
    #   opentelemetry-instrumentation-requests
    #   opentelemetry-instrumentation-wsgi
opentelemetry-instrumentation-celery==0.29b0
    # via
    #   -c requirements.txt
    #   -r requirements.txt
opentelemetry-instrumentation-django==0.29b0
    # via
    #   -c requirements.txt
    #   -r requirements.txt
opentelemetry-instrumentation-requests==0.29b0
    # via
    #   -c requirements.txt
    #   -r requirements.txt
opentelemetry-instrumentation-wsgi==0.29b0
    # via
    #   -c requirements.txt
    #   -r requirements.txt
    #   opentelemetry-instrumentation-django
opentelemetry-sdk==1.10.0
    # via
    #   -c requirements.txt
    #   -r requirements.txt
    #   opentelemetry-exporter-datadog
    #   opentelemetry-exporter-jaeger-proto-grpc
    #   opentelemetry-exporter-jaeger-thrift
opentelemetry-semantic-conventions==0.29b0
    # via
    #   -c requirements.txt
    #   -r requirements.txt
    #   opentelemetry-exporter-datadog
    #   opentelemetry-instrumentation-celery
    #   opentelemetry-instrumentation-django
    #   opentelemetry-instrumentation-requests
    #   opentelemetry-instrumentation-wsgi
    #   opentelemetry-sdk
opentelemetry-util-http==0.29b0
    # via
    #   -c requirements.txt
    #   -r requirements.txt
    #   opentelemetry-instrumentation-django
    #   opentelemetry-instrumentation-requests
    #   opentelemetry-instrumentation-wsgi
packaging==21.3
    # via
    #   -c requirements.txt
    #   -r requirements.txt
    #   pytest
    #   tox
parsimonious==0.8.1
    # via
    #   -c requirements.txt
    #   -r requirements.txt
    #   eth-abi
pep517==0.12.0
    # via
    #   -c requirements.txt
    #   -r requirements.txt
    #   pip-tools
pip-tools==6.6.0
    # via
    #   -c requirements.txt
    #   -r requirements.txt
platformdirs==2.5.1
    # via
    #   -c requirements.txt
    #   -r requirements.txt
    #   virtualenv
pluggy==1.0.0
    # via
    #   -c requirements.txt
    #   -r requirements.txt
    #   pytest
    #   tox
pqdm==0.2.0
    # via
    #   -c requirements.txt
    #   -r requirements.txt
prompt-toolkit==3.0.29
    # via
    #   -c requirements.txt
    #   -r requirements.txt
    #   click-repl
protobuf==3.20.0
    # via
    #   -c requirements.txt
    #   -r requirements.txt
    #   ddtrace
    #   googleapis-common-protos
    #   web3
psycopg2-binary==2.9.3
    # via
    #   -c requirements.txt
    #   -r requirements.txt
py==1.11.0
    # via
    #   -c requirements.txt
    #   -r requirements.txt
    #   pytest
    #   pytest-forked
    #   tox
pyannotate==1.2.0
    # via pytest-annotate
pycoingecko==2.2.0
    # via
    #   -c requirements.txt
    #   -r requirements.txt
pycparser==2.21
    # via
    #   -c requirements.txt
    #   -r requirements.txt
    #   cffi
pycryptodome==3.14.1
    # via
    #   -c requirements.txt
    #   -r requirements.txt
    #   eth-hash
    #   eth-keyfile
pyjwt[crypto]==2.3.0
    # via
    #   -c requirements.txt
    #   -r requirements.txt
    #   django-allauth
    #   drf-jwt
pyparsing==3.0.8
    # via
    #   -c requirements.txt
    #   -r requirements.txt
    #   packaging
pyrsistent==0.18.1
    # via
    #   -c requirements.txt
    #   -r requirements.txt
    #   jsonschema
pysha3==1.0.2
    # via
    #   -c requirements.txt
    #   -r requirements.txt
pytest==6.2.5
    # via
    #   -r requirements-test.in
    #   pytest-annotate
    #   pytest-cov
    #   pytest-django
    #   pytest-env
    #   pytest-forked
    #   pytest-xdist
pytest-annotate==1.0.4
    # via -r requirements-test.in
pytest-cov==3.0.0
    # via -r requirements-test.in
pytest-django==4.5.2
    # via -r requirements-test.in
pytest-env==0.6.2
    # via -r requirements-test.in
pytest-forked==1.4.0
    # via pytest-xdist
pytest-xdist==2.5.0
    # via -r requirements-test.in
python-crontab==2.6.0
    # via
    #   -c requirements.txt
    #   -r requirements.txt
    #   django-celery-beat
python-dateutil==2.8.2
    # via
    #   -c requirements.txt
    #   -r requirements.txt
    #   data-wizard
    #   python-crontab
python3-openid==3.2.0
    # via
    #   -c requirements.txt
    #   -r requirements.txt
    #   django-allauth
pytz==2022.1
    # via
    #   -c requirements.txt
    #   -r requirements.txt
    #   celery
    #   django
    #   django-notifications-hq
    #   django-timezone-field
    #   djangorestframework
    #   djangorestframework-datatables
pyyaml==6.0
    # via
    #   -c requirements.txt
    #   -r requirements.txt
    #   drf-spectacular
qrcode==7.3.1
    # via
    #   -c requirements.txt
    #   -r requirements.txt
    #   django-allauth-2fa
ratelimit==2.2.1
    # via
    #   -c requirements.txt
    #   -r requirements.txt
redis==3.5.3
    # via
    #   -c requirements.txt
    #   -r requirements.txt
    #   celery-once
    #   django-cacheops
    #   django-redis-cache
requests==2.27.1
    # via
    #   -c requirements.txt
    #   -r requirements.txt
    #   django-allauth
    #   django-anymail
    #   django-oauth-toolkit
    #   influence-api
    #   ipfshttpclient
    #   itertable
    #   opensea-api
    #   pycoingecko
    #   requests-cache
    #   requests-mock
    #   requests-oauthlib
    #   web3
requests-cache==0.9.3
    # via
    #   -c requirements.txt
    #   -r requirements.txt
    #   influence-api
requests-mock==1.9.3
    # via -r requirements-test.in
requests-oauthlib==1.3.1
    # via
    #   -c requirements.txt
    #   -r requirements.txt
    #   django-allauth
    #   influence-api
rlp==2.0.1
    # via
    #   -c requirements.txt
    #   -r requirements.txt
    #   eth-account
    #   eth-rlp
sentry-sdk==1.5.10
    # via
    #   -c requirements.txt
    #   -r requirements.txt
six==1.16.0
    # via
    #   -c requirements.txt
    #   -r requirements.txt
    #   bleach
    #   click-repl
    #   django-cacheops
    #   django-organizations
    #   django-rest-polymorphic
    #   djangorestframework-csv
    #   grpcio
    #   multiaddr
    #   parsimonious
    #   pyannotate
    #   python-dateutil
    #   requests-mock
    #   thrift
    #   tox
    #   url-normalize
    #   virtualenv
smmap==5.0.0
    # via gitdb
sortedcontainers==2.4.0
    # via
    #   -c requirements.txt
    #   -r requirements.txt
    #   intervaltree
soupsieve==2.3.2.post1
    # via
    #   -c requirements.txt
    #   -r requirements.txt
    #   beautifulsoup4
sqlparse==0.4.2
    # via
    #   -c requirements.txt
    #   -r requirements.txt
    #   django
swapper==1.3.0
    # via
    #   -c requirements.txt
    #   -r requirements.txt
    #   django-notifications-hq
tenacity==8.0.1
    # via
    #   -c requirements.txt
    #   -r requirements.txt
    #   ddtrace
thrift==0.16.0
    # via
    #   -c requirements.txt
    #   -r requirements.txt
    #   opentelemetry-exporter-jaeger-thrift
toml==0.10.2
    # via
    #   -c requirements.txt
    #   -r requirements.txt
    #   pytest
    #   tox
tomli==2.0.1
    # via
    #   -c requirements.txt
    #   -r requirements.txt
    #   coverage
    #   pep517
toolz==0.11.2
    # via
    #   -c requirements.txt
    #   -r requirements.txt
    #   cytoolz
tox==3.25.0
    # via
    #   -c requirements.txt
    #   -r requirements.txt
tqdm==4.64.0
    # via
    #   -c requirements.txt
    #   -r requirements.txt
    #   pqdm
typing-extensions==4.1.1
    # via
    #   -c requirements.txt
    #   -r requirements.txt
    #   opentelemetry-sdk
    #   pqdm
unicodecsv==0.14.1
    # via
    #   -c requirements.txt
    #   -r requirements.txt
    #   djangorestframework-csv
uritemplate==4.1.1
    # via
    #   -c requirements.txt
    #   -r requirements.txt
    #   drf-spectacular
url-normalize==1.4.3
    # via
    #   -c requirements.txt
    #   -r requirements.txt
    #   requests-cache
urllib3==1.26.9
    # via
    #   -c requirements.txt
    #   -r requirements.txt
    #   requests
    #   requests-cache
    #   sentry-sdk
varint==1.0.2
    # via
    #   -c requirements.txt
    #   -r requirements.txt
    #   multiaddr
vine==5.0.0
    # via
    #   -c requirements.txt
    #   -r requirements.txt
    #   amqp
    #   celery
    #   kombu
virtualenv==20.14.1
    # via
    #   -c requirements.txt
    #   -r requirements.txt
    #   tox
waitress==2.1.1
    # via
    #   -c requirements.txt
    #   -r requirements.txt
wcwidth==0.2.5
    # via
    #   -c requirements.txt
    #   -r requirements.txt
    #   prompt-toolkit
web3==5.29.0
    # via
    #   -c requirements.txt
    #   -r requirements.txt
webencodings==0.5.1
    # via
    #   -c requirements.txt
    #   -r requirements.txt
    #   bleach
websockets==9.1
    # via
    #   -c requirements.txt
    #   -r requirements.txt
    #   web3
wheel==0.37.1
    # via
    #   -c requirements.txt
    #   -r requirements.txt
    #   pip-tools
whitenoise==6.0.0
    # via
    #   -c requirements.txt
    #   -r requirements.txt
wrapt==1.14.0
    # via
    #   -c requirements.txt
    #   -r requirements.txt
    #   deprecated
    #   opentelemetry-instrumentation
xlrd==2.0.1
    # via
    #   -c requirements.txt
    #   -r requirements.txt
    #   itertable
yarl==1.7.2
    # via
    #   -c requirements.txt
    #   -r requirements.txt
    #   aiohttp
zipp==3.8.0
    # via
    #   -c requirements.txt
    #   -r requirements.txt
    #   importlib-metadata
zope-event==4.5.0
    # via
    #   -c requirements.txt
    #   -r requirements.txt
    #   gevent
zope-interface==5.4.0
    # via
    #   -c requirements.txt
    #   -r requirements.txt
    #   gevent

# The following packages are considered to be unsafe in a requirements file:
# pip
# setuptools
