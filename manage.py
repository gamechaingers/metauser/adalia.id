#!/usr/bin/env python
"""Django's command-line utility for administrative tasks."""
import os
import sys
from pathlib import Path

MANAGE_PY = Path(__file__).resolve()


def main():
    """Run administrative tasks."""
    os.environ.setdefault("DJANGO_SETTINGS_MODULE", "server.settings.local")

    try:
        from django.core.management import execute_from_command_line
    except ImportError as exc:
        raise ImportError(
            "Couldn't import Django. Are you sure it's installed and "
            "available on your PYTHONPATH environment variable? Did you "
            "forget to activate a virtual environment?"
        ) from exc
    execute_from_command_line(sys.argv)


if __name__ == "__main__":

    from importlib import import_module

    try:
        import_module("metauser.common.apps")

    except ImportError as exc:
        print(exc)
        import subprocess

        subprocess.run(["pip", "install", "-e", "./metauser"])
        argv = sys.argv
        argv[0] = str(MANAGE_PY)
        subprocess.run(sys.argv)
        exit(0)

    main()
