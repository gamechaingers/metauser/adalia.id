import {
  generateSchemaTypes,
  generateReactQueryComponents,
} from '@openapi-codegen/typescript'
import { defineConfig } from '@openapi-codegen/cli'
export default defineConfig({
  adaliaId: {
    from: {
      relativePath: './api-schema.yaml',
      source: 'file',
    },
    outputDir: 'src/api',
    to: async (context) => {
      const filenamePrefix = 'adaliaId'
      const { schemasFiles } = await generateSchemaTypes(context, {
        filenamePrefix,
      })
      await generateReactQueryComponents(context, {
        filenamePrefix,
        schemasFiles,
      })
    },
  },
})
