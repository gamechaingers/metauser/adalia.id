openapi: 3.0.3
info:
  title: adalia.id API
  version: 1.0.0
  description: The premier alliance management platform
paths:
  /api/:
    get:
      operationId: root_retrieve
      parameters:
      - in: query
        name: format
        schema:
          type: string
          enum:
          - csv
          - datatables
          - json
      security:
      - jwtAuth: []
      - cookieAuth: []
      - basicAuth: []
      - {}
      responses:
        '200':
          description: No response body
  /api/adalia_id/asteroids/:
    get:
      operationId: adalia_id_asteroids_list
      parameters:
      - in: query
        name: alliance_controlled
        schema:
          type: boolean
      - in: query
        name: anomaly_max
        schema:
          type: number
          format: double
          maximum: 1000
          minimum: -1000
          exclusiveMaximum: true
          exclusiveMinimum: true
          nullable: true
          title: Mean anomaly at epoch
      - in: query
        name: anomaly_min
        schema:
          type: number
          format: double
          maximum: 1000
          minimum: -1000
          exclusiveMaximum: true
          exclusiveMinimum: true
          nullable: true
          title: Mean anomaly at epoch
      - in: query
        name: ascending_node_max
        schema:
          type: number
          format: double
          maximum: 1000
          minimum: -1000
          exclusiveMaximum: true
          exclusiveMinimum: true
          nullable: true
      - in: query
        name: ascending_node_min
        schema:
          type: number
          format: double
          maximum: 1000
          minimum: -1000
          exclusiveMaximum: true
          exclusiveMinimum: true
          nullable: true
      - in: query
        name: current_owner
        schema:
          type: string
      - in: query
        name: diameter_max
        schema:
          type: integer
          maximum: 750284
          minimum: 2046
          nullable: true
      - in: query
        name: diameter_min
        schema:
          type: integer
          maximum: 750284
          minimum: 2046
          nullable: true
      - in: query
        name: eccentricity_max
        schema:
          type: number
          format: double
          maximum: 0.392
          minimum: 0.007
          nullable: true
      - in: query
        name: eccentricity_min
        schema:
          type: number
          format: double
          maximum: 0.392
          minimum: 0.007
          nullable: true
      - in: query
        name: format
        schema:
          type: string
          enum:
          - csv
          - datatables
          - json
      - in: query
        name: inclination_max
        schema:
          type: number
          format: double
          maximum: 37.77
          minimum: 0.01
          nullable: true
      - in: query
        name: inclination_min
        schema:
          type: number
          format: double
          maximum: 37.77
          minimum: 0.01
          nullable: true
      - name: limit
        required: false
        in: query
        description: Number of results to return per page.
        schema:
          type: integer
      - in: query
        name: name
        schema:
          type: string
      - name: offset
        required: false
        in: query
        description: The initial index from which to return the results.
        schema:
          type: integer
      - in: query
        name: orbital_period_max
        schema:
          type: number
          format: double
          maximum: 10000
          minimum: 0
          exclusiveMaximum: true
          nullable: true
          title: Period
      - in: query
        name: orbital_period_min
        schema:
          type: number
          format: double
          maximum: 10000
          minimum: 0
          exclusiveMaximum: true
          nullable: true
          title: Period
      - in: query
        name: ordering
        schema:
          type: array
          items:
            type: string
            enum:
            - -anomaly
            - -ascending_node
            - -current_owner
            - -diameter
            - -eccentricity
            - -inclination
            - -name
            - -orbital_period
            - -periapsis
            - -semi_major_axis
            - -serial
            - anomaly
            - ascending_node
            - current_owner
            - diameter
            - eccentricity
            - inclination
            - name
            - orbital_period
            - periapsis
            - semi_major_axis
            - serial
        description: Ordering
        explode: false
        style: form
      - in: query
        name: periapsis_max
        schema:
          type: number
          format: double
          maximum: 1000
          minimum: -1000
          exclusiveMaximum: true
          exclusiveMinimum: true
          nullable: true
          title: Argument of periapsis
      - in: query
        name: periapsis_min
        schema:
          type: number
          format: double
          maximum: 1000
          minimum: -1000
          exclusiveMaximum: true
          exclusiveMinimum: true
          nullable: true
          title: Argument of periapsis
      - in: query
        name: semi_major_axis_max
        schema:
          type: number
          format: double
          maximum: 3.913
          minimum: 0.89
          nullable: true
      - in: query
        name: semi_major_axis_min
        schema:
          type: number
          format: double
          maximum: 3.913
          minimum: 0.89
          nullable: true
      - in: query
        name: serial_max
        schema:
          type: integer
          maximum: 9223372036854775807
          minimum: 0
          format: int64
          title: Serial number
      - in: query
        name: serial_min
        schema:
          type: integer
          maximum: 9223372036854775807
          minimum: 0
          format: int64
          title: Serial number
      - in: query
        name: user_owned
        schema:
          type: boolean
      tags:
      - adalia_id
      security:
      - jwtAuth: []
      - cookieAuth: []
      - basicAuth: []
      - {}
      responses:
        '200':
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/PaginatedAsteroidList'
            text/csv:
              schema:
                $ref: '#/components/schemas/PaginatedAsteroidList'
          description: ''
  /api/adalia_id/asteroids/{serial}/:
    get:
      operationId: adalia_id_asteroids_retrieve
      parameters:
      - in: query
        name: format
        schema:
          type: string
          enum:
          - csv
          - datatables
          - json
      - in: path
        name: serial
        schema:
          type: integer
          maximum: 9223372036854775807
          minimum: 0
          format: int64
          title: Serial number
        required: true
      tags:
      - adalia_id
      security:
      - jwtAuth: []
      - cookieAuth: []
      - basicAuth: []
      - {}
      responses:
        '200':
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Asteroid'
            text/csv:
              schema:
                $ref: '#/components/schemas/Asteroid'
          description: ''
  /api/adalia_id/asteroids/alliance_control/:
    get:
      operationId: adalia_id_asteroids_alliance_control_retrieve
      parameters:
      - in: query
        name: format
        schema:
          type: string
          enum:
          - csv
          - datatables
          - json
      tags:
      - adalia_id
      security:
      - jwtAuth: []
      - cookieAuth: []
      - basicAuth: []
      - {}
      responses:
        '200':
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Asteroid'
            text/csv:
              schema:
                $ref: '#/components/schemas/Asteroid'
          description: ''
  /api/adalia_id/asteroids/early_adopter/:
    get:
      operationId: adalia_id_asteroids_early_adopter_retrieve
      parameters:
      - in: query
        name: format
        schema:
          type: string
          enum:
          - csv
          - datatables
          - json
      tags:
      - adalia_id
      security:
      - jwtAuth: []
      - cookieAuth: []
      - basicAuth: []
      - {}
      responses:
        '200':
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Asteroid'
            text/csv:
              schema:
                $ref: '#/components/schemas/Asteroid'
          description: ''
  /api/adalia_id/asteroids/personal/:
    get:
      operationId: adalia_id_asteroids_personal_retrieve
      parameters:
      - in: query
        name: format
        schema:
          type: string
          enum:
          - csv
          - datatables
          - json
      tags:
      - adalia_id
      security:
      - jwtAuth: []
      - cookieAuth: []
      - basicAuth: []
      - {}
      responses:
        '200':
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Asteroid'
            text/csv:
              schema:
                $ref: '#/components/schemas/Asteroid'
          description: ''
  /api/adalia_id/asteroids/summary/:
    get:
      operationId: adalia_id_asteroids_summary_retrieve
      parameters:
      - in: query
        name: format
        schema:
          type: string
          enum:
          - csv
          - datatables
          - json
      tags:
      - adalia_id
      security:
      - jwtAuth: []
      - cookieAuth: []
      - basicAuth: []
      - {}
      responses:
        '200':
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Asteroid'
            text/csv:
              schema:
                $ref: '#/components/schemas/Asteroid'
          description: ''
  /api/adalia_id/crewmates/:
    get:
      operationId: adalia_id_crewmates_list
      parameters:
      - in: query
        name: alliance_controlled
        schema:
          type: boolean
      - in: query
        name: collection
        schema:
          type: integer
      - in: query
        name: current_owner
        schema:
          type: string
      - in: query
        name: description
        schema:
          type: string
      - in: query
        name: format
        schema:
          type: string
          enum:
          - csv
          - datatables
          - json
      - in: query
        name: image_url
        schema:
          type: string
      - name: limit
        required: false
        in: query
        description: Number of results to return per page.
        schema:
          type: integer
      - in: query
        name: name
        schema:
          type: string
      - name: offset
        required: false
        in: query
        description: The initial index from which to return the results.
        schema:
          type: integer
      - in: query
        name: polymorphic_ctype
        schema:
          type: integer
      - in: query
        name: profile
        schema:
          type: string
      - in: query
        name: serial_max
        schema:
          type: integer
          maximum: 9223372036854775807
          minimum: 0
          format: int64
          title: Serial number
      - in: query
        name: serial_min
        schema:
          type: integer
          maximum: 9223372036854775807
          minimum: 0
          format: int64
          title: Serial number
      - in: query
        name: user_owned
        schema:
          type: boolean
      tags:
      - adalia_id
      security:
      - jwtAuth: []
      - cookieAuth: []
      - basicAuth: []
      - {}
      responses:
        '200':
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/PaginatedCrewmateList'
            text/csv:
              schema:
                $ref: '#/components/schemas/PaginatedCrewmateList'
          description: ''
  /api/adalia_id/crewmates/{serial}/:
    get:
      operationId: adalia_id_crewmates_retrieve
      parameters:
      - in: query
        name: format
        schema:
          type: string
          enum:
          - csv
          - datatables
          - json
      - in: path
        name: serial
        schema:
          type: integer
          maximum: 9223372036854775807
          minimum: 0
          format: int64
          title: Serial number
        required: true
      tags:
      - adalia_id
      security:
      - jwtAuth: []
      - cookieAuth: []
      - basicAuth: []
      - {}
      responses:
        '200':
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Crewmate'
            text/csv:
              schema:
                $ref: '#/components/schemas/Crewmate'
          description: ''
  /api/adalia_id/crewmates/alliance_control/:
    get:
      operationId: adalia_id_crewmates_alliance_control_retrieve
      parameters:
      - in: query
        name: format
        schema:
          type: string
          enum:
          - csv
          - datatables
          - json
      tags:
      - adalia_id
      security:
      - jwtAuth: []
      - cookieAuth: []
      - basicAuth: []
      - {}
      responses:
        '200':
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Crewmate'
            text/csv:
              schema:
                $ref: '#/components/schemas/Crewmate'
          description: ''
  /api/adalia_id/crewmates/personal/:
    get:
      operationId: adalia_id_crewmates_personal_retrieve
      parameters:
      - in: query
        name: format
        schema:
          type: string
          enum:
          - csv
          - datatables
          - json
      tags:
      - adalia_id
      security:
      - jwtAuth: []
      - cookieAuth: []
      - basicAuth: []
      - {}
      responses:
        '200':
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Crewmate'
            text/csv:
              schema:
                $ref: '#/components/schemas/Crewmate'
          description: ''
  /api/auth/users/:
    get:
      operationId: auth_users_list
      parameters:
      - in: query
        name: format
        schema:
          type: string
          enum:
          - csv
          - datatables
          - json
      - name: limit
        required: false
        in: query
        description: Number of results to return per page.
        schema:
          type: integer
      - name: offset
        required: false
        in: query
        description: The initial index from which to return the results.
        schema:
          type: integer
      tags:
      - auth
      security:
      - jwtAuth: []
      - cookieAuth: []
      - basicAuth: []
      responses:
        '200':
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/PaginatedUserList'
            text/csv:
              schema:
                $ref: '#/components/schemas/PaginatedUserList'
          description: ''
  /api/auth/users/{username}/:
    get:
      operationId: auth_users_retrieve
      parameters:
      - in: query
        name: format
        schema:
          type: string
          enum:
          - csv
          - datatables
          - json
      - in: path
        name: username
        schema:
          type: string
          description: Required. 150 characters or fewer. Letters, digits and @/./+/-/_
            only.
        required: true
      tags:
      - auth
      security:
      - jwtAuth: []
      - cookieAuth: []
      - basicAuth: []
      responses:
        '200':
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/User'
            text/csv:
              schema:
                $ref: '#/components/schemas/User'
          description: ''
    put:
      operationId: auth_users_update
      parameters:
      - in: query
        name: format
        schema:
          type: string
          enum:
          - csv
          - datatables
          - json
      - in: path
        name: username
        schema:
          type: string
          description: Required. 150 characters or fewer. Letters, digits and @/./+/-/_
            only.
        required: true
      tags:
      - auth
      requestBody:
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/User'
          application/x-www-form-urlencoded:
            schema:
              $ref: '#/components/schemas/User'
          multipart/form-data:
            schema:
              $ref: '#/components/schemas/User'
        required: true
      security:
      - jwtAuth: []
      - cookieAuth: []
      - basicAuth: []
      responses:
        '200':
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/User'
            text/csv:
              schema:
                $ref: '#/components/schemas/User'
          description: ''
    patch:
      operationId: auth_users_partial_update
      parameters:
      - in: query
        name: format
        schema:
          type: string
          enum:
          - csv
          - datatables
          - json
      - in: path
        name: username
        schema:
          type: string
          description: Required. 150 characters or fewer. Letters, digits and @/./+/-/_
            only.
        required: true
      tags:
      - auth
      requestBody:
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/PatchedUser'
          application/x-www-form-urlencoded:
            schema:
              $ref: '#/components/schemas/PatchedUser'
          multipart/form-data:
            schema:
              $ref: '#/components/schemas/PatchedUser'
      security:
      - jwtAuth: []
      - cookieAuth: []
      - basicAuth: []
      responses:
        '200':
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/User'
            text/csv:
              schema:
                $ref: '#/components/schemas/User'
          description: ''
  /api/login:
    post:
      operationId: login_create
      description: |-
        API View that receives a POST with a user's username and password.

        Returns a JSON Web Token that can be used for authenticated requests.
      parameters:
      - in: query
        name: format
        schema:
          type: string
          enum:
          - csv
          - datatables
          - json
      tags:
      - login
      requestBody:
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/JSONWebToken'
          application/x-www-form-urlencoded:
            schema:
              $ref: '#/components/schemas/JSONWebToken'
          multipart/form-data:
            schema:
              $ref: '#/components/schemas/JSONWebToken'
        required: true
      responses:
        '200':
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/JSONWebToken'
            text/csv:
              schema:
                $ref: '#/components/schemas/JSONWebToken'
          description: ''
  /api/nfts/{collection}/assets/:
    get:
      operationId: nfts_assets_list
      description: This viewset automatically provides `list` and `retrieve` actions.
      parameters:
      - in: path
        name: collection
        schema:
          type: string
          pattern: ^[A-Z]+$
        required: true
      - in: query
        name: format
        schema:
          type: string
          enum:
          - csv
          - datatables
          - json
      - name: limit
        required: false
        in: query
        description: Number of results to return per page.
        schema:
          type: integer
      - name: offset
        required: false
        in: query
        description: The initial index from which to return the results.
        schema:
          type: integer
      tags:
      - nfts
      security:
      - jwtAuth: []
      - cookieAuth: []
      - basicAuth: []
      - {}
      responses:
        '200':
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/PaginatedNFTAssetList'
            text/csv:
              schema:
                $ref: '#/components/schemas/PaginatedNFTAssetList'
          description: ''
  /api/nfts/{collection}/assets/{serial}/:
    get:
      operationId: nfts_assets_retrieve
      description: This viewset automatically provides `list` and `retrieve` actions.
      parameters:
      - in: path
        name: collection
        schema:
          type: string
          pattern: ^[A-Z]+$
        required: true
      - in: query
        name: format
        schema:
          type: string
          enum:
          - csv
          - datatables
          - json
      - in: path
        name: serial
        schema:
          type: string
        required: true
      tags:
      - nfts
      security:
      - jwtAuth: []
      - cookieAuth: []
      - basicAuth: []
      - {}
      responses:
        '200':
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/NFTAsset'
            text/csv:
              schema:
                $ref: '#/components/schemas/NFTAsset'
          description: ''
  /api/nfts/{collection}/assets/alliance_control/:
    get:
      operationId: nfts_assets_alliance_control_retrieve
      description: This viewset automatically provides `list` and `retrieve` actions.
      parameters:
      - in: path
        name: collection
        schema:
          type: string
          pattern: ^[A-Z]+$
        required: true
      - in: query
        name: format
        schema:
          type: string
          enum:
          - csv
          - datatables
          - json
      tags:
      - nfts
      security:
      - jwtAuth: []
      - cookieAuth: []
      - basicAuth: []
      - {}
      responses:
        '200':
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/NFTAsset'
            text/csv:
              schema:
                $ref: '#/components/schemas/NFTAsset'
          description: ''
  /api/nfts/{collection}/assets/personal/:
    get:
      operationId: nfts_assets_personal_retrieve
      description: This viewset automatically provides `list` and `retrieve` actions.
      parameters:
      - in: path
        name: collection
        schema:
          type: string
          pattern: ^[A-Z]+$
        required: true
      - in: query
        name: format
        schema:
          type: string
          enum:
          - csv
          - datatables
          - json
      tags:
      - nfts
      security:
      - jwtAuth: []
      - cookieAuth: []
      - basicAuth: []
      - {}
      responses:
        '200':
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/NFTAsset'
            text/csv:
              schema:
                $ref: '#/components/schemas/NFTAsset'
          description: ''
  /api/refresh:
    post:
      operationId: refresh_create
      description: |-
        API View that returns a refreshed token (with new expiration) based on
        existing token

        If 'orig_iat' field (original issued-at-time) is found it will first check
        if it's within expiration window, then copy it to the new token.
      parameters:
      - in: query
        name: format
        schema:
          type: string
          enum:
          - csv
          - datatables
          - json
      tags:
      - refresh
      requestBody:
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/RefreshAuthToken'
          application/x-www-form-urlencoded:
            schema:
              $ref: '#/components/schemas/RefreshAuthToken'
          multipart/form-data:
            schema:
              $ref: '#/components/schemas/RefreshAuthToken'
        required: true
      responses:
        '200':
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/RefreshAuthToken'
            text/csv:
              schema:
                $ref: '#/components/schemas/RefreshAuthToken'
          description: ''
  /api/wallets/wallets/:
    get:
      operationId: wallets_wallets_list
      parameters:
      - in: query
        name: format
        schema:
          type: string
          enum:
          - csv
          - datatables
          - json
      - name: limit
        required: false
        in: query
        description: Number of results to return per page.
        schema:
          type: integer
      - name: offset
        required: false
        in: query
        description: The initial index from which to return the results.
        schema:
          type: integer
      tags:
      - wallets
      security:
      - jwtAuth: []
      - cookieAuth: []
      - basicAuth: []
      - {}
      responses:
        '200':
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/PaginatedWalletList'
            text/csv:
              schema:
                $ref: '#/components/schemas/PaginatedWalletList'
          description: ''
  /api/wallets/wallets/{id}/:
    get:
      operationId: wallets_wallets_retrieve
      parameters:
      - in: query
        name: format
        schema:
          type: string
          enum:
          - csv
          - datatables
          - json
      - in: path
        name: id
        schema:
          type: string
        required: true
      tags:
      - wallets
      security:
      - jwtAuth: []
      - cookieAuth: []
      - basicAuth: []
      - {}
      responses:
        '200':
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Wallet'
            text/csv:
              schema:
                $ref: '#/components/schemas/Wallet'
          description: ''
components:
  schemas:
    Asteroid:
      type: object
      properties:
        id:
          type: integer
          readOnly: true
        collection:
          type: string
          pattern: ^[-a-zA-Z0-9_]+$
        current_owner:
          type: string
          description: Required. 150 characters or fewer. Letters, digits and @/./+/-/_
            only.
          readOnly: true
        allegiance:
          type: string
          description: The name of the organization
          readOnly: true
        most_recent_valuation:
          type: string
          readOnly: true
        spectral_type:
          type: string
          readOnly: true
        serial:
          type: integer
          maximum: 9223372036854775807
          minimum: 0
          format: int64
          title: Serial number
        name:
          type: string
          maxLength: 128
        description:
          type: string
          maxLength: 1024
        image_url:
          type: string
          maxLength: 256
        profile:
          type: string
        diameter:
          type: integer
          maximum: 750284
          minimum: 2046
          nullable: true
        surface_area:
          type: integer
          maximum: 2147483647
          minimum: -2147483648
          nullable: true
        semi_major_axis:
          type: number
          format: double
          maximum: 3.913
          minimum: 0.89
          nullable: true
        eccentricity:
          type: number
          format: double
          maximum: 0.392
          minimum: 0.007
          nullable: true
        inclination:
          type: number
          format: double
          maximum: 37.77
          minimum: 0.01
          nullable: true
        ascending_node:
          type: number
          format: double
          maximum: 1000
          minimum: -1000
          exclusiveMaximum: true
          exclusiveMinimum: true
          nullable: true
        anomaly:
          type: number
          format: double
          maximum: 1000
          minimum: -1000
          exclusiveMaximum: true
          exclusiveMinimum: true
          nullable: true
          title: Mean anomaly at epoch
        periapsis:
          type: number
          format: double
          maximum: 1000
          minimum: -1000
          exclusiveMaximum: true
          exclusiveMinimum: true
          nullable: true
          title: Argument of periapsis
        orbital_period:
          type: number
          format: double
          maximum: 10000
          minimum: 0
          exclusiveMaximum: true
          nullable: true
          title: Period
        purchase_order:
          type: integer
          maximum: 2147483647
          minimum: -2147483648
        polymorphic_ctype:
          type: integer
          readOnly: true
      required:
      - allegiance
      - collection
      - current_owner
      - id
      - most_recent_valuation
      - polymorphic_ctype
      - serial
      - spectral_type
    CrewClassEnum:
      enum:
      - -1
      - 1
      - 2
      - 3
      - 4
      - 5
      type: integer
    Crewmate:
      type: object
      properties:
        id:
          type: integer
          readOnly: true
        collection:
          type: string
          pattern: ^[-a-zA-Z0-9_]+$
        current_owner:
          type: string
          description: Required. 150 characters or fewer. Letters, digits and @/./+/-/_
            only.
          readOnly: true
        allegiance:
          type: string
          description: The name of the organization
          readOnly: true
        most_recent_valuation:
          type: string
          readOnly: true
        serial:
          type: integer
          maximum: 9223372036854775807
          minimum: 0
          format: int64
          title: Serial number
        name:
          type: string
          maxLength: 128
        description:
          type: string
          maxLength: 1024
        image_url:
          type: string
          maxLength: 256
        profile:
          type: string
        crew_collection:
          type: integer
          maximum: 2147483647
          minimum: -2147483648
        gender:
          allOf:
          - $ref: '#/components/schemas/GenderEnum'
          title: Gender identity
          description: Though the Influence API refers to this as `sex`, we use more
            inclusive language.
        body:
          type: integer
          maximum: 2147483647
          minimum: -2147483648
        crew_class:
          allOf:
          - $ref: '#/components/schemas/CrewClassEnum'
          minimum: -2147483648
          maximum: 2147483647
        title:
          allOf:
          - $ref: '#/components/schemas/TitleEnum'
          minimum: -2147483648
          maximum: 2147483647
        outfit:
          type: integer
          maximum: 2147483647
          minimum: -2147483648
        hair:
          type: integer
          maximum: 2147483647
          minimum: -2147483648
        facial_feature:
          type: integer
          maximum: 2147483647
          minimum: -2147483648
        hair_color:
          type: integer
          maximum: 2147483647
          minimum: -2147483648
        head_piece:
          type: integer
          maximum: 2147483647
          minimum: -2147483648
        bonus_item:
          type: integer
          maximum: 2147483647
          minimum: -2147483648
        polymorphic_ctype:
          type: integer
          readOnly: true
      required:
      - allegiance
      - collection
      - current_owner
      - id
      - most_recent_valuation
      - polymorphic_ctype
      - serial
    GenderEnum:
      enum:
      - M
      - F
      - '-'
      type: string
    JSONWebToken:
      type: object
      description: |-
        Serializer class used to validate a username and password.

        'username' is identified by the custom UserModel.USERNAME_FIELD.

        Returns a JSON Web Token that can be used to authenticate later calls.
      properties:
        password:
          type: string
          writeOnly: true
        token:
          type: string
          readOnly: true
        username:
          type: string
          writeOnly: true
      required:
      - password
      - token
      - username
    NFTAsset:
      type: object
      properties:
        id:
          type: integer
          readOnly: true
        serial:
          type: integer
          maximum: 9223372036854775807
          minimum: 0
          format: int64
          title: Serial number
        allegiance:
          type: string
          description: The name of the organization
          readOnly: true
        name:
          type: string
          maxLength: 128
        current_owner:
          type: string
          description: Required. 150 characters or fewer. Letters, digits and @/./+/-/_
            only.
          readOnly: true
        image_url:
          type: string
          maxLength: 256
        description:
          type: string
          maxLength: 1024
        collection:
          type: string
          pattern: ^[-a-zA-Z0-9_]+$
      required:
      - allegiance
      - collection
      - current_owner
      - id
      - serial
    PaginatedAsteroidList:
      type: object
      properties:
        count:
          type: integer
          example: 123
        next:
          type: string
          nullable: true
          format: uri
          example: http://api.example.org/accounts/?offset=400&limit=100
        previous:
          type: string
          nullable: true
          format: uri
          example: http://api.example.org/accounts/?offset=200&limit=100
        results:
          type: array
          items:
            $ref: '#/components/schemas/Asteroid'
    PaginatedCrewmateList:
      type: object
      properties:
        count:
          type: integer
          example: 123
        next:
          type: string
          nullable: true
          format: uri
          example: http://api.example.org/accounts/?offset=400&limit=100
        previous:
          type: string
          nullable: true
          format: uri
          example: http://api.example.org/accounts/?offset=200&limit=100
        results:
          type: array
          items:
            $ref: '#/components/schemas/Crewmate'
    PaginatedNFTAssetList:
      type: object
      properties:
        count:
          type: integer
          example: 123
        next:
          type: string
          nullable: true
          format: uri
          example: http://api.example.org/accounts/?offset=400&limit=100
        previous:
          type: string
          nullable: true
          format: uri
          example: http://api.example.org/accounts/?offset=200&limit=100
        results:
          type: array
          items:
            $ref: '#/components/schemas/NFTAsset'
    PaginatedUserList:
      type: object
      properties:
        count:
          type: integer
          example: 123
        next:
          type: string
          nullable: true
          format: uri
          example: http://api.example.org/accounts/?offset=400&limit=100
        previous:
          type: string
          nullable: true
          format: uri
          example: http://api.example.org/accounts/?offset=200&limit=100
        results:
          type: array
          items:
            $ref: '#/components/schemas/User'
    PaginatedWalletList:
      type: object
      properties:
        count:
          type: integer
          example: 123
        next:
          type: string
          nullable: true
          format: uri
          example: http://api.example.org/accounts/?offset=400&limit=100
        previous:
          type: string
          nullable: true
          format: uri
          example: http://api.example.org/accounts/?offset=200&limit=100
        results:
          type: array
          items:
            $ref: '#/components/schemas/Wallet'
    PatchedUser:
      type: object
      properties:
        username:
          type: string
          description: Required. 150 characters or fewer. Letters, digits and @/./+/-/_
            only.
          pattern: ^[\w.@+-]+$
          maxLength: 150
        allegiance:
          type: integer
          nullable: true
    RefreshAuthToken:
      type: object
      description: Serializer used for refreshing JWTs.
      properties:
        token:
          type: string
      required:
      - token
    TitleEnum:
      enum:
      - -1
      - 1
      - 2
      - 3
      - 4
      - 5
      - 6
      - 7
      - 8
      - 9
      - 10
      - 11
      - 12
      - 13
      - 14
      - 15
      - 16
      - 17
      - 18
      - 19
      - 20
      - 21
      - 22
      - 23
      - 24
      - 25
      - 26
      - 27
      - 28
      - 29
      - 30
      - 31
      - 32
      - 33
      - 34
      - 35
      - 36
      - 37
      - 38
      - 39
      - 40
      - 41
      - 42
      - 43
      - 44
      - 45
      - 46
      - 47
      - 48
      - 49
      - 50
      - 51
      - 52
      - 53
      - 54
      - 55
      - 56
      - 57
      - 58
      - 59
      - 60
      - 61
      - 62
      - 63
      - 64
      - 65
      type: integer
    User:
      type: object
      properties:
        username:
          type: string
          description: Required. 150 characters or fewer. Letters, digits and @/./+/-/_
            only.
          pattern: ^[\w.@+-]+$
          maxLength: 150
        allegiance:
          type: integer
          nullable: true
      required:
      - username
    Wallet:
      type: object
      properties:
        blockchain:
          type: integer
        address:
          type: string
          maxLength: 96
      required:
      - address
      - blockchain
  securitySchemes:
    basicAuth:
      type: http
      scheme: basic
    cookieAuth:
      type: apiKey
      in: cookie
      name: sessionid
    jwtAuth:
      type: http
      scheme: bearer
      bearerFormat: JWT
    oauth2:
      type: oauth2
      flows: {}
