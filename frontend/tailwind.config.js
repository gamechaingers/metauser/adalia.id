// eslint-disable-next-line @typescript-eslint/no-var-requires
const defaultTheme = require('tailwindcss/defaultTheme')

/** @type {import('tailwindcss/tailwind-config').TailwindConfig} */
module.exports = {
  content: [
    './index.html',
    './src/**/*.{ts,tsx}',
    './node_modules/@vechaiui/**/*.{js,ts,jsx,tsx}',
  ],
  darkMode: 'class',
  theme: {
    screens: {
      xs: '400px',
      ...defaultTheme.screens,
    },
    extend: {
      colors: {
        danger: 'var(--theme-color-danger)',
      },
      backgroundColor: {
        secondary: 'var(--theme-bg-secondary)',
      },
    },
  },
  plugins: [require('@tailwindcss/forms'), require('@vechaiui/core')],
}
