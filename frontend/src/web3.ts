import { t } from '@transifex/native'
import Onboard from 'bnc-onboard'
import { providers } from 'ethers'
import { useEffect, useState } from 'react'
import {
  fetchWalletChallenge,
  fetchWalletVerify,
} from './api/adaliaIdComponentsCustom'
import { useAuthStore } from './stores/auth-store'

let provider: providers.Web3Provider
const onboard = Onboard({
  networkId: 1,
  walletSelect: {
    wallets: [{ walletName: 'metamask' }],
  },
  subscriptions: {
    wallet: (wallet) => {
      if (wallet.name) {
        saveSelectedWallet(wallet.name)
      }
      provider = new providers.Web3Provider(wallet.provider)
    },
  },
})

const storageKey = 'selectedWallet'
const saveSelectedWallet = (wallet: string) =>
  localStorage.setItem(storageKey, wallet)
const getSelectedWallet = () => localStorage.getItem(storageKey)

export const useOnboardThemeSync = (darkMode: boolean) => {
  useEffect(() => {
    onboard.config({ darkMode })
  }, [darkMode])
}

const selectWallet = async () => {
  const selected = await onboard.walletSelect(getSelectedWallet() ?? undefined)
  if (selected) {
    return await onboard.walletCheck()
  }
  return false
}

export enum WalletAuthState {
  NONE,
  PENDING,
  SIGNATURE_ERROR,
  FAILED_AUTH,
  SERVER_ERROR,
  SUCCESS,
}

export const walletAuthErrorMessage = (
  authState: WalletAuthState
): string | undefined => {
  const mapping: Record<number, string> = {
    [WalletAuthState.SIGNATURE_ERROR]: t('Could not obtain signature.'),
    [WalletAuthState.FAILED_AUTH]: t('The provided signature is invalid.'),
    [WalletAuthState.SERVER_ERROR]: t(
      'There was an unexpected error. Please try again or contact support.'
    ),
  }
  return mapping[authState]
}
export type walletAuthFunc = () => void

const getChallenge = async (address: string) => {
  try {
    return await fetchWalletChallenge({
      pathParams: { blockchain: 'ETH', address },
    })
  } catch {
    return null
  }
}

const getSignature = async (
  signer: providers.JsonRpcSigner,
  message: string
) => {
  try {
    return await signer.signMessage(message)
  } catch {
    return null
  }
}

const verifySignature = async (
  signature: string,
  identifier: string,
  address: string
) => {
  try {
    return await fetchWalletVerify({
      pathParams: { blockchain: 'ETH', address },
      body: {
        signature,
        identifier,
      },
    })
  } catch {
    return null
  }
}

export const useWalletAuth = (): [walletAuthFunc, WalletAuthState] => {
  const [authState, setAuthState] = useState<WalletAuthState>(
    WalletAuthState.NONE
  )
  const login = useAuthStore((s) => s.jwtLogin)
  const tryWalletAuth = async () => {
    const selected = await selectWallet()
    const address = onboard.getState().address
    const signer = provider?.getSigner()
    if (!selected || !signer) {
      setAuthState(WalletAuthState.SIGNATURE_ERROR)
      return
    }
    const challenge = await getChallenge(address)
    if (!challenge) {
      setAuthState(WalletAuthState.SERVER_ERROR)
      return
    }
    const signature = await getSignature(signer, challenge.message)
    if (!signature) {
      setAuthState(WalletAuthState.SIGNATURE_ERROR)
      return
    }
    const r = await verifySignature(signature, challenge.identifier, address)
    if (r) {
      const success = login(r.token)
      setAuthState(
        success ? WalletAuthState.SUCCESS : WalletAuthState.FAILED_AUTH
      )
    } else {
      setAuthState(WalletAuthState.FAILED_AUTH)
    }
    setAuthState(r ? WalletAuthState.SUCCESS : WalletAuthState.FAILED_AUTH)
  }
  return [tryWalletAuth, authState]
}
