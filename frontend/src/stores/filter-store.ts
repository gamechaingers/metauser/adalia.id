import create from 'zustand'

export type Filter<T> = {
  active: boolean
  value: T
}

export type Filters = Record<string, Filter<unknown>>

export type FilterStore<T extends Filters> = {
  filters: T
  appliedFilters: T
  setFilters: (filters: T) => void
  setAppliedFilters: (filters: T) => void
  applyFilters: () => void
  resetFilters: () => void
  setActive: (property: keyof T, active: boolean, autoApply: boolean) => void
  setValue: (property: keyof T, value: unknown) => void
  hasUnappliedChanges: boolean
}

const hasUnappliedChanges = <T extends Filters>(current: T, applied: T) =>
  Object.keys(current).some((key) => {
    const c = current[key]
    const a = applied[key]
    const activeDifferent = c.active !== a.active
    const valueDifferent =
      c.active &&
      a.active &&
      JSON.stringify(c.value) !== JSON.stringify(a.value)
    return activeDifferent || valueDifferent
  })

export const createFilterStore = <T extends Filters>(defaultFilters: T) =>
  create<FilterStore<T>>((set) => ({
    hasUnappliedChanges: false,
    filters: defaultFilters,
    appliedFilters: defaultFilters,
    setFilters: (filters) => set(() => ({ filters })),
    setAppliedFilters: (filters) =>
      set(() => ({ filters, appliedFilters: filters })),
    applyFilters: () =>
      set((s) => ({ appliedFilters: s.filters, hasUnappliedChanges: false })),
    resetFilters: () =>
      set((s) => {
        const filters = Object.fromEntries(
          Object.entries(s.appliedFilters).map(([name, value]) => [
            name,
            { ...value, active: false },
          ])
        ) as T
        return { appliedFilters: filters, filters, hasUnappliedChanges: false }
      }),
    setActive: (prop, active, autoApply) =>
      set((s) => {
        const filters: T = {
          ...s.filters,
          [prop]: {
            ...s.filters[prop],
            active,
          },
        }
        const appliedFilters: T = autoApply
          ? {
              ...s.appliedFilters,
              [prop]: {
                ...s.appliedFilters[prop],
                active,
              },
            }
          : s.appliedFilters
        return {
          filters,
          appliedFilters,
          hasUnappliedChanges: hasUnappliedChanges(filters, appliedFilters),
        }
      }),
    setValue: (prop, value) =>
      set((s) => {
        const filter = s.filters[prop]
        const filters = {
          ...s.filters,
          [prop]: {
            ...filter,
            value,
          },
        }
        return {
          filters,
          hasUnappliedChanges: hasUnappliedChanges(filters, s.appliedFilters),
        }
      }),
  }))

export const filterValue = <T>(filter: Filter<T> | undefined) =>
  filter?.active ? filter.value : undefined

export const mapFilterValue = <T, R>(
  filter: Filter<T> | null | undefined,
  mapper: (f: T) => R
) => (filter?.active ? mapper(filter.value) : undefined)

export const activeFilter = <T>(value: T): Filter<T> => ({
  active: true,
  value,
})

export const inactiveFilter = <T>(value: T): Filter<T> => ({
  active: false,
  value,
})

export const initFilter = <T>(
  filter: Filter<T>,
  queryParam: T | null | undefined
) => (!filter.active && queryParam ? activeFilter(queryParam) : filter)
