import { useEffect } from 'react'
import create from 'zustand'
import {
  clearAuth,
  LoginFunc,
  processJwt,
  RefreshFunc,
  restoreAuth,
  saveAuth,
  useAuthFuncs,
  UserData,
} from '../auth'

export type AuthStore = {
  userData?: UserData
  jwtLogin: (jwt: string) => boolean
  login: (
    username: string,
    password: string,
    loginFunc: LoginFunc
  ) => Promise<UserData | null>
  refresh: (
    userData: UserData,
    refreshFunc: RefreshFunc
  ) => Promise<UserData | null>
  logout: () => void
}

export const useAuthStore = create<AuthStore>((set) => ({
  userData: restoreAuth() ?? undefined,
  jwtLogin: (jwt: string) => {
    const userData = processJwt(jwt)
    if (userData) {
      set(() => ({
        userData,
      }))
      return true
    } else {
      return false
    }
  },
  login: async (username, password, loginFunc) => {
    const userData = processJwt((await loginFunc(username, password))?.token)
    if (userData) {
      saveAuth(userData.accessToken)
      set(() => ({
        userData,
      }))
      return userData
    }
    return null
  },
  refresh: async (userData, refreshFunc) => {
    const newUserData = processJwt(
      (await refreshFunc(userData.accessToken))?.token
    )
    if (newUserData) {
      saveAuth(newUserData.accessToken)
      set(() => ({
        userData: newUserData,
      }))
      return newUserData
    } else {
      clearAuth()
      set(() => ({
        userData: undefined,
      }))
    }
    return null
  },
  logout: () =>
    set(() => {
      clearAuth()
      return { userData: undefined }
    }),
}))

export const useAuthStoreLogin = () => {
  const { loginFunc } = useAuthFuncs()
  const login = useAuthStore((s) => s.login)
  return (username: string, password: string) =>
    login(username, password, loginFunc)
}

export const useAuthStoreRefresh = () => {
  const { refreshFunc } = useAuthFuncs()
  const userData = useAuthStore((s) => s.userData)
  const refresh = useAuthStore((s) => s.refresh)

  return () => (userData ? refresh(userData, refreshFunc) : null)
}

export const useAuthRequestOptions = (): RequestInit => {
  const accessToken = useAuthStore((s) => s.userData?.accessToken)

  return accessToken
    ? {
        headers: {
          Authorization: `Bearer ${accessToken}`,
        },
      }
    : {}
}

export const usePeriodicAuthTokenRefresh = () => {
  const refresh = useAuthStoreRefresh()
  const tokenExpiry = useAuthStore((s) => s.userData?.tokenExpiry)
  const interval = 10 * 1000
  const maxDiff = 20 * 1000

  useEffect(() => {
    const id = setInterval(() => {
      if (tokenExpiry) {
        const timeDiffMs = tokenExpiry.getTime() - new Date().getTime()
        if (timeDiffMs < maxDiff) {
          refresh()
        }
      }
    }, interval)
    return () => clearInterval(id)
  }, [tokenExpiry])
}

export const useUserData = () => useAuthStore((s) => s.userData)
export const useLoggedIn = () => useAuthStore((s) => !!s.userData)
