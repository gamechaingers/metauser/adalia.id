import { ReactNode, useEffect } from 'react'
import create from 'zustand'

export type SidebarStore = {
  collapsed: boolean
  setCollapsed: (collapsed: boolean) => void
  pinned: boolean
  setPinned: (pinned: boolean) => void
  mobileOpen: boolean
  setMobileOpen: (open: boolean) => void
  filters: ReactNode
  setFilters: (filters: ReactNode) => void
  filtersOpen: boolean
  setFiltersOpen: (open: boolean) => void
}

export const useSidebarStore = create<SidebarStore>((set) => ({
  collapsed: false,
  pinned: true,
  setPinned: (pinned) => set(() => ({ pinned })),
  mobileOpen: false,
  filters: null,
  filtersOpen: false,
  setFiltersOpen: (filtersOpen) =>
    set((s) => ({
      filtersOpen,
      collapsed: filtersOpen || !s.pinned,
    })),
  setFilters: (filters) => set(() => ({ filters })),
  setMobileOpen: (mobileOpen) =>
    set(() => ({
      mobileOpen,
    })),
  setCollapsed: (collapsed) =>
    set(() => ({
      collapsed,
    })),
}))

export const useSidebarFilters = (filters: ReactNode) => {
  const setFilters = useSidebarStore((s) => s.setFilters)
  const setFiltersOpen = useSidebarStore((s) => s.setFiltersOpen)

  useEffect(() => {
    setFilters(filters)
    return () => {
      setFilters(null)
      setFiltersOpen(false)
    }
  }, [])
}
