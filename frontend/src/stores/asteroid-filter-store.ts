import { useEffect } from 'react'
import { StringParam, useQueryParams } from 'use-query-params'
import { Constants } from '../constants'
import { NumberRangeParam } from '../custom-query-params'
import {
  createFilterStore,
  inactiveFilter,
  Filter,
  filterValue,
  initFilter,
} from './filter-store'

export type AsteroidFilters = {
  orbitalPeriod: Filter<[number, number]>
  diameter: Filter<[number, number]>
  eccentricity: Filter<[number, number]>
  inclination: Filter<[number, number]>
  semiMajorAxis: Filter<[number, number]>
  name: Filter<string>
}

export const useAsteroidFilterStore = createFilterStore<AsteroidFilters>({
  orbitalPeriod: inactiveFilter([
    Constants.minOrbitalPeriod,
    Constants.maxOrbitalPeriod,
  ]),
  diameter: inactiveFilter([Constants.minDiameter, Constants.maxDiameter]),
  eccentricity: inactiveFilter([
    Constants.minEccentricity,
    Constants.maxEccentricity,
  ]),
  inclination: inactiveFilter([
    Constants.minInclination,
    Constants.maxInclination,
  ]),
  semiMajorAxis: inactiveFilter([
    Constants.minSemiMajorAxis,
    Constants.maxSemiMajorAxis,
  ]),
  name: inactiveFilter(''),
})

export const useAsteroidFiltersParamSync = () => {
  const setFilters = useAsteroidFilterStore((s) => s.setAppliedFilters)
  const filters = useAsteroidFilterStore((s) => s.appliedFilters)

  const [params, setParams] = useQueryParams({
    orbitalPeriod: NumberRangeParam,
    diameter: NumberRangeParam,
    eccentricity: NumberRangeParam,
    inclination: NumberRangeParam,
    semiMajorAxis: NumberRangeParam,
    name: StringParam,
  })

  const hasQpFilters = Object.values(params).some((v) => !!v)

  useEffect(() => {
    if (hasQpFilters) {
      setFilters({
        diameter: initFilter(filters.diameter, params.diameter),
        orbitalPeriod: initFilter(filters.orbitalPeriod, params.orbitalPeriod),
        eccentricity: initFilter(filters.eccentricity, params.eccentricity),
        inclination: initFilter(filters.inclination, params.inclination),
        semiMajorAxis: initFilter(filters.semiMajorAxis, params.semiMajorAxis),
        name: initFilter(filters.name, params.name),
      })
    }
  }, [])

  useEffect(() => {
    setParams(
      {
        diameter: filterValue(filters.diameter),
        orbitalPeriod: filterValue(filters.orbitalPeriod),
        eccentricity: filterValue(filters.eccentricity),
        inclination: filterValue(filters.inclination),
        semiMajorAxis: filterValue(filters.semiMajorAxis),
        name: filterValue(filters.name),
      },
      'replaceIn'
    )
  }, [filters])
}
