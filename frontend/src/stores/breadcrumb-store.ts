import { ReactNode } from 'react'
import create from 'zustand'

export interface BreadcrumbItem {
  name: ReactNode
  path: string
  hidden?: boolean
  icon?: ReactNode
}

export type BreadcrumbStore = {
  items: BreadcrumbItem[]
  setItems: (items: BreadcrumbItem[]) => void
}

export const useBreadcrumbStore = create<BreadcrumbStore>((set) => ({
  items: [],
  setItems: (items) => set(() => ({ items })),
}))
