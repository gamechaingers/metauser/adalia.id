export const Constants = {
  minDiameter: 2046,
  maxDiameter: 375_142 * 2,
  minOrbitalPeriod: 307,
  maxOrbitalPeriod: 2826,
  minEccentricity: 0.007,
  maxEccentricity: 0.393,
  minInclination: 0,
  maxInclination: 37.77,
  minSemiMajorAxis: 0.891,
  maxSemiMajorAxis: 3.912,
}
