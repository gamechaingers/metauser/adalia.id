import { decodeToken, isExpired } from 'react-jwt'
import { fetchLoginCreate, fetchRefreshCreate } from './api/adaliaIdComponents'
import { JSONWebToken, RefreshAuthToken } from './api/adaliaIdSchemas'

export type LoginData = {
  username: string
  password: string
}

export type UserData = {
  username: string
  userId: number
  accessToken: string
  tokenExpiry: Date
}

type Token = {
  username: string
  user_id: number
  exp: number
}

export const processJwt = (jwt: string | undefined) => {
  if (jwt && isExpired(jwt)) {
    return null
  }
  const token = jwt ? (decodeToken(jwt) as Token | null) : null
  if (jwt && token) {
    return {
      username: token.username,
      userId: token.user_id,
      accessToken: jwt,
      tokenExpiry: new Date(token.exp * 1000),
    } as UserData
  }
  return null
}

export const saveAuth = (token: string) =>
  localStorage.setItem('accessToken', token)

export const restoreAuth = () => {
  const token = localStorage.getItem('accessToken')
  return processJwt(token ?? undefined)
}

export const clearAuth = () => {
  localStorage.removeItem('accessToken')
}

export type LoginFunc = (
  username: string,
  password: string
) => Promise<JSONWebToken | null>

export type RefreshFunc = (token: string) => Promise<RefreshAuthToken | null>

export type AuthFuncs = {
  loginFunc: LoginFunc
  refreshFunc: RefreshFunc
}

export const useAuthFuncs = (): AuthFuncs => {
  return {
    loginFunc: (username: string, password: string) =>
      fetchLoginCreate({ body: { username, password, token: '' } }).catch(
        () => null
      ),
    refreshFunc: (token: string) =>
      fetchRefreshCreate({ body: { token } }).catch(() => null),
  }
}
