import { ReactNode } from 'react'

export interface ChildrenProps {
  children?: ReactNode
}

export interface ClassNameProps {
  className?: string
}

export type ChildrenClassNameProps = ChildrenProps & ClassNameProps
