import { ReactElement, ReactNode } from 'react'
import { Loading } from './components/loading'
import { ErrorPage } from './pages/generic-pages'

type WithApiDataProps<T, E> = {
  loading: boolean
  data: T | null | undefined
  error?: E
  errorElement?: (error: E) => ReactElement
  notFound?: {
    title: ReactNode
    subtext: ReactNode
  }
  loadingText?: ReactNode
  children: (data: T) => JSX.Element
}

export const WithApiData = <T, E>({
  loading,
  data,
  error,
  errorElement,
  loadingText,
  children,
}: WithApiDataProps<T, E>) => {
  if (loading) {
    return <Loading text={loadingText} />
  } else if (data) {
    return children(data)
  } else if (error && errorElement) {
    return errorElement(error)
  } else {
    return <ErrorPage />
  }
}
