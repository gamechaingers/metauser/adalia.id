import { useT } from '@transifex/react'
import { useMemo } from 'react'
import { useLocaleSettings } from './hooks/locale-hooks'

const digits = (value: number, digits: number, locale: string) =>
  value.toLocaleString([locale], { maximumFractionDigits: digits })

export const useFormatters = () => {
  const t = useT()
  const { language } = useLocaleSettings()
  const locale = language.code

  return useMemo(
    () => ({
      diameter: (value: number) => `${digits(value, 0, locale)} m`,
      inclination: (value: number) => `${digits(value, 3, locale)} °`,
      semiMajorAxis: (value: number) => `${digits(value, 3, locale)} AU`,
      orbitalPeriod: (value: number) =>
        `${digits(value, 0, locale)} ${t('days')}`,
      eccentricity: (value: number) => `${digits(value, 3, locale)}`,
      surfaceArea: (value: number) => `${digits(value, 0, locale)} km²`,
      range: (
        [from, to]: [number, number],
        formatter: (value: number) => string
      ) => `${formatter(from)} - ${formatter(to)}`,
    }),
    [locale]
  )
}
