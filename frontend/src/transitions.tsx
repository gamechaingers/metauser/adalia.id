export const TransitionProps = {
  HorizontalSlide: {
    enter: 'transition ease-in-out duration-200',
    enterFrom: '-translate-x-1/2 scale-x-0',
    enterTo: 'translate-x-0 scale-x-100',
    leave: 'transition-all duration-200 ease-in-out',
    leaveFrom: 'translate-x-0 scale-x-100',
    leaveTo: '-translate-x-1/2 scale-x-0',
  },
  VerticalSlide: {
    enter: 'transition ease-in-out duration-200',
    enterFrom: 'transform scale-y-0 -translate-y-1/2',
    enterTo: 'transform scale-y-100 translate-x-0',
    leave: 'transition ease-out duration-100',
    leaveFrom: 'transform scale-y-100 translate-y-0',
    leaveTo: 'transform scale-y-0 -translate-y-1/2',
  },
  Appear: {
    enter: 'transition ease-in-out duration-150',
    enterFrom: 'transform opacity-0 scale-90',
    enterTo: 'transform opacity-100 scale-100',
    leave: 'transition ease-out duration-75',
    leaveFrom: 'transform opacity-100 scale-100',
    leaveTo: 'transform opacity-0 scale-90',
  },
}
