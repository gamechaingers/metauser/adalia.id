import { colors, ColorScheme, extendTheme } from '@vechaiui/react'
import React, { ReactNode } from 'react'
import { VechaiProvider } from '@vechaiui/react'
import { useOnboardThemeSync } from './web3'

type CustomColorScheme = ColorScheme & {
  customColors: {
    secondary: string
    danger: string
  }
}

const defaultLight: CustomColorScheme = {
  id: 'defaultLight',
  type: 'light',
  colors: {
    bg: {
      base: colors.gray['300'],
      fill: colors.blueGray['700'],
    },
    text: {
      foreground: colors.black['200'],
      muted: colors.black['400'],
    },
    primary: colors.blue,
    neutral: colors.gray,
  },
  customColors: {
    secondary: colors.coolGray['400'],
    danger: colors.red['400'],
  },
}

const defaultDark: CustomColorScheme = {
  id: 'defaultDark',
  type: 'dark',
  colors: {
    bg: {
      base: colors.blueGray['800'],
      fill: colors.blueGray['700'],
    },
    text: {
      foreground: colors.coolGray['300'],
      muted: colors.coolGray['400'],
    },
    primary: colors.blue,
    neutral: colors.gray,
  },
  customColors: {
    secondary: colors.coolGray['900'],
    danger: colors.red['400'],
  },
}

const useCustomColors = (colorScheme: CustomColorScheme) =>
  React.useEffect(() => {
    const vars = [
      {
        var: 'bg-secondary',
        value: colorScheme.customColors.secondary,
      },
      {
        var: 'color-danger',
        value: colorScheme.customColors.danger,
      },
    ]
    vars.forEach((v) =>
      document.documentElement.style.setProperty(`--theme-${v.var}`, v.value)
    )
  }, [colorScheme.id])

export const theme = extendTheme({
  cursor: 'pointer',
  colorSchemes: {
    defaultLight,
    defaultDark,
  },
})

const defaultThemes = { defaultLight, defaultDark }

export type ThemedVechaiProviderProps = {
  children: (themeSwitcher: ReactNode) => ReactNode
}
export const ThemedVechaiProvider = ({
  children,
}: ThemedVechaiProviderProps) => {
  const [dark] = React.useState(true)
  const colorScheme = dark
    ? defaultThemes.defaultDark
    : defaultThemes.defaultLight

  useCustomColors(colorScheme)
  useOnboardThemeSync(dark)

  // Disabled until the light theme is actually good
  // const themeSwitcher = <ThemeSwitcher dark={dark} setDark={setDark} />
  const themeSwitcher = null

  return (
    <VechaiProvider theme={theme} colorScheme={colorScheme.id}>
      {children(themeSwitcher)}
    </VechaiProvider>
  )
}
