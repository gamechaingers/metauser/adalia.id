import { T } from '@transifex/react'
import { Button, FormControl, FormLabel, Input } from '@vechaiui/react'
import { FormEvent, useState } from 'react'
import { Link } from '../components/link'
import {
  LoginRegisterHeader,
  LoginRegisterPageLayout,
} from '../components/login-register-page-utils'
import { routes } from './routes'

export const RegisterPage = () => {
  const [email, setEmail] = useState('')
  const [password, setPassword] = useState('')
  const [confirmPassword, setConfirmPassword] = useState('')
  const [registerPending, setRegisterPending] = useState(false)

  const doPasswordsNotMatch =
    password.length > 0 &&
    confirmPassword.length > 0 &&
    password !== confirmPassword
  const isInvalid =
    email.length === 0 ||
    doPasswordsNotMatch ||
    password.length === 0 ||
    confirmPassword.length === 0

  const onSubmit = (e: FormEvent) => {
    e.preventDefault()
    setRegisterPending(true)
  }

  const header = (
    <LoginRegisterHeader
      text={<T _str='Create your adalia.id account' />}
      subText={
        <span>
          <T
            _str='Or {linkToLogin} '
            linkToLogin={
              <Link to={routes.login()}>
                <T _str='log into your existing account' />
              </Link>
            }
          />
        </span>
      }
    />
  )
  return (
    <LoginRegisterPageLayout header={header}>
      <form className='flex flex-col space-y-4' onSubmit={onSubmit}>
        <FormControl id='email'>
          <FormLabel className='text-lg'>
            <T _str='Email' />
          </FormLabel>
          <Input
            type='text'
            required
            autoFocus
            value={email}
            disabled={registerPending}
            onChange={(e) => setEmail(e.currentTarget.value)}
          />
        </FormControl>
        <FormControl id='password'>
          <FormLabel className='text-lg'>
            <T _str='Password' />
          </FormLabel>
          <Input
            type='password'
            required
            invalid={doPasswordsNotMatch}
            disabled={registerPending}
            value={password}
            onChange={(e) => setPassword(e.currentTarget.value)}
          />
        </FormControl>
        <FormControl id='confirm-password'>
          <FormLabel className='text-lg'>
            <T _str='Confirm password' />
          </FormLabel>
          <Input
            type='password'
            required
            invalid={doPasswordsNotMatch}
            disabled={registerPending}
            value={confirmPassword}
            onChange={(e) => setConfirmPassword(e.currentTarget.value)}
          />
        </FormControl>
        {doPasswordsNotMatch && (
          <span className='text-red-400'>
            <T _str='Passwords do not match!' />
          </span>
        )}
        <Button
          type='submit'
          variant='solid'
          color='primary'
          loading={registerPending}
          disabled={isInvalid}
        >
          <T _str='Create account' />
        </Button>
      </form>
    </LoginRegisterPageLayout>
  )
}
