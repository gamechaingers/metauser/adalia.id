import { faArrowUpRightFromSquare } from '@fortawesome/free-solid-svg-icons'
import { T } from '@transifex/react'
import { ReactNode } from 'react'
import { useBreadcrumbUpdate } from '../breadcrumb'
import { AsteroidTable } from '../components/asteroid-table'
import { CrewmatesTable } from '../components/crewmate-table'
import { Icon } from '../components/icon'
import { Link } from '../components/link'
import { useStandardTableProps } from '../hooks/table-hooks'
import { routes } from './routes'

type HeaderProps = {
  columnToggle: ReactNode
  title: ReactNode
  openPath: string
}
const Header = ({ columnToggle, title, openPath }: HeaderProps) => (
  <div className='flex flex-row w-full'>
    <Link
      className='flex flex-row gap-x-3 items-center grow text-2xl hover:underline'
      to={openPath}
      noStyle
    >
      {title}
      <Icon faIcon={faArrowUpRightFromSquare} />
    </Link>
    {columnToggle}
  </div>
)

const pageSizes = [10, 25, 50]

const Asteroids = () => {
  const tableProps = useStandardTableProps(10)
  return (
    <AsteroidTable
      {...tableProps}
      pageSizes={pageSizes}
      header={(columnToggle) => (
        <Header
          columnToggle={columnToggle}
          title={<T _str='My Asteroids' />}
          openPath={routes.myAsteroids}
        />
      )}
    />
  )
}

const Crewmates = () => {
  const tableProps = useStandardTableProps(10)
  return (
    <CrewmatesTable
      {...tableProps}
      pageSizes={pageSizes}
      header={(columnToggle) => (
        <Header
          columnToggle={columnToggle}
          title={<T _str='My Crewmates' />}
          openPath={routes.allCrewmates}
        />
      )}
    />
  )
}

export const DashboardPage = () => {
  useBreadcrumbUpdate(() => [
    {
      name: <T _str='Dashboard' />,
      path: routes.dashboard,
    },
  ])
  return (
    <div className='flex flex-col gap-y-3'>
      <h1>
        <T _str='Dashboard' />
      </h1>
      <Asteroids />
      <Crewmates />
    </div>
  )
}
