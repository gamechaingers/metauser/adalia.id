import { useBreadcrumbUpdate } from '../breadcrumb'

export const HomePage = () => {
  useBreadcrumbUpdate(() => [])
  return (
    <div className='flex flex-col w-full'>
      <h1>Hello Adalia</h1>
    </div>
  )
}
