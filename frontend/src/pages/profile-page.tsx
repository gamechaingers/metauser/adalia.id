import { T } from '@transifex/react'
import { useBreadcrumbUpdate } from '../breadcrumb'
import { routes } from './routes'

export const ProfilePage = () => {
  useBreadcrumbUpdate(() => [
    {
      name: <T _str='Profile' />,
      path: routes.profile,
    },
  ])
  return (
    <h1>
      <T _str='Profile' />
    </h1>
  )
}
