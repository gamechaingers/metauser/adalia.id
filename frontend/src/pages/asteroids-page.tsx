import { faMeteor } from '@fortawesome/free-solid-svg-icons'
import { T } from '@transifex/react'
import { Button } from '@vechaiui/react'
import { ReactNode } from 'react'
import { Asteroid } from '../api/adaliaIdSchemas'
import { useBreadcrumbUpdate } from '../breadcrumb'
import { AsteroidFilterTags } from '../components/asteroid-filter-tags'
import { AsteroidFilters } from '../components/asteroid-filters'
import { AsteroidTable, AsteroidTableProps } from '../components/asteroid-table'
import { FilterBtn } from '../components/filters'
import { Icon } from '../components/icon'
import { Link } from '../components/link'
import {
  usePaginationQueryParams,
  useSortByQueryParam,
} from '../hooks/table-hooks'
import {
  useAsteroidFiltersParamSync,
  useAsteroidFilterStore,
} from '../stores/asteroid-filter-store'
import { useUserData } from '../stores/auth-store'
import { useSidebarFilters } from '../stores/sidebar-store'
import { routes } from './routes'

const pageSizes = [15, 25, 50]

type ActionButtonProps = {
  icon: JSX.Element
  link: string
  text: ReactNode
}
const ActionButton = ({ icon, link, text }: ActionButtonProps) => (
  <Link className='hidden md:flex' to={link} noStyle>
    <Button className='text-lg gap-x-3 !bg-base' leftIcon={icon}>
      <span className='hidden md:flex'>{text}</span>
    </Button>
  </Link>
)

export type AsteroidsPageProps = {
  selfOwned?: boolean
  allianceControlled?: boolean
}
export const AsteroidsPage = ({
  selfOwned,
  allianceControlled,
}: AsteroidsPageProps) => {
  const user = useUserData()
  useAsteroidFiltersParamSync()
  useBreadcrumbUpdate(() => [
    {
      name: <T _str='Asteroids' />,
      path: routes.allAsteroids,
    },
  ])
  useSidebarFilters(<AsteroidFilters />)
  const ActionButtons = () => (
    <>
      {user && (allianceControlled || selfOwned) && (
        <ActionButton
          link={routes.allAsteroids}
          icon={<Icon faIcon={faMeteor} />}
          text={<T _str='All Asteroids' />}
        />
      )}
      {user && !selfOwned && (
        <ActionButton
          link={routes.myAsteroids}
          icon={<Icon faIcon={faMeteor} />}
          text={<T _str='My Asteroids' />}
        />
      )}
      {user && !allianceControlled && (
        <ActionButton
          link={routes.allianceAsteroids}
          icon={<Icon faIcon={faMeteor} />}
          text={<T _str='Alliance asteroids' />}
        />
      )}
    </>
  )

  const asteroidFilters = useAsteroidFilterStore((s) => s.appliedFilters)
  const [sortBy, setSortBy] = useSortByQueryParam<Asteroid>()
  const paginationParams = usePaginationQueryParams(15)
  const props: AsteroidTableProps = {
    ...paginationParams,
    sortBy,
    setSortBy,
    pageSizes,
    filters: asteroidFilters,
    selfOwned,
    allianceControlled,
    useQueryParamColumns: true,
    header: (columnToggle) => (
      <div className='flex flex-row gap-x-2 justify-end items-center flex-wrap w-full'>
        <ActionButtons />
        <FilterBtn className='md:hidden !bg-base' mobile>
          <T _str='Filters' />
        </FilterBtn>
        <FilterBtn className='hidden md:flex !bg-base'>
          <T _str='Filters' />
        </FilterBtn>
        {columnToggle}
      </div>
    ),
  }
  return (
    <div className='flex flex-col gap-y-3'>
      <h1 className='grow'>
        {selfOwned && 'My asteroids'}
        {allianceControlled && 'Alliance asteroids'}
        {!selfOwned && !allianceControlled && 'All asteroids'}
      </h1>
      <AsteroidFilterTags />
      <AsteroidTable {...props} />
    </div>
  )
}
