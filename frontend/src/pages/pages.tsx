import {
  Redirect,
  Route,
  RouteProps,
  Switch,
  useHistory,
  useLocation,
} from 'react-router-dom'
import { useEffect } from 'react'
import { useLoggedIn } from '../stores/auth-store'
import { routes } from './routes'
import { HomePage } from './home-page'
import { LoginPage } from './login-page'
import { ProfilePage } from './profile-page'
import { RegisterPage } from './register-page'
import { SettingsPage } from './settings/settings-page'
import { AllCrewmatesPage } from './all-crewmates-page'
import { AsteroidPage } from './asteroid-page'
import { CrewmatePage } from './crewmate-page'
import { NotFoundPage } from './generic-pages'
import { AsteroidsPage } from './asteroids-page'
import { DashboardPage } from './dashboard-page'

const fullscreenRoutes = [routes.login(), routes.register]
export const useIsFullscreenPage = () => {
  const { pathname } = useLocation()
  return fullscreenRoutes.some((r) => pathname.startsWith(r))
}

const Logout = () => {
  const { goBack } = useHistory()
  useEffect(() => {
    goBack()
  }, [])
  return null
}

export const AuthRoute = (props: RouteProps) => {
  const loggedIn = useLoggedIn()
  const location = useLocation()
  const next = location.pathname + location.search
  return loggedIn ? <Route {...props} /> : <Redirect to={routes.login(next)} />
}

export const Pages = () => {
  return (
    <Switch>
      <Route exact path={routes.home} component={HomePage} />
      <Route path={routes.login()} component={LoginPage} />
      <Route path={routes.logout} component={Logout} />
      <Route path={routes.register} component={RegisterPage} />
      <AuthRoute path={routes.dashboard} component={DashboardPage} />
      <AuthRoute path={routes.profile} component={ProfilePage} />
      <AuthRoute path={routes.settings} component={SettingsPage} />
      <Route path={routes.asteroid(':id')} component={AsteroidPage} />
      <Route path={routes.crewmate(':id')} component={CrewmatePage} />
      <Route path={routes.allAsteroids}>
        <AsteroidsPage />
      </Route>
      <AuthRoute path={routes.myAsteroids}>
        <AsteroidsPage selfOwned />
      </AuthRoute>
      <AuthRoute path={routes.allianceAsteroids}>
        <AsteroidsPage allianceControlled />
      </AuthRoute>
      <Route path={routes.allCrewmates} component={AllCrewmatesPage} />
      <Route path='*' component={NotFoundPage} />
    </Switch>
  )
}
