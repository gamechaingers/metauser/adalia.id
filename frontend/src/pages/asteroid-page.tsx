import { T } from '@transifex/react'
import { useParams } from 'react-router-dom'
import { WithApiData } from '../api-utils'
import { useAdaliaIdAsteroidsRetrieve } from '../api/adaliaIdComponents'
import { useBreadcrumbUpdate } from '../breadcrumb'
import { AsteroidDetails } from '../components/asteroid-details'
import { NotFoundPage } from './generic-pages'
import { routes } from './routes'

const Details = ({ id }: { id: number }) => {
  const { data, isLoading, error } = useAdaliaIdAsteroidsRetrieve({
    pathParams: { serial: id },
  })

  const name = data?.name
  useBreadcrumbUpdate(
    () => [
      { name: <T _str='Asteroids' />, path: routes.allAsteroids },
      {
        name: name ?? '',
        path: routes.asteroid(data?.id.toString() ?? ''),
        hidden: !name,
      },
    ],
    [name]
  )

  return (
    <WithApiData
      data={data}
      loading={isLoading}
      error={error}
      notFound={{
        title: <T _sr='Asteroid does not exist.' />,
        subtext: <T _str='Seems like you entered an invalid id.' />,
      }}
    >
      {(asteroid) => <AsteroidDetails asteroid={asteroid} />}
    </WithApiData>
  )
}

type AsteroidPageParams = {
  id: string
}
export const AsteroidPage = () => {
  const { id } = useParams<AsteroidPageParams>()

  const parsedId = parseInt(id, 10)
  return isNaN(parsedId) ? <NotFoundPage /> : <Details id={parsedId} />
}
