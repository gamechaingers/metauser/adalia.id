import { faEthereum } from '@fortawesome/free-brands-svg-icons'
import { faAdd, faCopy, faTrash } from '@fortawesome/free-solid-svg-icons'
import { t } from '@transifex/native'
import { T, UT } from '@transifex/react'
import { useNotification } from '@vechaiui/react'
import { useCallback, useEffect, useMemo } from 'react'
import { Column, usePagination, useTable } from 'react-table'
import { useWalletsWalletsList } from '../../api/adaliaIdComponents'
import { Wallet } from '../../api/adaliaIdSchemas'
import { Button } from '../../components/button'
import { Icon } from '../../components/icon'
import { Table } from '../../components/table'
import { Tooltip } from '../../components/tooltip'
import {
  usePageCount,
  usePaginationQueryParams,
  usePaginationSync,
} from '../../hooks/table-hooks'
import {
  useWalletAuth,
  walletAuthErrorMessage,
  WalletAuthState,
} from '../../web3'

const pageSizes = [10, 20, 50]

const useWalletAdd = () => {
  const notification = useNotification()
  const [authWallet, authState] = useWalletAuth()

  useEffect(() => {
    const errorMsg = walletAuthErrorMessage(authState)
    const isSuccess = authState === WalletAuthState.SUCCESS
    if (errorMsg || isSuccess) {
      notification({
        position: 'top',
        status: errorMsg ? 'error' : 'success',
        title: errorMsg
          ? t('Wallet authentication error')
          : T('Wallet added successfully'),
        description: errorMsg,
      })
    }
  }, [authState])
  return authWallet
}

const useWalletColumns = () => {
  const notification = useNotification()
  return useMemo(
    (): Column<Wallet>[] => [
      {
        id: 'blockchain',
        Header: <T _str='Blockchain' />,
        accessor: () => (
          <span>
            <Icon faIcon={faEthereum} /> Ethereum
          </span>
        ),
      },
      {
        id: 'address',
        Header: <T _str='Address' />,
        accessor: (wallet) => wallet.address,
      },
      {
        id: 'actions',
        Header: <T _str='Actions' />,
        accessor: (wallet) => (
          <div className='flex flex-row gap-x-3'>
            <Button
              className='gap-x-3 text-lg'
              faIcon={faCopy}
              responsiveIcon
              onClick={() => {
                window.navigator.clipboard.writeText(wallet.address).then(() =>
                  notification({
                    title: t('Wallet address copied!'),
                    position: 'top',
                    status: 'info',
                    duration: 2000,
                  })
                )
              }}
            >
              <T _str='Copy' />
            </Button>
            <Button
              className='gap-x-3 text-lg'
              faIcon={faTrash}
              responsiveIcon
              danger
            >
              <T _str='Remove' />
            </Button>
          </div>
        ),
      },
    ],
    []
  )
}
export const WalletsPage = () => {
  const addWallet = useWalletAdd()
  const columns = useWalletColumns()
  const { pageIndex, pageSize, setPageIndex, setPageSize } =
    usePaginationQueryParams(10)
  const { data, isLoading } = useWalletsWalletsList({
    queryParams: { limit: pageSize, offset: pageIndex - 1 },
  })
  const pageCount = usePageCount(data?.count, pageSize)
  const wallets = useMemo(() => data?.results ?? [], [data])
  const table = useTable(
    {
      data: wallets,
      columns,
      pageCount,
      initialState: { pageIndex: pageIndex - 1, pageSize },
      manualPagination: true,
    },
    usePagination
  )
  usePaginationSync(table, pageSizes, setPageIndex, setPageSize)

  const Header = useCallback(
    () =>
      data ? (
        <div className='flex flex-row items-center px-2 w-full'>
          <h2 className='grow'>
            <T _str='{walletCount} Linked Wallets' walletCount={data?.count} />
          </h2>
          <Tooltip
            content={
              <>
                <UT _str='This will prompt you to connect a wallet and then sign a message. After the signature has been verified, the wallet will be linked to your account.' />
              </>
            }
          >
            <div>
              <Button
                faIcon={faAdd}
                onClick={addWallet}
                variant='solid'
                color='primary'
                size='md'
              >
                <T _str='Add wallet' />
              </Button>
            </div>
          </Tooltip>
        </div>
      ) : null,
    [data]
  )

  return (
    <Table
      table={table}
      loadingText={<T _str='Loading wallets...' />}
      canToggleColumns={false}
      loading={isLoading}
      header={() => <Header />}
      paging={{ pageSizes }}
      totalCount={data?.count}
    />
  )
}
