import {
  faLock,
  faUsers,
  faWallet,
  faEnvelopeOpen,
} from '@fortawesome/free-solid-svg-icons'
import { T } from '@transifex/react'
import { Redirect, Switch, useRouteMatch } from 'react-router-dom'
import { useBreadcrumbUpdate } from '../../breadcrumb'
import { Icon } from '../../components/icon'
import { TabItem, Tabs } from '../../components/tabs'
import { AuthRoute } from '../pages'
import { routes } from '../routes'
import { WalletsPage } from './wallets-page'

const EmailSettings = () => <div>Emails</div>
const ChangePasswordSettings = () => <div>Change Password</div>
const ConnectedAccountsSettings = () => <div>Connected Accounts</div>

export const SettingsPage = () => {
  const { path, url } = useRouteMatch()
  useBreadcrumbUpdate(() => [
    {
      name: <T _str='Settings' />,
      path: routes.settings,
    },
  ])
  const tabs: TabItem[] = [
    {
      title: <T _str='Emails' />,
      icon: <Icon faIcon={faEnvelopeOpen} />,
      link: `${url}/emails`,
    },
    {
      title: <T _str='Wallets' />,
      icon: <Icon faIcon={faWallet} />,
      link: `${url}/wallets`,
    },
    {
      title: <T _str='Change Password' />,
      icon: <Icon faIcon={faLock} />,
      link: `${url}/change-password`,
    },
    {
      title: <T _str='Connected Accounts' />,
      icon: <Icon faIcon={faUsers} />,
      link: `${url}/accounts`,
    },
  ]

  return (
    <div className='flex flex-col space-y-2'>
      <Tabs items={tabs} />
      <Switch>
        <Redirect exact from={`${path}`} to={`${path}/emails`} />
        <AuthRoute path={`${path}/emails`} component={EmailSettings} />
        <AuthRoute path={`${path}/wallets`} component={WalletsPage} />
        <AuthRoute
          path={`${path}/change-password`}
          component={ChangePasswordSettings}
        />
        <AuthRoute
          path={`${path}/accounts`}
          component={ConnectedAccountsSettings}
        />
      </Switch>
    </div>
  )
}
