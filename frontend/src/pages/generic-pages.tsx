import { faFrown } from '@fortawesome/free-solid-svg-icons'
import { T } from '@transifex/react'
import { Icon } from '../components/icon'

export const NotFoundPage = ({
  title,
  subtext,
}: {
  title?: string
  subtext?: string
}) => (
  <div className='flex flex-col items-center gap-y-3'>
    <h1>{title ?? 'Site not found'}</h1>
    <Icon className='text-9xl' faIcon={faFrown} />
    <span>{subtext ?? <T _str='This site does not exist' />}</span>
  </div>
)

export const ErrorPage = () => (
  <div className='flex flex-col items-center gap-y-3'>
    <h1>
      <T _str='Something went wrong' />
    </h1>
    <Icon className='text-9xl' faIcon={faFrown} />
    <span>
      <T
        _str='There was an error on our side. Please try again and reach out to the
      support if the issue persists.'
      />
    </span>
  </div>
)
