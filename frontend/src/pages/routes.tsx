export const routes = {
  home: '/',
  dashboard: '/dashboard',
  login: (next: string | null = null) =>
    '/login' + (next ? `?next=${next}` : ''),
  logout: '/logout',
  register: '/register',
  profile: '/profile',
  settings: '/settings',
  allAsteroids: '/asteroids',
  myAsteroids: '/my-asteroids/',
  allianceAsteroids: '/alliance-asteroids',
  allCrewmates: '/crewmates',
  asteroid: (id: string) => `/asteroids/${id}`,
  crewmate: (id: string) => `/crewmates/${id}`,
}
