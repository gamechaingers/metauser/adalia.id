import { T } from '@transifex/react'
import { useParams } from 'react-router-dom'
import { WithApiData } from '../api-utils'
import { useAdaliaIdCrewmatesRetrieve } from '../api/adaliaIdComponents'
import { useBreadcrumbUpdate } from '../breadcrumb'
import { CrewmateDetails } from '../components/crewmate-details'
import { NotFoundPage } from './generic-pages'
import { routes } from './routes'

const Details = ({ id }: { id: number }) => {
  const { data, isLoading, error } = useAdaliaIdCrewmatesRetrieve({
    pathParams: { serial: id },
  })

  const name = data?.name
  useBreadcrumbUpdate(
    () => [
      { name: <T _str='Crewmates' />, path: routes.allCrewmates },
      {
        name: name ?? '',
        path: routes.crewmate(data?.id.toString() ?? ''),
        hidden: !name,
      },
    ],
    [name]
  )
  return (
    <WithApiData
      data={data}
      loading={isLoading}
      error={error}
      notFound={{
        title: <T _str='Crewmate does not exist' />,
        subtext: <T _str='Seems like you entered an invalid id.' />,
      }}
    >
      {(crewmate) => <CrewmateDetails crewmate={crewmate} />}
    </WithApiData>
  )
}

type CrewmatePageParams = {
  id: string
}
export const CrewmatePage = () => {
  const { id } = useParams<CrewmatePageParams>()

  const parsedId = parseInt(id, 10)
  return isNaN(parsedId) ? <NotFoundPage /> : <Details id={parsedId} />
}
