import { Crewmate } from '../api/adaliaIdSchemas'
import { useBreadcrumbUpdate } from '../breadcrumb'
import {
  CrewmatesTable,
  CrewmateTableProps,
} from '../components/crewmate-table'
import {
  useSortByQueryParam,
  usePaginationQueryParams,
} from '../hooks/table-hooks'
import { routes } from './routes'

const pageSizes = [15, 25, 50]

export const AllCrewmatesPage = () => {
  useBreadcrumbUpdate(() => [
    {
      name: 'Crewmates',
      path: routes.allCrewmates,
    },
  ])

  const [sortBy, setSortBy] = useSortByQueryParam<Crewmate>()
  const paginationParams = usePaginationQueryParams(15)

  const props: CrewmateTableProps = {
    ...paginationParams,
    sortBy,
    setSortBy,
    pageSizes,
  }
  return (
    <>
      <h1>All crewmates</h1>
      <CrewmatesTable {...props} />
    </>
  )
}
