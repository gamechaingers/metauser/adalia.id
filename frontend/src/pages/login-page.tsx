import { T } from '@transifex/react'
import { Button, FormControl, FormLabel, Input } from '@vechaiui/react'
import { FormEvent, useState } from 'react'
import { Redirect } from 'react-router-dom'
import { StringParam, useQueryParam } from 'use-query-params'
import { Link } from '../components/link'
import {
  LoginRegisterHeader,
  LoginRegisterPageLayout,
} from '../components/login-register-page-utils'
import { useAuthStoreLogin, useLoggedIn } from '../stores/auth-store'
import { routes } from './routes'

export const LoginPage = () => {
  const loggedIn = useLoggedIn()
  const login = useAuthStoreLogin()
  const [username, setUsername] = useState('')
  const [password, setPassword] = useState('')
  const isInvalid = username.length === 0 || password.length === 0
  const [loginPending, setLoginPending] = useState(false)
  const [loginFailed, setLoginFailed] = useState(false)
  const [next] = useQueryParam('next', StringParam)

  const onSubmit = async (e: FormEvent) => {
    e.preventDefault()
    setLoginPending(true)
    const success = await login(username, password)
    if (!success) {
      setPassword('')
      setLoginFailed(true)
      setLoginPending(false)
    }
  }

  if (loggedIn) {
    const redirectTo = () => {
      if (!next || next === routes.home) {
        return routes.dashboard
      }
      return next
    }
    return <Redirect to={redirectTo()} />
  }

  const header = (
    <LoginRegisterHeader
      text={<T _str='Sign in to adalia.id' />}
      subText={
        <span>
          <T
            _str='Or {linkToCreateNewAccount} '
            linkToCreateNewAccount={
              <Link to={routes.register}>
                <T _str='create a new account' />
              </Link>
            }
          />
        </span>
      }
    />
  )
  return (
    <LoginRegisterPageLayout header={header}>
      <form className='flex flex-col gap-y-4' onSubmit={onSubmit}>
        <FormControl id='username'>
          <FormLabel className='text-lg'>
            <T _str='Username' />
          </FormLabel>
          <Input
            type='text'
            disabled={loginPending}
            required
            autoFocus
            value={username}
            onChange={(e) => setUsername(e.currentTarget.value)}
          />
        </FormControl>
        <FormControl id='password'>
          <FormLabel className='text-lg'>
            <T _str='Password' />
          </FormLabel>
          <Input
            type='password'
            disabled={loginPending}
            required
            value={password}
            onChange={(e) => setPassword(e.currentTarget.value)}
          />
        </FormControl>
        {loginFailed && (
          <span className='text-red-500'>
            <T _str='Username or password is incorrect' />
          </span>
        )}
        <Button
          type='submit'
          variant='solid'
          color='primary'
          loading={loginPending}
          disabled={isInvalid}
        >
          <T _str='Log in' />
        </Button>
      </form>
    </LoginRegisterPageLayout>
  )
}
