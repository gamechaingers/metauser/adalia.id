import { faHome } from '@fortawesome/free-solid-svg-icons'
import { Breadcrumb as VBreadcrumb } from '@vechaiui/react'
import React, { useMemo } from 'react'
import { Icon } from './components/icon'
import { Link } from './components/link'
import { ClassNameProps } from './default-props'
import { routes } from './pages/routes'
import { BreadcrumbItem, useBreadcrumbStore } from './stores/breadcrumb-store'

export const useBreadcrumbUpdate = (
  makeItems: () => BreadcrumbItem[],
  deps: React.DependencyList = []
) => {
  const { setItems } = useBreadcrumbStore()
  const items = useMemo(makeItems, deps)
  React.useEffect(() => setItems(items), [items])
}

interface HomeItemProps {
  forceFull: boolean
}
const HomeItem = ({ forceFull }: HomeItemProps) => (
  <Link
    className='flex flex-row items-center space-x-2 text-foreground hover:underline'
    to={routes.home}
    noStyle
  >
    <Icon faIcon={faHome} className='h-5' />
    <span className={forceFull ? 'flex' : 'hidden lg:flex'}>Home</span>
  </Link>
)

interface ItemsProps extends ClassNameProps {
  items: BreadcrumbItem[]
  trimmed: boolean
  back?: string
}
const Items = ({ items, className, trimmed, back }: ItemsProps) => (
  <div className='flex flex-col'>
    <VBreadcrumb className={`text-2xl ml-0 ${className}`}>
      <VBreadcrumb.Item>
        <HomeItem forceFull={items.length == 0} />
      </VBreadcrumb.Item>
      {trimmed && (
        <VBreadcrumb.Item>
          {back ? (
            <Link className='hover:underline' to={back} noStyle>
              ...
            </Link>
          ) : (
            <span>...</span>
          )}
        </VBreadcrumb.Item>
      )}
      {items.map((item) => (
        <VBreadcrumb.Item key={item.path}>
          <Link
            className='flex flex-row items-center space-x-2 text-foreground hover:underline'
            to={item.path}
            noStyle
          >
            {item.icon}
            <span className='flex truncate'>{item.name}</span>
          </Link>
        </VBreadcrumb.Item>
      ))}
    </VBreadcrumb>
  </div>
)

export const Breadcrumb = ({ className }: ClassNameProps) => {
  const items = useBreadcrumbStore((s) => s.items.filter((i) => !i.hidden))
  const trimmedItems = items.length > 1 ? [items[items.length - 1]] : items
  const trimmed = items.length !== trimmedItems.length
  const back = trimmed ? items[items.length - 2].path : undefined

  return (
    <>
      <div className='hidden md:block'>
        <Items
          className={className}
          items={items}
          trimmed={false}
          back={back}
        />
      </div>
      <div className='block md:hidden'>
        <Items
          className={className}
          items={trimmedItems}
          trimmed={trimmed}
          back={back}
        />
      </div>
    </>
  )
}
