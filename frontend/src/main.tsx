import React from 'react'
import { createRoot } from 'react-dom/client'
import './index.css'
import { tx } from '@transifex/native'
import { App } from './app'

tx.init({
  token: '1/76a8696dcdb28516e8669b86fe4af06e09595a4b',
})

// eslint-disable-next-line @typescript-eslint/no-non-null-assertion
createRoot(document.getElementById('root')!).render(<App />)
