import { Transition } from '@headlessui/react'
import { cx } from '@vechaiui/react'
import { ReactNode } from 'react'
import { QueryClient, QueryClientProvider } from 'react-query'
import { BrowserRouter, Route } from 'react-router-dom'
import { QueryParamProvider } from 'use-query-params'
import { Footer } from './components/footer'
import { Header } from './components/header'
import { Sidebar } from './components/sidebar'
import { useLocaleInit } from './hooks/locale-hooks'
import { Pages } from './pages/pages'
import { usePeriodicAuthTokenRefresh } from './stores/auth-store'
import { useSidebarStore } from './stores/sidebar-store'
import { ThemedVechaiProvider } from './theme-provider'
import { TransitionProps } from './transitions'

const Layout = ({ themeSwitcher }: { themeSwitcher: ReactNode }) => {
  const showMobileSidebar = useSidebarStore((s) => s.mobileOpen)
  return (
    <div className='flex flex-row h-full'>
      <Transition show appear {...TransitionProps.HorizontalSlide}>
        <Sidebar className='hidden md:flex h-screen sticky top-0' />
      </Transition>
      <div className='flex flex-col w-full h-full overflow-x-hidden'>
        <Header
          className='sticky top-0 z-40 w-full h-[4rem]'
          themeSwitcher={themeSwitcher}
        />
        <aside
          className={cx(
            'fixed top-[3.95rem] z-40 h-full w-full',
            'transition-all ease-in-out duration-300',
            {
              'opacity-0 -translate-x-full z-[-1]': !showMobileSidebar,
              'opacity-100 translate-x-0': showMobileSidebar,
            }
          )}
        >
          <Sidebar className='h-full w-full' mobile />
        </aside>
        <div className={cx('flex flex-col gap-y-5 flex-grow overflow-auto')}>
          <div className='p-3'>
            <Pages />
          </div>
          <Footer />
        </div>
      </div>
    </div>
  )
}

const client = new QueryClient()
export const App = () => {
  usePeriodicAuthTokenRefresh()
  useLocaleInit()

  return (
    <QueryClientProvider client={client}>
      <ThemedVechaiProvider>
        {(themeSwitcher) => (
          <BrowserRouter>
            <QueryParamProvider ReactRouterRoute={Route}>
              <Layout themeSwitcher={themeSwitcher} />
            </QueryParamProvider>
          </BrowserRouter>
        )}
      </ThemedVechaiProvider>
    </QueryClientProvider>
  )
}
