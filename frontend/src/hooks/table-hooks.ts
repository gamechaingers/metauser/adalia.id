import { ReactNode, useEffect, useMemo, useState } from 'react'
import { SortingRule, TableInstance } from 'react-table'
import {
  DelimitedArrayParam,
  NumberParam,
  UrlUpdateType,
  useQueryParam,
  withDefault,
} from 'use-query-params'
import { SortByParam } from '../custom-query-params'

export const usePaginationQueryParams = (defaultPageSize: number) => {
  const [pageIndex, setPageIndex] = useQueryParam(
    'page',
    withDefault(NumberParam, 1)
  )
  const [pageSize, setPageSize] = useQueryParam(
    'pageSize',
    withDefault(NumberParam, defaultPageSize)
  )

  return {
    pageIndex,
    setPageIndex,
    pageSize,
    setPageSize,
  }
}

export const useSortByQueryParam = <T extends object>() =>
  useQueryParam('sortBy', SortByParam<T>())

export const useSortBySync = <T extends object>(
  table: TableInstance<T>,
  setSortBy: (
    newSortBy: SortingRule<T> | undefined,
    updateType: UrlUpdateType
  ) => void
) => {
  const [initial, setInitial] = useState(true)
  useEffect(() => {
    const sb = table.state.sortBy
    const mode: UrlUpdateType = initial ? 'replaceIn' : 'pushIn'
    if (sb.length === 0) {
      setSortBy(undefined, mode)
    } else if (sb.length === 1) {
      setSortBy(sb[0], mode)
    }
    if (initial) {
      setInitial(false)
    }
  }, [JSON.stringify(table.state.sortBy)])
}

export const usePaginationSync = <T extends object>(
  table: TableInstance<T>,
  pageSizes: number[],
  setPageIndex: (pi: number, updateType: UrlUpdateType) => void,
  setPageSize: (ps: number, updateType: UrlUpdateType) => void
) => {
  const [initial, setInitial] = useState(true)
  useEffect(() => {
    const mode: UrlUpdateType = initial ? 'replaceIn' : 'pushIn'
    const ps = pageSizes.includes(table.state.pageSize)
      ? table.state.pageSize
      : 15

    setPageIndex(table.state.pageIndex + 1, mode)
    setPageSize(ps, 'replaceIn')
    if (initial) {
      setInitial(false)
    }
  }, [table.state.pageIndex, table.state.pageSize, pageSizes])
}

export const usePageCount = (
  totalCount: number | undefined,
  pageSize: number
) => {
  const [pageCount, setPageCount] = useState(0)

  useEffect(() => {
    if (totalCount) {
      setPageCount(Math.ceil(totalCount / pageSize))
    }
  }, [totalCount, pageSize])

  return pageCount
}

export const useTableColumns = <T extends object>(
  table: TableInstance<T>,
  config: { useQueryParams?: boolean }
) => {
  const tableColumns = useMemo(
    () => table.visibleColumns.map((c) => c.id),
    [table.visibleColumns.length]
  )
  const [cols, setCols] = useQueryParam('columns', DelimitedArrayParam)

  useEffect(() => {
    if (cols && config.useQueryParams) {
      const qpCols = cols.flatMap((c) => (c ? [c] : []))
      const allColumns = table.allColumns.map((c) => c.id)
      const hiddenColumns = allColumns.filter((c) => !qpCols.includes(c))
      table.setHiddenColumns(hiddenColumns)
    }
  }, [])

  useEffect(() => {
    if (config.useQueryParams) {
      setCols(tableColumns, 'replaceIn')
    }
  }, [tableColumns])
}

export const useStandardTableProps = <T>(initialPageSize: number) => {
  const [sortBy, setSortBy] = useState<SortingRule<T> | undefined>(undefined)
  const [pageSize, setPageSize] = useState(initialPageSize)
  const [pageIndex, setPageIndex] = useState(1)

  return { sortBy, setSortBy, pageSize, setPageSize, pageIndex, setPageIndex }
}

export type StandardTableProps<T> = {
  pageIndex: number
  setPageIndex: (pageIndex: number) => void
  pageSize: number
  setPageSize: (pageSize: number) => void
  sortBy: SortingRule<T> | undefined
  setSortBy: (sb: SortingRule<T> | undefined) => void
  pageSizes: number[]
  header?: (columnToggle: ReactNode) => ReactNode
  useQueryParamColumns?: boolean
}
