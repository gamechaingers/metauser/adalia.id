import { T } from '@transifex/react'
import { useMemo } from 'react'
import { Column } from 'react-table'
import { Crewmate } from '../api/adaliaIdSchemas'
import { Link } from '../components/link'
import { routes } from '../pages/routes'

const columns: Column<Crewmate>[] = [
  {
    Header: <T _str='ID' />,
    accessor: (c) => <Link to={routes.crewmate(c.id.toString())}>{c.id}</Link>,
    id: 'id',
  },
  {
    Header: <T _str='Name' />,
    accessor: (c) => c.name,
    id: 'name',
    disableSortBy: true,
  },
  {
    Header: <T _str='Owner' />,
    accessor: (c) => c.current_owner,
    id: 'owner',
    disableSortBy: true,
  },
  {
    Header: <T _str='Allegiance' />,
    accessor: (c) => c.allegiance,
    id: 'allegiance',
  },
]

export const useCrewmateTableColumns = () => useMemo(() => columns, [])
