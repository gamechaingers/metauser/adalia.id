import { useLanguages, useLocale, useTX } from '@transifex/react'
import { useEffect } from 'react'

export type Language = {
  name: string
  code: string
}
export type LocaleSettings = {
  language: Language
  languages: Language[]
  setLanguage: (languageCode: string) => void
}

const languageKey = 'language'

export const useLocaleSettings = () => {
  const languages: Language[] = useLanguages()
  const locale: string = useLocale()
  const tx = useTX()

  return {
    language: languages.find((l) => l.code === locale) ?? {
      name: '',
      code: 'en',
    },
    languages,
    setLanguage: (l) => tx.setCurrentLocale(l),
  } as LocaleSettings
}

const navigatorLanguage =
  {
    en_us: 'en',
    en_gb: 'en',
  }[navigator.language.toLowerCase()] ?? navigator.language
const savedLanguage = localStorage.getItem(languageKey)

export const useLocaleInit = () => {
  const { languages, setLanguage } = useLocaleSettings()
  const isSupported = (locale: string) =>
    languages.some((l) => l.code === locale)

  useEffect(() => {
    if (languages.length > 0)
      if (savedLanguage && isSupported(savedLanguage)) {
        setLanguage(savedLanguage)
      } else if (isSupported(navigatorLanguage)) {
        setLanguage(navigator.language)
      } else {
        setLanguage(languages[0].code)
      }
  }, [languages.length])
}
