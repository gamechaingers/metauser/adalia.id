import { T } from '@transifex/react'
import { useMemo } from 'react'
import { Column, SortingRule } from 'react-table'
import { AdaliaIdAsteroidsListQueryParams } from '../api/adaliaIdComponents'
import { Asteroid } from '../api/adaliaIdSchemas'
import { Link } from '../components/link'
import { useFormatters } from '../format'
import { routes } from '../pages/routes'

export const useAsteroidTableColumns = () => {
  const Format = useFormatters()
  return useMemo(
    (): Column<Asteroid>[] => [
      {
        Header: 'ID',
        accessor: (a) => (
          <Link to={routes.asteroid(a.id.toString())}>{a.id}</Link>
        ),
        id: 'id',
      },
      {
        Header: <T _str='Name' />,
        accessor: (a) => a.name,
        id: 'name',
        disableSortBy: true,
      },
      {
        Header: <T _str='Owner' />,
        accessor: (a) => a.current_owner,
        id: 'owner',
        disableSortBy: true,
      },
      {
        Header: <T _str='Allegiance' />,
        accessor: (a) => a.allegiance,
        id: 'allegiance',
      },
      {
        Header: <T _str='Spectral type' />,
        accessor: (a) => a.spectral_type,
        id: 'type',
      },
      {
        Header: <T _str='Diameter' />,
        accessor: (a) => (a.diameter ? Format.diameter(a.diameter) : null),
        id: 'diameter',
      },
      {
        Header: <T _str='Surface area' />,
        accessor: (a) => {
          if (a.diameter) {
            const surface = 4 * Math.PI * Math.pow(a.diameter / 1000 / 2, 2)
            return Format.surfaceArea(surface)
          }
        },
        id: 'surface',
      },
      {
        Header: <T _str='Orbital period' />,
        accessor: (a) =>
          a.orbital_period ? Format.orbitalPeriod(a.orbital_period) : null,
        id: 'op',
      },
      {
        Header: <T _str='Semi major axis' />,
        accessor: (a) =>
          a.semi_major_axis ? Format.semiMajorAxis(a.semi_major_axis) : null,
        id: 'sma',
      },
    ],
    [Format]
  )
}

export const useAsteroidOrdering = (
  sortBy: SortingRule<Asteroid> | undefined
) =>
  useMemo(() => {
    if (!sortBy) {
      return undefined
    }

    type Ordering = AdaliaIdAsteroidsListQueryParams['ordering']
    const field = {
      id: 'diameter',
      surface: 'diameter',
      diameter: 'diameter',
      owner: 'current_owner',
      name: 'name',
      op: 'orbital_period',
      sma: 'semi_major_axis',
      eccentricity: 'eccentricity',
      inclination: 'inclination',
    }[sortBy.id]

    if (field && sortBy.desc) {
      return ['-' + field] as Ordering
    } else if (field) {
      return [field] as Ordering
    }
    return []
  }, [sortBy])
