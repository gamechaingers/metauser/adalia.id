import { useMutation, UseMutationOptions } from 'react-query'
import { AdaliaIdContext, useAdaliaIdContext } from './adaliaIdContext'
import { adaliaIdFetch } from './adaliaIdFetcher'

export type WalletChallenge = {
  identifier: string
  message: string
}

export type SignedChallenge = {
  identifier: string
  signature: string
}

export type ChallengePathParams = {
  blockchain: string
  address: string
}
export type GenerateChallengeVariables = {
  pathParams: ChallengePathParams
} & AdaliaIdContext['fetcherOptions']

export const fetchWalletChallenge = (variables: GenerateChallengeVariables) =>
  adaliaIdFetch<WalletChallenge, {}, {}, {}, ChallengePathParams>({
    url: '/api/wallets/{blockchain}/{address}/challenge/generate',
    method: 'post',
    ...variables,
  })

export const useWalletChallenge = (
  options?: Omit<
    UseMutationOptions<{}, undefined, GenerateChallengeVariables>,
    'mutationFn'
  >
) => {
  const { fetcherOptions } = useAdaliaIdContext()
  return useMutation<WalletChallenge, undefined, GenerateChallengeVariables>(
    (variables: GenerateChallengeVariables) =>
      fetchWalletChallenge({ ...fetcherOptions, ...variables }),
    options
  )
}

export type WalletSuccess = {
  token: string
}

export type VerifyChallengeVariables = {
  pathParams: ChallengePathParams
  body: SignedChallenge
} & AdaliaIdContext['fetcherOptions']

export const fetchWalletVerify = (variables: VerifyChallengeVariables) =>
  adaliaIdFetch<WalletSuccess, SignedChallenge, {}, {}, ChallengePathParams>({
    url: '/api/wallets/{blockchain}/{address}/challenge/verify',
    method: 'post',
    ...variables,
  })

export const useWalletVerify = (
  options?: Omit<
    UseMutationOptions<{}, undefined, VerifyChallengeVariables>,
    'mutationFn'
  >
) => {
  const { fetcherOptions } = useAdaliaIdContext()
  return useMutation<WalletSuccess, undefined, VerifyChallengeVariables>(
    (variables: VerifyChallengeVariables) =>
      fetchWalletVerify({ ...fetcherOptions, ...variables }),
    options
  )
}
