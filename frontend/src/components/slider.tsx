import {
  Slider as RSlider,
  SliderRange,
  SliderThumb,
  SliderTrack,
} from '@radix-ui/react-slider'
import { cx } from '@vechaiui/react'
import { ClassNameProps } from '../default-props'

export type SliderProps = {
  value: number[]
  onChange: (value: number[]) => void
  min: number
  max: number
  step?: number
} & ClassNameProps
export const Slider = ({
  className,
  value,
  onChange,
  min,
  max,
  step,
}: SliderProps) => (
  <RSlider
    className={cx('relative flex items-center h-6 select-none', className)}
    min={min}
    max={max}
    step={step}
    value={value}
    onValueChange={onChange}
  >
    <SliderTrack className='w-full h-1 bg-neutral-300'>
      <SliderRange className='absolute h-1 rounded-full bg-primary-300' />
    </SliderTrack>
    {value.map((_, i) => (
      <SliderThumb
        key={i}
        className='block w-5 h-5 rounded-full border-2 border-neutral-300 focus:border-primary-500 bg-neutral-300'
      />
    ))}
  </RSlider>
)
