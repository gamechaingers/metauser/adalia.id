import { cx } from '@vechaiui/react'
import { ChildrenClassNameProps } from '../default-props'

export const Progress = ({
  value,
  max,
  className,
  indicatorClassName,
  children,
}: {
  value: number
  max?: number
  indicatorClassName?: string
} & ChildrenClassNameProps) => {
  const maxValue = max ?? 100
  const width = (value / maxValue) * 100

  return (
    <div className={cx('flex rounded-xl bg-neutral-700 relative', className)}>
      <div className='flex w-full h-full absolute justify-center items-center'>
        {children}
      </div>
      <div
        style={{ width: `${width}%` }}
        className={cx(
          'bg-primary-400 h-full rounded-l-2xl',
          {
            'rounded-r-2xl': value === maxValue,
          },
          indicatorClassName
        )}
      />
    </div>
  )
}
