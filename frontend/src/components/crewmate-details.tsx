import { Crewmate } from '../api/adaliaIdSchemas'

export const CrewmateDetails = ({ crewmate }: { crewmate: Crewmate }) => (
  <div>{crewmate.name}</div>
)
