import { cx } from '@vechaiui/react'
import { NavLink } from 'react-router-dom'
import { ChildrenClassNameProps } from '../default-props'
import { useSidebarStore } from '../stores/sidebar-store'

export type LinkProps = {
  activeClassName?: string
  to: string
  external?: boolean
  onClick?: () => void
  noStyle?: boolean
  nav?: boolean
} & ChildrenClassNameProps

export const Link = ({
  className,
  activeClassName,
  children,
  external = false,
  noStyle,
  to,
  nav,
  onClick,
}: LinkProps) => {
  const setMobileSidebarOpen = useSidebarStore((s) => s.setMobileOpen)

  return external ? (
    <a
      className={cx(
        noStyle ? '' : 'text-primary-400 hover:underline',
        className
      )}
      target='_blank'
      href={to}
    >
      {children}
    </a>
  ) : (
    <NavLink
      className={cx(
        noStyle ? '' : 'text-primary-400 hover:underline',
        className
      )}
      to={to}
      activeClassName={activeClassName}
      onClick={() => {
        setMobileSidebarOpen(false)
        if (onClick) {
          onClick()
        }
      }}
    >
      {children}
    </NavLink>
  )
}
