import { cx } from '@vechaiui/react'
import { ReactNode } from 'react'
import { ClassNameProps } from '../default-props'
import { Link } from './link'

export type TabItem = {
  title: ReactNode
  icon: ReactNode
  content?: ReactNode
  link?: string
}

export type TabsProps = {
  items: TabItem[]
} & ClassNameProps

export const Tabs = ({ items, className }: TabsProps) => (
  <div
    className={cx(
      'flex flex-row justify-start items-center flex-wrap',
      className
    )}
  >
    {items.map((tab, i) => (
      <div className='text-2xl cursor-pointer' key={i.toString()}>
        <Link
          nav
          noStyle
          to={tab.link ?? ''}
          className={cx(
            'px-4 py-2 flex flex-row space-x-3 items-center',
            'border-b border-neutral-700 hover:border-neutral-300'
          )}
          activeClassName='border-b-primary-400 border-b'
        >
          <div>{tab.icon}</div>
          <div>{tab.title}</div>
        </Link>
      </div>
    ))}
  </div>
)
