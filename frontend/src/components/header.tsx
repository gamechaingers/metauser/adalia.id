import {
  faBars,
  faChevronDown,
  faClose,
  faCog,
  faSignOutAlt,
  faUser,
  IconDefinition,
} from '@fortawesome/free-solid-svg-icons'
import { Menu } from '@headlessui/react'
import { T } from '@transifex/react'
import { Avatar, Button, cx, Divider } from '@vechaiui/react'
import { ReactNode } from 'react'
import { useLocation } from 'react-router-dom'
import { UserData } from '../auth'
import { Breadcrumb } from '../breadcrumb'
import { Icon } from '../components/icon'
import { ClassNameProps } from '../default-props'
import { useIsFullscreenPage } from '../pages/pages'
import { routes } from '../pages/routes'
import { useAuthStore } from '../stores/auth-store'
import { useSidebarStore } from '../stores/sidebar-store'
import { DropdownMenu } from './dropdow-menu'
import { LanguagePicker } from './language-picker'
import { Link } from './link'

interface MenuItemProps {
  children: ReactNode
  icon: IconDefinition
}
const MenuItem = ({ children, icon }: MenuItemProps) => (
  <Menu.Item>
    {() => (
      <div
        className={cx(
          'flex flex-row space-x-3 items-center flex-shrink-0',
          'w-full px-3 py-2 text-sm text-left',
          'hover:bg-fill cursor-base'
        )}
      >
        <Icon faIcon={icon} />
        {children}
      </div>
    )}
  </Menu.Item>
)

type UserMenuProps = {
  onLogout: () => void
}
const UserMenu = ({ onLogout }: UserMenuProps) => (
  <>
    <MenuItem icon={faUser}>
      <Link to={routes.profile} noStyle>
        <T _str='My Profile' />
      </Link>
    </MenuItem>
    <MenuItem icon={faCog}>
      <Link to={routes.settings} noStyle>
        <T _str='Settings' />
      </Link>
    </MenuItem>
    <Divider className='m-0' />
    <MenuItem icon={faSignOutAlt}>
      <Link to={routes.logout} onClick={onLogout} noStyle>
        <T _str='Sign out' />
      </Link>
    </MenuItem>
  </>
)

type HeaderProps = {
  themeSwitcher: ReactNode
} & ClassNameProps
export const Header = ({ themeSwitcher, className }: HeaderProps) => {
  const { userData, logout } = useAuthStore()
  const mobileSidebarOpen = useSidebarStore((s) => s.mobileOpen)
  const setFiltersOpen = useSidebarStore((s) => s.setFiltersOpen)
  const setMobileSidebarOpen = useSidebarStore((s) => s.setMobileOpen)
  const location = useLocation()
  const toggleMobileSidebar = () => {
    if (mobileSidebarOpen) {
      setMobileSidebarOpen(false)
    } else {
      setFiltersOpen(false)
      setMobileSidebarOpen(true)
    }
  }

  if (useIsFullscreenPage()) {
    return null
  }

  type LoggedInProps = {
    userData: UserData
  }
  const LoggedIn = ({ userData }: LoggedInProps) => (
    <DropdownMenu
      className='w-40 mr-1 right-0'
      button={
        <div className='flex flex-row space-x-3 items-center mr-3'>
          <Avatar name={userData.username} size='xl' />
          <span className='hidden md:flex'>{userData.username}</span>
          <Icon faIcon={faChevronDown} className='w-4 h-4 hidden md:inline' />
        </div>
      }
    >
      <UserMenu onLogout={logout} />
    </DropdownMenu>
  )

  const LoggedOut = () => (
    <div className='flex flex-row space-x-3 mr-3'>
      <Link to={routes.login(location.pathname + location.search)} noStyle>
        <Button variant='solid' color='primary'>
          <T _str='Log in' />
        </Button>
      </Link>
      <Link className='hidden sm:flex' to={routes.register} noStyle>
        <Button>
          <T _str='Sign up' />
        </Button>
      </Link>
    </div>
  )

  return (
    <nav
      className={cx('flex flex-row py-3 pl-3 bg-secondary text-xl', className)}
    >
      <div className='flex flex-row space-x-3 md:space-x-0 flex-grow items-center'>
        <Button className='flex md:hidden' onClick={toggleMobileSidebar}>
          <Icon
            className={cx(
              'transition-all ease-in-out duration-200',
              mobileSidebarOpen
                ? 'opacity-100 rotate-0'
                : 'opacity-0 w-0 rotate-180'
            )}
            faIcon={faClose}
          />
          <Icon
            className={cx(
              'transition-all ease-in-out duration-200',
              mobileSidebarOpen
                ? 'opacity-0 w-0 rotate-180'
                : 'opacity-100 rotate-0'
            )}
            faIcon={faBars}
          />
        </Button>
        <Breadcrumb className='flex-grow' />
      </div>
      {themeSwitcher}
      <LanguagePicker />
      <div className='flex ml-5'>
        {userData ? <LoggedIn userData={userData} /> : <LoggedOut />}
      </div>
    </nav>
  )
}
