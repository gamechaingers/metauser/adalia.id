import {
  faChevronDown,
  faClose,
  faFilter,
  faMinus,
  faPlus,
  faSave,
  faSearch,
  faTimes,
  faTrash,
} from '@fortawesome/free-solid-svg-icons'
import { Popover, Transition } from '@headlessui/react'
import { T, useT } from '@transifex/react'
import { Button, cx, Input } from '@vechaiui/react'
import React, { FormEvent, useState } from 'react'
import { ChildrenClassNameProps } from '../default-props'
import { useSidebarStore } from '../stores/sidebar-store'
import { TransitionProps } from '../transitions'
import { Icon } from './icon'
import { Slider } from './slider'

type RangeFilterProps = {
  value: [number, number]
  setValue: (value: [number, number]) => void
  min: number
  max: number
  step?: number
}
export const RangeFilter = ({
  value: [from, to],
  setValue,
  min,
  max,
  step,
}: RangeFilterProps) => (
  <div className='flex flex-col gap-y-2'>
    <Slider
      value={[from, to]}
      onChange={([from, to]) => setValue([from, to])}
      max={max}
      min={min}
      step={step}
    />
    <div className='flex flex-row gap-x-3 items-center'>
      <Input
        required
        value={from}
        onChange={(e) => setValue([parseFloat(e.target.value), to])}
        type='number'
        min={min}
        max={max}
        step={step}
      />
      <Icon faIcon={faMinus} />
      <Input
        required
        value={to}
        onChange={(e) => setValue([from, parseFloat(e.target.value)])}
        type='number'
        min={min}
        max={max}
        step={step}
      />
    </div>
  </div>
)

export const FilterBtn = ({
  className,
  mobile,
  children,
}: ChildrenClassNameProps & { mobile?: boolean }) => {
  const filters = useSidebarStore((s) => s.filters)
  const filtersOpen = useSidebarStore((s) => s.filtersOpen)
  const setFiltersOpen = useSidebarStore((s) => s.setFiltersOpen)
  const setMobileOpen = useSidebarStore((s) => s.setMobileOpen)
  return filters ? (
    <Button
      className={cx('gap-x-3 text-lg', className)}
      leftIcon={<Icon faIcon={faFilter} />}
      onClick={() => {
        if (mobile) {
          setFiltersOpen(true)
          setMobileOpen(true)
        } else {
          setFiltersOpen(!filtersOpen)
        }
      }}
    >
      {children}
    </Button>
  ) : null
}

export type FilterElement = {
  name: string
  unit?: string
  keywords: string[]
  element: JSX.Element
  active: boolean
  setActive: (active: boolean) => void
}

type FilterCardProps = {
  name: string
  unit?: string
  actionIcon: JSX.Element
  action: () => void
  defaultOpen?: boolean
} & ChildrenClassNameProps
const FilterCard = ({
  className,
  children,
  name,
  unit,
  actionIcon,
  action,
  defaultOpen = false,
}: FilterCardProps) => {
  const [open, setOpen] = useState(defaultOpen)
  const title = name + (unit ? ` [${unit}]` : '')
  return (
    <div
      className={cx(
        'flex flex-col gap-y-2',
        'bg-fill rounded-lg p-2 ease-in-out duration-200',
        className
      )}
    >
      <div className='flex flex-row gap-x-3 items-center'>
        <div
          className={cx('flex flex-row gap-x-3 items-center grow', {
            'cursor-pointer': !!children,
          })}
          onClick={() => setOpen(!open)}
        >
          {children && (
            <Icon
              faIcon={faChevronDown}
              className={cx('transition-all ease-in-out duration-200', {
                'transform -rotate-90': !open,
              })}
            />
          )}
          <span className='text-lg grow'>{title}</span>
        </div>
        <Button onClick={action}>{actionIcon}</Button>
      </div>
      <Transition show={open && !!children} {...TransitionProps.VerticalSlide}>
        {children}
      </Transition>
    </div>
  )
}

type AvailableFilterProps = {
  filters: FilterElement[]
  onAdd: () => void
}
const AvailableFilters = ({ filters, onAdd }: AvailableFilterProps) => {
  const [search, setSearch] = useState('')
  const s = search.trim().toLowerCase()
  const t = useT()
  const shownInactiveFilters = filters.filter(
    (f) =>
      s === '' ||
      [f.name, ...f.keywords].some((key) => key.toLowerCase().includes(s))
  )
  return (
    <div className='flex flex-col gap-y-3 max-h-[calc(100vh-15rem)]'>
      <h1>
        <T _str='Available filters' />
      </h1>
      <Input.Group>
        <Input.LeftElement>
          <Icon faIcon={faSearch} />
        </Input.LeftElement>
        <Input
          autoFocus
          value={search}
          onChange={(e) => setSearch(e.target.value)}
          placeholder={t('Search available filters')}
        />
      </Input.Group>
      <div className=' flex flex-col gap-y-3 overflow-y-auto'>
        {shownInactiveFilters.map((f) => (
          <FilterCard
            key={f.name}
            name={f.name}
            unit={f.unit}
            actionIcon={<Icon faIcon={faPlus} />}
            action={() => {
              onAdd()
              f.setActive(true)
            }}
          />
        ))}
      </div>
    </div>
  )
}

export type FiltersProps = {
  filters: FilterElement[]
  hasUnappliedChanges: boolean
  onApply: () => void
  onReset: () => void
}
export const Filters = ({
  filters,
  hasUnappliedChanges,
  onApply,
  onReset,
}: FiltersProps) => {
  const setFiltersOpen = useSidebarStore((s) => s.setFiltersOpen)
  const mobileOpen = useSidebarStore((s) => s.mobileOpen)
  const setMobileSidebarOpen = useSidebarStore((s) => s.setMobileOpen)
  const hasAvailableFilters = filters.some((f) => !f.active)

  const onSubmit = (e: FormEvent) => {
    e.preventDefault()
    if (mobileOpen) {
      setMobileSidebarOpen(false)
    }
    onApply()
  }
  const ApplyButton = () => (
    <Button
      className={cx('flex flex-row gap-x-3 text-lg grow', {
        'border !border-primary-500': hasUnappliedChanges,
      })}
      type='submit'
      disabled={!hasUnappliedChanges}
    >
      <Icon faIcon={faSave} />
      <T _str='Apply' />
    </Button>
  )
  const ResetButton = () => (
    <Button
      type='button'
      className='flex flex-row gap-x-3 text-lg grow'
      onClick={onReset}
    >
      <Icon faIcon={faTimes} />
      <T _str='Reset' />
    </Button>
  )
  const AvailableFilterPopover = () => (
    <Popover className='relative'>
      <Popover.Button as={React.Fragment}>
        <Button
          type='button'
          className='flex flex-row gap-x-3 text-lg grow'
          disabled={!hasAvailableFilters}
        >
          <Icon faIcon={faPlus} />
          <T _str='Add' />
        </Button>
      </Popover.Button>
      <Transition {...TransitionProps.Appear}>
        <Popover.Panel className='popover-panel right-0 w-[calc(100vw-1.5rem)] md:w-[22.5rem] mt-2'>
          {({ close }) => (
            <AvailableFilters
              filters={filters.filter((f) => !f.active)}
              onAdd={close}
            />
          )}
        </Popover.Panel>
      </Transition>
    </Popover>
  )
  const onClose = () => {
    if (mobileOpen) {
      setMobileSidebarOpen(false)
    } else {
      setFiltersOpen(false)
    }
  }
  const activeFilters = filters.filter((f) => f.active)
  return (
    <form
      onSubmit={onSubmit}
      className='h-full w-full bg-base py-4 flex flex-col gap-y-3'
    >
      <div className='flex flex-row items-center gap-x-3 px-3'>
        <h1 className='grow mb-0'>
          <T _str='Filters' />
        </h1>
        {!mobileOpen && (
          <Button type='button' onClick={onClose}>
            <Icon faIcon={faClose} />
          </Button>
        )}
      </div>
      <div className='flex flex-row gap-x-3 p-3'>
        <ApplyButton />
        <ResetButton />
        <AvailableFilterPopover />
      </div>
      <div className='overflow-y-auto px-3'>
        <div className='flex flex-col gap-y-3'>
          {activeFilters.length > 0 ? (
            activeFilters.map((f) => (
              <FilterCard
                defaultOpen
                key={f.name}
                actionIcon={<Icon faIcon={faTrash} />}
                action={() => f.setActive(false)}
                name={f.name}
                unit={f.unit}
              >
                {f.element}
              </FilterCard>
            ))
          ) : (
            <p>
              <T _str='You have no filters selected yet, try adding some and hit apply to update the table.' />
            </p>
          )}
        </div>
      </div>
    </form>
  )
}
