import {
  IconDefinition,
  faChevronUp,
  faGem,
  faMeteor,
  faUserCog,
  faUser,
  faTerminal,
  faExternalLinkAlt,
  faChartLine,
} from '@fortawesome/free-solid-svg-icons'
import { Disclosure, Transition } from '@headlessui/react'
import { T } from '@transifex/react'
import { cx } from '@vechaiui/react'
import { ChildrenProps } from '../default-props'
import { routes } from '../pages/routes'
import { useAuthStore } from '../stores/auth-store'
import { TransitionProps } from '../transitions'
import { Icon } from './icon'
import { Link } from './link'

type EntryItemProps = {
  href: string
  icon?: IconDefinition
  external?: boolean
} & ChildrenProps
const EntryItem = ({ children, href, icon, external }: EntryItemProps) => (
  <Link
    className='flex flex-row items-center space-x-3'
    to={href}
    external={external}
  >
    {icon && <Icon faIcon={icon} />}
    <span className='flex'>{children}</span>
  </Link>
)
type EntryProps = {
  title: string
  icon: IconDefinition
  defaultOpen?: boolean
  collapsed: boolean
} & ChildrenProps

const Entry = ({
  title,
  icon,
  defaultOpen,
  collapsed,
  children,
}: EntryProps) => {
  return (
    <Disclosure defaultOpen={defaultOpen}>
      {({ open }) => (
        <>
          <Disclosure.Button className='flex flex-row items-center space-x-3 text-left'>
            <Icon className={cx({ 'text-2xl': collapsed })} faIcon={icon} />
            {!collapsed && (
              <>
                <div className='text-xl flex-grow'>{title}</div>
                <Icon
                  className={`w-4 h-4 transition-transform ${
                    open ? 'rotate-0' : 'rotate-180'
                  }`}
                  faIcon={faChevronUp}
                />
              </>
            )}
          </Disclosure.Button>
          <Transition
            show={open && !collapsed}
            {...TransitionProps.VerticalSlide}
          >
            <Disclosure.Panel className='flex flex-col space-y-2 ml-6'>
              {children}
            </Disclosure.Panel>
          </Transition>
        </>
      )}
    </Disclosure>
  )
}

export const SidebarEntries = ({ collapsed }: { collapsed: boolean }) => {
  const { userData } = useAuthStore()
  return (
    <>
      <Entry title='All assets' icon={faGem} defaultOpen collapsed={collapsed}>
        <EntryItem href={routes.allAsteroids} icon={faMeteor}>
          <T _str='Asteroids' />
        </EntryItem>
        <EntryItem href={routes.allCrewmates} icon={faUserCog}>
          <T _str='Crewmates' />
        </EntryItem>
      </Entry>
      {userData && (
        <Entry title={userData.username} icon={faUser} collapsed={collapsed}>
          <EntryItem href={routes.dashboard} icon={faChartLine}>
            <T _str='Dashboard' />
          </EntryItem>
          <EntryItem href={routes.myAsteroids} icon={faMeteor}>
            <T _str='My Asteroids' />
          </EntryItem>
          <EntryItem href={routes.allCrewmates} icon={faUserCog}>
            <T _str='My Crewmates' />
          </EntryItem>
          <EntryItem href={routes.profile} icon={faUser}>
            <T _str='My Profile' />
          </EntryItem>
        </Entry>
      )}
      <Entry title='Community Apps' icon={faTerminal} collapsed={collapsed}>
        <EntryItem href='https://adalia.info' external>
          adalia.info
        </EntryItem>
        <EntryItem href='https://influence-sales.space' external>
          <T _str='influence-sales' />
        </EntryItem>
        <EntryItem href='https://tyrell-yutani.app' external>
          Tyrell Yutani
        </EntryItem>
      </Entry>
      <Entry title='Other Links' icon={faExternalLinkAlt} collapsed={collapsed}>
        <EntryItem href='https://influenceth.io' external>
          Influence
        </EntryItem>
        <EntryItem href='https://discord.gg/fhMN7C3JbK' external>
          Influence Discord
        </EntryItem>
        <EntryItem href='https://opensea.io' external>
          OpenSea
        </EntryItem>
        <EntryItem href='https://etherscan.io' external>
          EtherScan
        </EntryItem>
      </Entry>
    </>
  )
}
