import { Menu } from '@headlessui/react'
import ReactCountryFlag from 'react-country-flag'
import { DropdownMenu } from '../components/dropdow-menu'
import { useLocaleSettings } from '../hooks/locale-hooks'

const mapLocale = (locale: string) =>
  ({
    en: 'US',
    de: 'DE',
  }[locale] ?? locale)

const Flag = ({ locale }: { locale: string }) => (
  <ReactCountryFlag countryCode={mapLocale(locale)} />
)

export const LanguagePicker = () => {
  const { languages, language, setLanguage } = useLocaleSettings()
  return (
    <DropdownMenu
      className='-right-2'
      buttonClassName='flex flex-row gap-x-2 items-center h-full hover:text-primary-400'
      button={
        <>
          <Flag locale={language.code} />
          <span className='uppercase hidden md:flex xl:hidden'>
            {language.code}
          </span>
          <span className='hidden xl:flex'>{language.name}</span>
        </>
      }
    >
      <div className='flex flex-col'>
        {languages.map((lang: { code: string; name: string }) => (
          <Menu.Item key={lang.code}>
            <div
              className='cursor-pointer flex flex-row gap-x-3 items-center hover:bg-fill p-2 text-base'
              onClick={() => setLanguage(lang.code)}
            >
              <Flag locale={lang.code} />
              {lang.name}
            </div>
          </Menu.Item>
        ))}
      </div>
    </DropdownMenu>
  )
}
