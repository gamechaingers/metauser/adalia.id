import * as RTooltip from '@radix-ui/react-tooltip'
import { cx } from '@vechaiui/react'
import { ChildrenClassNameProps } from '../default-props'

export type TooltipProps = {
  content: JSX.Element
  withDelay?: boolean
} & ChildrenClassNameProps

export const Tooltip = ({
  content,
  withDelay,
  children,
  className,
}: TooltipProps) => (
  <RTooltip.Provider delayDuration={withDelay ? 500 : 0}>
    <RTooltip.Root>
      <RTooltip.Trigger asChild>{children}</RTooltip.Trigger>
      <RTooltip.Content
        className={cx(
          'bg-secondary border border-neutral-600',
          'shadow shadow-neutral-600',
          'p-2 mt-2 max-w-[50vw] rounded text-wrap whitespace-wrap ',
          className
        )}
      >
        {content}
      </RTooltip.Content>
    </RTooltip.Root>
  </RTooltip.Provider>
)
