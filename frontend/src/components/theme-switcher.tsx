import { faMoon, faSun } from '@fortawesome/free-solid-svg-icons'
import { Icon } from './icon'

export type ThemeSwitcherProps = {
  dark: boolean
  setDark: (dark: boolean) => void
}
export const ThemeSwitcher = ({ dark, setDark }: ThemeSwitcherProps) => (
  <div
    className='cursor-base flex flex-row space-x-3 items-center'
    onClick={() => setDark(!dark)}
  >
    <span className='hidden lg:flex'>Theme</span>
    <Icon className='flex text-2xl' faIcon={dark ? faMoon : faSun} />
  </div>
)
