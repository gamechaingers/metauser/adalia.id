import { useFormatters } from '../format'
import { useAsteroidFilterStore } from '../stores/asteroid-filter-store'
import { FilterTag, FilterTags } from './filter-tags'

export const AsteroidFilterTags = () => {
  const filters = useAsteroidFilterStore((s) => s.appliedFilters)
  const setActive = useAsteroidFilterStore((s) => s.setActive)
  const Format = useFormatters()
  const tags: FilterTag[] = [
    {
      name: 'Name',
      active: filters.name.active,
      value: filters.name.value,
      onRemove: () => setActive('name', false, true),
    },
    {
      name: 'Diameter',
      active: filters.diameter.active,
      value: Format.range(filters.diameter.value, Format.diameter),
      onRemove: () => setActive('diameter', false, true),
    },
    {
      name: 'Orbital period',
      active: filters.orbitalPeriod.active,
      value: Format.range(filters.orbitalPeriod.value, Format.orbitalPeriod),
      onRemove: () => setActive('orbitalPeriod', false, true),
    },
    {
      name: 'Inclination',
      active: filters.inclination.active,
      value: Format.range(filters.inclination.value, Format.inclination),
      onRemove: () => setActive('inclination', false, true),
    },
    {
      name: 'Eccentricity',
      active: filters.eccentricity.active,
      value: Format.range(filters.eccentricity.value, Format.eccentricity),
      onRemove: () => setActive('eccentricity', false, true),
    },
    {
      name: 'Semi major axis',
      active: filters.semiMajorAxis.active,
      value: Format.range(filters.semiMajorAxis.value, Format.semiMajorAxis),
      onRemove: () => setActive('semiMajorAxis', false, true),
    },
  ]

  return <FilterTags tags={tags} />
}
