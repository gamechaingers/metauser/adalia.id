import { faDiscord, faEthereum } from '@fortawesome/free-brands-svg-icons'
import { T } from '@transifex/react'
import { Button, cx, Divider, useNotification } from '@vechaiui/react'
import { ReactNode, useEffect, useRef } from 'react'
import { useHistory } from 'react-router-dom'
import { ChildrenClassNameProps, ClassNameProps } from '../default-props'
import { routes } from '../pages/routes'
import { WalletAuthState, useWalletAuth } from '../web3'
import { CustomIcon, Icon } from './icon'
import { Link } from './link'

export const AuthProviderButtons = ({ className }: ClassNameProps) => {
  const [tryLogin, loginState] = useWalletAuth()
  const ethButton = useRef<HTMLButtonElement>(null)
  const authPending = loginState == WalletAuthState.PENDING
  const notification = useNotification()
  const history = useHistory()

  useEffect(() => {
    if (loginState === WalletAuthState.SUCCESS) {
      history.push(routes.dashboard)
      return
    }
    let errorMsg = null
    switch (loginState) {
      case WalletAuthState.SIGNATURE_ERROR:
        errorMsg = 'Could not obtain signature'
        break
      case WalletAuthState.FAILED_AUTH:
        errorMsg = 'Signature is invalid'
        break
      case WalletAuthState.SERVER_ERROR:
        errorMsg = 'Server error'
        break
    }
    if (errorMsg) {
      notification({
        position: 'top',
        status: 'error',
        description: errorMsg,
        duration: null,
      })
    }
  }, [loginState])

  return (
    <div className={cx('flex flex-col space-y-3', className)}>
      <Button disabled={authPending}>
        <Icon className='text-xl mr-2' faIcon={faDiscord} />
        <T _str='Log in with Discord' />
      </Button>
      <Button ref={ethButton} onClick={tryLogin} disabled={authPending}>
        <Icon className='text-xl mr-2' faIcon={faEthereum} />
        <T _str='Log in with Etherum' />
      </Button>
    </div>
  )
}

export type LoginRegisterHeaderProps = {
  text: ReactNode
  subText?: ReactNode
}
export const LoginRegisterHeader = ({
  text,
  subText,
}: LoginRegisterHeaderProps) => (
  <div className='flex flex-col space-y-3 justify-center items-center'>
    <Link to={routes.home}>
      <Icon className='h-14' customIcon={CustomIcon.LogoMedium} />
    </Link>
    <div className='flex text-3xl'>{text}</div>
    {subText && <div className='flex text-lg'>{subText}</div>}
  </div>
)

export type LoginRegisterPageLayoutProps = {
  header: ReactNode
} & ChildrenClassNameProps
export const LoginRegisterPageLayout = ({
  children,
  header,
}: LoginRegisterPageLayoutProps) => (
  <div className='flex items-start justify-center h-full w-full'>
    <div className='flex flex-col space-y-8 w-96 md:mt-32'>
      {header}
      <div className='flex flex-col space-y-8 p-8 bg-secondary border border-neutral-600 rounded-2xl '>
        {children}
        <Divider />
        <AuthProviderButtons />
      </div>
    </div>
  </div>
)
