import {
  faAngleLeft,
  faAngleRight,
  IconDefinition,
} from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { T } from '@transifex/react'
import { cx, Select } from '@vechaiui/react'
import { TableInstance } from 'react-table'
import { ClassNameProps } from '../default-props'

type PageSizeSelectProps = {
  pageSize: number
  setPageSize: (pageSize: number) => void
  options: number[]
} & ClassNameProps
const PageSizeSelect = ({
  pageSize,
  setPageSize,
  options,
  className,
}: PageSizeSelectProps) => (
  <div className={cx('flex flex-row gap-x-2 items-center', className)}>
    {
      <span className='flex sm:flex'>
        <T _str='Rows per page:' />
      </span>
    }
    <Select
      size='sm'
      className='w-fit !pl-1 !pr-7'
      value={pageSize}
      onChange={(e) => setPageSize(parseInt(e.target.value, 10))}
    >
      {options.map((size) => (
        <option key={size} value={size}>
          {size}
        </option>
      ))}
    </Select>
  </div>
)

type TableNavigatorProps<D extends object> = {
  table: TableInstance<D>
}
const TableNavigator = <D extends object>({
  table,
}: TableNavigatorProps<D>) => {
  const border = 'border border-primary-600'
  const layout = 'p-1 min-w-[2rem] text-center'
  const clickable = 'cursor-pointer hover:bg-primary-800'
  const active = '!bg-primary-600 !opacity-100'
  const pageCount = table.pageCount
  const currentPage = table.state.pageIndex + 1
  const range = (from: number, to: number) =>
    [...Array(Math.max(to - from, 0)).keys()].map((i) => i + from)

  const pages = range(2, pageCount).filter((p) => {
    let diff = 2
    if (currentPage === 1 || currentPage === pageCount) {
      diff = 4
    } else if (currentPage === 2 || currentPage === pageCount - 1) {
      diff = 3
    }
    return Math.abs(p - currentPage) < diff
  })

  const first = pages.length > 0 ? pages[0] : 1
  const last = pages.length > 0 ? pages[pages.length - 1] : 1
  const leftGap = first > 2
  const rightGap = last < pageCount - 1

  const formatPage = (page: number) => {
    if (page === first && leftGap) {
      return `...${page}`
    } else if (page === pageCount && rightGap) {
      return `...${page}`
    } else {
      return page.toString()
    }
  }
  type PageBtnProps = {
    page: number
  }
  const PageBtn = ({ page }: PageBtnProps) => (
    <button
      disabled={currentPage === page}
      className={cx(border, layout, clickable, {
        [active]: currentPage === page,
      })}
      onClick={() => table.gotoPage(page - 1)}
    >
      {formatPage(page)}
    </button>
  )
  type NavBtnProps = {
    onClick: () => void
    disabled: boolean
    icon: IconDefinition
  } & ClassNameProps
  const NavBtn = ({ onClick, disabled, icon, className }: NavBtnProps) => (
    <button
      className={cx(
        border,
        layout,
        {
          [clickable]: !disabled,
          'opacity-50': disabled,
          'cursor-not-allowed': disabled,
        },
        className
      )}
      disabled={disabled}
      onClick={onClick}
    >
      <FontAwesomeIcon icon={icon} />
    </button>
  )

  const PrevBtn = () => (
    <NavBtn
      className='rounded-l-md'
      onClick={table.previousPage}
      disabled={!table.canPreviousPage}
      icon={faAngleLeft}
    />
  )
  const NextBtn = () => (
    <NavBtn
      className='rounded-r-md'
      onClick={table.nextPage}
      disabled={!table.canNextPage}
      icon={faAngleRight}
    />
  )
  return (
    <div className='flex flex-row'>
      <PrevBtn />
      <PageBtn page={1} />
      {pages.map((page) => (
        <PageBtn key={page} page={page} />
      ))}
      {pageCount > 1 && <PageBtn page={pageCount} />}
      <NextBtn />
    </div>
  )
}

type TablePaginatorProps<D extends object> = {
  table: TableInstance<D>
  pageSizes: number[]
  totalCount?: number
}
export const TablePaginator = <D extends object>({
  table,
  pageSizes,
  totalCount,
}: TablePaginatorProps<D>) => {
  const Nav = () => <TableNavigator table={table} />
  const Select = () => (
    <PageSizeSelect
      pageSize={table.state.pageSize}
      setPageSize={table.setPageSize}
      options={pageSizes}
    />
  )
  const Counts = ({ className }: ClassNameProps) => {
    const from = table.state.pageIndex * table.state.pageSize + 1
    const to = from + table.page.length - 1
    return totalCount ? (
      <span className={cx('italic', className)}>
        <T
          _str='Showing {from} to {to} of {totalCount} entries'
          from={from}
          to={to}
          totalCount={totalCount}
        />
      </span>
    ) : null
  }
  const baseClass = 'items-center py-3 px-3 text-base justify-center'
  return (
    <div
      className={cx(
        baseClass,
        'flex flex-col-reverse lg:flex-row lg:justify-end gap-x-7 gap-y-3'
      )}
    >
      <Counts className='flex grow' />
      <Select />
      <Nav />
    </div>
  )
}
