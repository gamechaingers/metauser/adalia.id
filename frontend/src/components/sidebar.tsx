import { Transition } from '@headlessui/react'
import { cx, Image } from '@vechaiui/react'
import { faSlash, faThumbtack } from '@fortawesome/free-solid-svg-icons'
import { useState } from 'react'
import { ClassNameProps } from '../default-props'
import { useIsFullscreenPage } from '../pages/pages'
import { TransitionProps } from '../transitions'
import { routes } from '../pages/routes'
import { useSidebarStore } from '../stores/sidebar-store'
import { Link } from './link'
import { Icon } from './icon'
import { SidebarEntries } from './sidebar-entries'

export interface Props extends ClassNameProps {
  mobile?: boolean
}
export const Sidebar = ({ className = '', mobile }: Props) => {
  const collapsed = useSidebarStore((s) => s.collapsed && !mobile)
  const setCollapsed = useSidebarStore((s) => s.setCollapsed)
  const pinned = useSidebarStore((s) => s.pinned)
  const setPinned = useSidebarStore((s) => s.setPinned)
  const filters = useSidebarStore((s) => s.filters)
  const filtersOpen = useSidebarStore((s) => s.filtersOpen)
  const [blockHover, setBlockHover] = useState(false)

  if (useIsFullscreenPage()) {
    return null
  }

  const onMouseEnter = () => {
    if (!blockHover) {
      setCollapsed(false)
    }
  }
  const onMouseLeave = () => {
    if (!pinned || filtersOpen) {
      setCollapsed(true)
    }
  }

  const togglePinned = () => {
    setPinned(!pinned)
    if (pinned) {
      setCollapsed(true)
      setBlockHover(true)
      setTimeout(() => setBlockHover(false), 200)
    }
  }

  const Header = () => (
    <div className='flex flex-row items-end gap-x-3 mb-2'>
      <Link className='grow' to={routes.home}>
        {collapsed ? (
          <Image className='h-10' src='/icon-128.png' />
        ) : (
          <Image className='object-fit h-12' src='/logo.png' />
        )}
      </Link>
      {!mobile && !collapsed && (
        <span
          className='fa-stack text-lg cursor-pointer'
          onClick={togglePinned}
        >
          <Icon className='fa-stack-1x mr-0' faIcon={faThumbtack} />
          {!pinned && (
            <Icon className='fa-stack-1x mr-0 rotate-180' faIcon={faSlash} />
          )}
        </span>
      )}
    </div>
  )

  return (
    <>
      {filters && filtersOpen && (
        <div className={cx('flex md:hidden w-full', className)}>{filters}</div>
      )}
      <div
        className={cx(
          'flex flex-row',
          { hidden: filtersOpen && filters },
          className
        )}
      >
        <div
          className={cx(
            'flex flex-col gap-y-3 bg-fill px-5 py-2 text-neutral-50 h-full w-full',
            'transition-all ease-in-out duration-200',
            {
              'w-80': !mobile && !collapsed,
              'w-20': collapsed,
            }
          )}
          onMouseEnter={onMouseEnter}
          onMouseLeave={onMouseLeave}
        >
          <Header />
          <Transition show appear {...TransitionProps.VerticalSlide}>
            <div
              className={cx('flex flex-col gap-y-2', {
                'items-center gap-y-6': collapsed,
              })}
            >
              <SidebarEntries collapsed={collapsed} />
            </div>
          </Transition>
        </div>
        <div
          className={cx(
            'border-r border-r-neutral-600 h-full',
            'transition-all ease-in-out duration-200',
            {
              'w-0 opacity-0': !filtersOpen,
              'w-96 opacity-100': filtersOpen,
            }
          )}
        >
          {filters}
        </div>
      </div>
    </>
  )
}
