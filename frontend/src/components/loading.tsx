import { cx } from '@vechaiui/react'
import { ReactNode } from 'react'
import { ClassNameProps } from '../default-props'
import { CustomIcon, Icon } from './icon'

export type LoadingProps = {
  text?: ReactNode
  visible?: boolean
  noLayout?: boolean
} & ClassNameProps

export const Loading = ({
  text,
  noLayout,
  visible = true,
  className,
}: LoadingProps) =>
  visible ? (
    <div
      className={cx(
        'flex flex-col animate-pulse duration-500 items-center justify-center',
        noLayout ? '' : 'w-full h-full',
        className
      )}
    >
      <Icon className='h-40 md:h-60' customIcon={CustomIcon.LogoLarge} />
      {text && <span className='text-2xl'>{text}</span>}
    </div>
  ) : null
