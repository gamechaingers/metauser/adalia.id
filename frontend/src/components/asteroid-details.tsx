import { cx, Image } from '@vechaiui/react'
import { Asteroid } from '../api/adaliaIdSchemas'
import { ClassNameProps } from '../default-props'
import { useFormatters } from '../format'
import { Progress } from './progress'

const imageUrl = (id: number) =>
  `https://images.influenceth.io/v1/asteroids/${id}/image.svg`

type PropertyProps<T> = {
  name: string
  value?: T | null
  formatter?: (value: T) => string
}

const Property = <T,>({ name, value, formatter }: PropertyProps<T>) => {
  let formatted = value ? `${value}` : null
  if (formatter && value) {
    formatted = formatter(value)
  }

  return formatted ? (
    <>
      <span className='text-primary-400 text-xl'>{name}</span>
      <span className='truncate' title={formatted}>
        {formatted}
      </span>
    </>
  ) : null
}

const Properties = ({ asteroid }: { asteroid: Asteroid }) => {
  const Format = useFormatters()
  return (
    <div
      className={cx(
        'gap-x-5 gap-y-2',
        'flex flex-col justify-center',
        '2xl:grid 2xl:grid-cols-2 2xl:items-center'
      )}
    >
      <Property name='Owner' value={asteroid.current_owner} />
      <Property name='Spectral type' value={asteroid.spectral_type} />
      <Property name='Eccentricity' value={asteroid.eccentricity} />
      <Property
        name='Orbital period'
        value={asteroid.orbital_period}
        formatter={Format.orbitalPeriod}
      />
      <Property
        name='Inclination'
        value={asteroid.inclination}
        formatter={Format.inclination}
      />
      <Property
        name='Semi major axis'
        value={asteroid.semi_major_axis}
        formatter={Format.semiMajorAxis}
      />
    </div>
  )
}

const AsteroidImage = ({
  asteroid,
  className,
}: { asteroid: Asteroid } & ClassNameProps) => {
  const url =
    asteroid.image_url && asteroid.image_url !== ''
      ? asteroid.image_url
      : imageUrl(asteroid.id)

  return <Image className={className} src={url} loading='eager' />
}

const KeyStat = ({
  value,
  max,
  title,
  formatter,
  background,
}: {
  value: number
  max: number
  title: string
  formatter: (v: number) => string
  background: string
}) => (
  <div className='flex flex-col gap-y-2 w-full md:w-72'>
    <div className='text-xl'>{title}</div>
    <Progress
      value={value}
      max={max}
      className='w-full h-5'
      indicatorClassName={background}
    >
      {formatter(value)}
    </Progress>
  </div>
)

const maxDiameter = 750_284
export const AsteroidDetails = ({ asteroid }: { asteroid: Asteroid }) => {
  const Format = useFormatters()
  return (
    <div className='flex flex-col gap-y-5'>
      <div className='flex flex-row flex-wrap gap-x-4 gap-y-2'>
        {asteroid.diameter && (
          <KeyStat
            value={asteroid.diameter}
            max={maxDiameter}
            title='Diameter'
            formatter={Format.diameter}
            background='bg-primary-700'
          />
        )}
      </div>
      <div className='flex flex-col lg:flex-row gap-5'>
        <AsteroidImage
          className='w-full md:w-96 xl:w-[40rem]'
          asteroid={asteroid}
        />
        <div className='truncate'>
          <Properties asteroid={asteroid} />
        </div>
      </div>
    </div>
  )
}
