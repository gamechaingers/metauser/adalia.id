import { T } from '@transifex/react'
import { useMemo } from 'react'
import { usePagination, useSortBy, useTable } from 'react-table'
import { useAdaliaIdCrewmatesList } from '../api/adaliaIdComponents'
import { Crewmate } from '../api/adaliaIdSchemas'
import { ClassNameProps } from '../default-props'
import { useCrewmateTableColumns } from '../hooks/crewmate-table-hooks'
import {
  StandardTableProps,
  usePageCount,
  usePaginationSync,
  useSortBySync,
} from '../hooks/table-hooks'
import { Table } from './table'

export type CrewmateTableProps = StandardTableProps<Crewmate> & ClassNameProps

export const CrewmatesTable = ({
  pageIndex,
  setPageIndex,
  pageSize,
  setPageSize,
  sortBy,
  setSortBy,
  pageSizes,
  header,
  className,
}: CrewmateTableProps) => {
  const { data, isFetching } = useAdaliaIdCrewmatesList(
    {
      queryParams: {
        offset: (pageIndex - 1) * pageSize,
        limit: pageSize,
      },
    },
    { keepPreviousData: true, refetchOnWindowFocus: false }
  )
  const crewmates = useMemo(() => data?.results ?? [], [data])
  const columns = useCrewmateTableColumns()

  const pageCount = usePageCount(data?.count, pageSize)
  const table = useTable(
    {
      data: crewmates,
      columns,
      pageCount,
      disableMultiSort: true,
      initialState: {
        pageSize,
        pageIndex: pageIndex - 1,
        sortBy: sortBy ? [sortBy] : [],
      },
      manualPagination: true,
    },
    useSortBy,
    usePagination
  )
  usePaginationSync(table, pageSizes, setPageIndex, setPageSize)
  useSortBySync(table, setSortBy)

  return (
    <Table
      className={className}
      table={table}
      paging={{
        pageSizes,
      }}
      header={header}
      loading={isFetching}
      loadingText={<T _str='Loading crewmates...' />}
      totalCount={data?.count}
      noDataText={<T _str='No crewmates found' />}
    />
  )
}
