import { Menu, Transition } from '@headlessui/react'
import { cx } from '@vechaiui/react'
import { ChildrenClassNameProps } from '../default-props'
import { TransitionProps } from '../transitions'

type DropdownMenuProps = {
  button: JSX.Element
  buttonClassName?: string
} & ChildrenClassNameProps
export const DropdownMenu = ({
  button,
  buttonClassName,
  className,
  children,
}: DropdownMenuProps) => (
  <Menu as='div' className='relative'>
    <Menu.Button className={buttonClassName}>{button}</Menu.Button>
    <Transition appear {...TransitionProps.Appear}>
      <Menu.Items
        className={cx(
          'absolute z-dropdown min-w-max mt-3 origin-top-right rounded-md shadow-sm outline-none bg-secondary border-neutral-500 border',
          className
        )}
      >
        <div>
          <div role='group'>{children}</div>
        </div>
      </Menu.Items>
    </Transition>
  </Menu>
)
