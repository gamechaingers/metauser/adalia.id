import { IconDefinition } from '@fortawesome/fontawesome-svg-core'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { cx, Image } from '@vechaiui/react'
import { ClassNameProps } from '../default-props'

export enum CustomIcon {
  LogoSmall = '/icon-128.png',
  LogoMedium = '/icon-256.png',
  LogoLarge = '/icon-512.png',
}
export interface IconProps extends ClassNameProps {
  faIcon?: IconDefinition
  customIcon?: CustomIcon
}
export const Icon = ({ faIcon, customIcon, className }: IconProps) => {
  if (faIcon) {
    return <FontAwesomeIcon className={className} icon={faIcon} />
  } else if (customIcon) {
    return (
      <Image
        className={cx('object-contain', className)}
        src={customIcon.toString()}
      />
    )
  } else {
    return null
  }
}
