import {
  ButtonProps as VButtonProps,
  Button as VButton,
  cx,
} from '@vechaiui/react'
import { IconDefinition } from '@fortawesome/fontawesome-svg-core'
import { ChildrenClassNameProps } from '../default-props'
import { Icon } from './icon'

export type ButtonProps = {
  faIcon?: IconDefinition
  responsiveIcon?: boolean
  danger?: boolean
} & ChildrenClassNameProps &
  VButtonProps

export const Button = ({
  className,
  children,
  faIcon,
  danger,
  responsiveIcon,
  ...props
}: ButtonProps) => (
  <VButton
    className={cx({ 'gap-x-3': !!faIcon, '!text-danger': danger }, className)}
    leftIcon={<Icon faIcon={faIcon} />}
    {...props}
  >
    {responsiveIcon ? (
      <span className='hidden md:flex'>{children}</span>
    ) : (
      children
    )}
  </VButton>
)
