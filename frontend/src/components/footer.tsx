import { T } from '@transifex/react'
import { Link } from './link'

export const Footer = () => {
  return (
    <div className='bg-secondary h-full text-muted flex flex-col space-y-3 p-3'>
      <div className='flex flex-row space-x-4 justify-center items-center'>
        <Link to='/tos'>
          <T _str='Terms of Service' />
        </Link>
        <Link to='/privacy'>
          <T _str='Privacy' />
        </Link>
        <Link to='/support'>
          <T _str='Support' />
        </Link>
        <Link to='https://status.gamechaingers.io/' external>
          <T _str='Status' />
        </Link>
      </div>
      <div className='text-center'>
        <T _str='© 2022 GameChaingers, LLC, All Rights Reserved.' />
      </div>
    </div>
  )
}
