import { faClose } from '@fortawesome/free-solid-svg-icons'
import { cx } from '@vechaiui/react'
import { ReactNode } from 'react'
import { Icon } from './icon'

export type FilterTag = {
  name: string
  value: ReactNode
  active: boolean
  onRemove?: () => void
}

const Tag = ({ name, value, onRemove }: FilterTag) => (
  <div
    className={cx(
      'flex flex-row gap-x-2 items-center px-4 py-2 ',
      'bg-fill rounded-3xl border border-primary-600'
    )}
  >
    <span className='font-bold'>{name + ':'}</span>
    {value}
    {onRemove && (
      <button className='flex' onClick={onRemove}>
        <Icon
          className='hover:text-primary-500 cursor-pointer text-xl'
          faIcon={faClose}
        />
      </button>
    )}
  </div>
)

export type FilterTagsProps = {
  tags: FilterTag[]
}
export const FilterTags = ({ tags }: FilterTagsProps) => {
  const activeTags = tags.filter((tag) => tag.active)
  return activeTags.length > 0 ? (
    <div className='flex flex-row gap-3 flex-wrap items-center'>
      {activeTags.map((tag) => (
        <Tag {...tag} key={tag.name} />
      ))}
    </div>
  ) : null
}
