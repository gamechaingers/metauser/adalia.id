import {
  faArrowDown,
  faArrowUp,
  faList,
} from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { Popover, Transition } from '@headlessui/react'
import { T } from '@transifex/react'
import { Button, Checkbox, cx } from '@vechaiui/react'
import React, { ReactNode, useCallback } from 'react'
import { TableInstance } from 'react-table'
import { ClassNameProps } from '../default-props'
import { TransitionProps } from '../transitions'
import { Icon } from './icon'
import { Loading } from './loading'
import { TablePaginator } from './table-paginator'

export type PagingSettings = {
  pageSizes: number[]
}
export type TableProps<D extends object> = {
  table: TableInstance<D>
  loading?: boolean
  loadingText: ReactNode
  canToggleColumns?: boolean
  header?: (columnToggle: ReactNode) => ReactNode
  paging?: PagingSettings
  totalCount?: number
  noDataText?: ReactNode
} & ClassNameProps

export const Table = <D extends object>({
  className,
  table,
  loading,
  totalCount,
  canToggleColumns = true,
  header,
  loadingText,
  paging,
  noDataText,
}: TableProps<D>) => {
  const rows = paging ? table.page : table.rows
  const noData = rows.length === 0
  const pagingLoad = loading && rows.length > 0

  const Head = () => (
    <thead className='bg-fill'>
      {table.headerGroups.map((group) => (
        <tr {...group.getHeaderGroupProps()}>
          {group.headers.map((header) => (
            <th
              {...header.getHeaderProps()}
              {...(header.canSort ? header.getSortByToggleProps() : {})}
              className={cx('text-xl text-left py-1 px-3 whitespace-nowrap', {
                'hover:text-primary-500': header.canSort,
              })}
            >
              {header.render('Header')}
              <FontAwesomeIcon
                className={cx(
                  'ml-2',
                  header.isSorted ? 'visible' : 'invisible'
                )}
                icon={header.isSortedDesc ? faArrowDown : faArrowUp}
              />
            </th>
          ))}
        </tr>
      ))}
    </thead>
  )
  const Body = () => (
    <tbody {...table.getTableBodyProps()}>
      {rows.map((row) => {
        table.prepareRow(row)
        return (
          <tr
            {...row.getRowProps()}
            className='border-y border-neutral-400 hover:bg-fill hover:bg-opacity-40'
          >
            {row.cells.map((cell) => (
              <td {...cell.getCellProps()} className='py-1 px-3'>
                {cell.render('Cell')}
              </td>
            ))}
          </tr>
        )
      })}
    </tbody>
  )

  const ColumnToggle = useCallback(
    () => (
      <div className='flex flex-col gap-y-1 '>
        {table.allColumns.map((c) => (
          <Checkbox
            size='lg'
            key={c.id}
            checked={c.isVisible}
            onChange={() => c.toggleHidden()}
          >
            {c.Header as ReactNode}
          </Checkbox>
        ))}
      </div>
    ),
    []
  )

  const Header = useCallback(
    () =>
      header ? (
        <div
          className={cx(
            'flex flex-row gap-x-3 gap-y-1 flex-wrap items-center',
            'bg-fill p-1 border-b border-neutral-400'
          )}
        >
          {header(
            <div>
              {canToggleColumns && (
                <Popover className='relative'>
                  <Popover.Button as={React.Fragment}>
                    <Button
                      className='gap-x-3 text-lg !bg-base'
                      type='button'
                      leftIcon={<Icon faIcon={faList} />}
                    >
                      <span className='hidden md:flex'>
                        <T _str='Columns' />
                      </span>
                    </Button>
                  </Popover.Button>
                  <Transition {...TransitionProps.Appear}>
                    <Popover.Panel className='popover-panel right-0 mt-1 w-48'>
                      <ColumnToggle />
                    </Popover.Panel>
                  </Transition>
                </Popover>
              )}
            </div>
          )}
        </div>
      ) : null,
    [header]
  )

  return (
    <div className={cx('flex flex-col bg-opacity-10 text-lg', className)}>
      <div className='relative'>
        {pagingLoad && (
          <Loading className='absolute left-1/3 top-1/3' noLayout />
        )}
        <Header />
        <div className='overflow-x-auto'>
          <table
            {...table.getTableProps()}
            className={cx('table-fixed min-w-full border-collapse', {
              'opacity-40': loading,
            })}
          >
            <Head />
            <Body />
          </table>
        </div>
      </div>
      {noData && (
        <div className='flex w-full items-center justify-center pt-2 pb-3'>
          {loading ? (
            <Loading text={loadingText} />
          ) : (
            <span>{noDataText ?? 'No data'}</span>
          )}
        </div>
      )}
      {paging && !noData && (
        <TablePaginator
          table={table}
          pageSizes={paging.pageSizes}
          totalCount={totalCount}
        />
      )}
    </div>
  )
}
