import { useT } from '@transifex/react'
import { Input } from '@vechaiui/react'
import { Constants } from '../constants'
import { useAsteroidFilterStore } from '../stores/asteroid-filter-store'
import { FilterElement, Filters, RangeFilter } from './filters'

export const AsteroidFilters = () => {
  const filters = useAsteroidFilterStore((s) => s.filters)
  const applyFilters = useAsteroidFilterStore((s) => s.applyFilters)
  const resetFilters = useAsteroidFilterStore((s) => s.resetFilters)
  const setActive = useAsteroidFilterStore((s) => s.setActive)
  const setValue = useAsteroidFilterStore((s) => s.setValue)
  const hasUnappliedChanges = useAsteroidFilterStore(
    (s) => s.hasUnappliedChanges
  )
  const t = useT()

  const elements: FilterElement[] = [
    {
      name: t('Name'),
      keywords: [],
      active: filters.name.active,
      setActive: (active) => setActive('name', active, false),
      element: (
        <Input
          placeholder='Asteroid name'
          value={filters.name.value}
          onChange={(e) => setValue('name', e.target.value)}
        />
      ),
    },
    {
      name: t('Diameter'),
      unit: 'm',
      keywords: ['size'],
      active: filters.diameter.active,
      setActive: (active) => setActive('diameter', active, false),
      element: (
        <RangeFilter
          value={filters.diameter.value}
          setValue={(value) => setValue('diameter', value)}
          min={Constants.minDiameter}
          max={Constants.maxDiameter}
          step={1}
        />
      ),
    },
    {
      name: t('Orbital period'),
      unit: t('days'),
      keywords: ['orbitals'],
      active: filters.orbitalPeriod.active,
      setActive: (active) => setActive('orbitalPeriod', active, false),
      element: (
        <RangeFilter
          value={filters.orbitalPeriod.value}
          setValue={(value) => setValue('orbitalPeriod', value)}
          min={Constants.minOrbitalPeriod}
          max={Constants.maxOrbitalPeriod}
          step={1}
        />
      ),
    },
    {
      name: t('Eccentricity'),
      keywords: ['orbitals'],
      active: filters.eccentricity.active,
      setActive: (active) => setActive('eccentricity', active, false),
      element: (
        <RangeFilter
          value={filters.eccentricity.value}
          setValue={(value) => setValue('eccentricity', value)}
          min={Constants.minEccentricity}
          max={Constants.maxEccentricity}
          step={0.001}
        />
      ),
    },
    {
      name: t('Inclination'),
      unit: t('degrees'),
      keywords: ['orbitals'],
      active: filters.inclination.active,
      setActive: (active) => setActive('inclination', active, false),
      element: (
        <RangeFilter
          value={filters.inclination.value}
          setValue={(value) => setValue('inclination', value)}
          min={Constants.minInclination}
          max={Constants.maxInclination}
          step={0.01}
        />
      ),
    },
    {
      name: t('Semi major axis'),
      unit: 'AU',
      keywords: ['orbitals'],
      active: filters.semiMajorAxis.active,
      setActive: (active) => setActive('semiMajorAxis', active, false),
      element: (
        <RangeFilter
          value={filters.semiMajorAxis.value}
          setValue={(value) => setValue('semiMajorAxis', value)}
          min={Constants.minSemiMajorAxis}
          max={Constants.maxSemiMajorAxis}
          step={0.001}
        />
      ),
    },
  ]

  return (
    <Filters
      filters={elements}
      onApply={applyFilters}
      onReset={resetFilters}
      hasUnappliedChanges={hasUnappliedChanges}
    />
  )
}
