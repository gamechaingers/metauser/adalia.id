import { T } from '@transifex/react'
import { useMemo } from 'react'
import { usePagination, useSortBy, useTable } from 'react-table'
import { useAdaliaIdAsteroidsList } from '../api/adaliaIdComponents'
import { Asteroid } from '../api/adaliaIdSchemas'
import { ClassNameProps } from '../default-props'
import {
  useAsteroidOrdering,
  useAsteroidTableColumns,
} from '../hooks/asteroid-table-hooks'
import {
  StandardTableProps,
  usePageCount,
  usePaginationSync,
  useSortBySync,
  useTableColumns,
} from '../hooks/table-hooks'
import { AsteroidFilters } from '../stores/asteroid-filter-store'
import { mapFilterValue } from '../stores/filter-store'
import { Table } from './table'

export type AsteroidTableProps = StandardTableProps<Asteroid> & {
  filters?: AsteroidFilters
  selfOwned?: boolean
  allianceControlled?: boolean
} & ClassNameProps

export const AsteroidTable = ({
  pageIndex,
  setPageIndex,
  pageSize,
  setPageSize,
  sortBy,
  setSortBy,
  pageSizes,
  filters,
  header,
  useQueryParamColumns,
  selfOwned,
  allianceControlled,
  className,
}: AsteroidTableProps) => {
  const ordering = useAsteroidOrdering(sortBy)
  const { data, isFetching } = useAdaliaIdAsteroidsList(
    {
      queryParams: {
        offset: (pageIndex - 1) * pageSize,
        limit: pageSize,
        orbital_period_min: mapFilterValue(filters?.orbitalPeriod, (v) => v[0]),
        orbital_period_max: mapFilterValue(filters?.orbitalPeriod, (v) => v[1]),
        diameter_min: mapFilterValue(filters?.diameter, (v) => v[0]),
        diameter_max: mapFilterValue(filters?.diameter, (v) => v[1]),
        eccentricity_min: mapFilterValue(filters?.eccentricity, (v) => v[0]),
        eccentricity_max: mapFilterValue(filters?.eccentricity, (v) => v[1]),
        inclination_min: mapFilterValue(filters?.inclination, (v) => v[0]),
        inclination_max: mapFilterValue(filters?.inclination, (v) => v[1]),
        semi_major_axis_min: mapFilterValue(
          filters?.semiMajorAxis,
          (v) => v[0]
        ),
        semi_major_axis_max: mapFilterValue(
          filters?.semiMajorAxis,
          (v) => v[1]
        ),
        name: mapFilterValue(filters?.name, (v) => v),
        user_owned: selfOwned,
        alliance_controlled: allianceControlled,
        ordering,
      },
    },
    { keepPreviousData: true, refetchOnWindowFocus: false }
  )
  const asteroids = useMemo(() => data?.results ?? [], [data])
  const columns = useAsteroidTableColumns()

  const pageCount = usePageCount(data?.count, pageSize)
  const table = useTable(
    {
      data: asteroids,
      columns,
      pageCount,
      disableMultiSort: true,
      initialState: {
        pageSize,
        pageIndex: pageIndex - 1,
        sortBy: sortBy ? [sortBy] : [],
      },
      manualPagination: true,
    },
    useSortBy,
    usePagination
  )
  usePaginationSync(table, pageSizes, setPageIndex, setPageSize)
  useSortBySync(table, setSortBy)
  useTableColumns(table, { useQueryParams: useQueryParamColumns })

  return (
    <Table
      className={className}
      table={table}
      paging={{
        pageSizes,
      }}
      loading={isFetching}
      loadingText={<T _str='Loading asteroids...' />}
      totalCount={data?.count}
      header={header}
      noDataText={<T _str='No asteroids found' />}
    />
  )
}
