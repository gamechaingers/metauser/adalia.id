import { SortingRule } from 'react-table'
import { decodeString, QueryParamConfig } from 'use-query-params'

export const SortByParam = <T>(): QueryParamConfig<
  SortingRule<T> | undefined
> => ({
  encode: (value) =>
    value ? `${value.id}:${value?.desc ? 'desc' : 'asc'}` : undefined,
  decode: (input) => {
    const str = decodeString(input)
    if (!str) {
      return undefined
    }
    const parts = str.split(':')
    if (parts.length == 2) {
      const [id, mode] = parts
      return {
        id,
        desc: mode == 'desc',
      }
    }
    return undefined
  },
})

export const NumberRangeParam: QueryParamConfig<[number, number] | undefined> =
  {
    decode: (input) => {
      const str = decodeString(input)
      if (str) {
        const parts = str
          .split('-')
          .map(parseFloat)
          .filter((v) => !isNaN(v))
        if (parts.length >= 2) {
          return [parts[0], parts[1]]
        }
      }
      return undefined
    },
    encode: (value) => (value ? `${value[0]}-${value[1]}` : undefined),
  }
