# Common Code Placement

- Status: accepted
- Deciders: Rebecca Murphy
- Date: 05-11-2021
- Tags: common-code

## Context and Problem Statement

Where is the best place to store commonly used code such that:

1. Separation of concerns is maintained
1. It remains easily accessible to all apps

## Considered Options

- A root level common directory for project-wide common code, and more specific common directories for more specific code that will still be reused between modules.
- utils.py in each module.

## Decision Outcome

Code will be stored in common directories to be denoted in the README.md.

All code in `utils.py` is to be moved to commons if it is referenced from outside its respective package.
