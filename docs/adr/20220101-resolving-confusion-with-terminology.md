# Resolving Confusion with Terminology

- Status: draft
- Deciders: Rebecca Murphy, Gareth Stonebraker
- Date: 2022-01-01
- Tags: terminology

## Context and Problem Statement

Overloaded terminology is getting confusing. For the benefit of us and our users, we should formally define some select terms.

## Decision Drivers

- Confusion

## Decision Outcome

Define the following terms:

### Entities

#### NFT

A non-fungible token. Common Influence NFTs include Asteroids and Crewmates.

#### Asset

NFTs and other Influence fungible or utility tokens, such as SWAY.

#### Wallet

The address of an ERC-20 compatible crypto wallet. A Wallet can hold any number of Assets.

#### Player

A person who has a my.adalia.id account. A Player can associate any number of Wallets with their my.adalia.id account.

#### Organization

An entity within my.adalia.id. Players may be members of any number of Organizations. Players may create Organizations so long as they have a unique Name and ID. Common Organizations might include localized player groups (i.e. "German Influencers"), loose in-game affiliations (i.e. "Trojan Asteroid Miners"), or other special interest groups (i.e. "LGBTQ+ Influencers").

#### Super Organization

An entity within my.adalia.id. It has all of the qualities of an Organization except Players may only be a member of one Super Organization. Players may request that a Super Organization be added to my.adalia.id. Super Organizations are Alliances (i.e. First Alliance), DAOs (i.e. YGG), and IRL Corporations (i.e. Unstoppable Games) in Influence. Super Organizations must have at minimum one Player member and a unique Discord Server.

#### Zone of Control

An attribute of a Super Organization that is inclusive of all Assets held in all Wallets that belong to a Super Organization and every Player that is a member of that Super Organization.
