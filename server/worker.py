import os

from celery import Celery
from celery.signals import setup_logging, worker_process_init
from opentelemetry.instrumentation.celery import CeleryInstrumentor

import django

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "server.settings.local")


from django.conf import settings  # noqa

if __name__ == "__main__":
    django.setup()

app = Celery("celery_tasks")
app.config_from_object("django.conf:settings")

app.conf.CELERY_ACCEPT_CONTENT = ["json"]
app.conf.CELERY_TASK_SERIALIZER = "json"
app.conf.CELERY_RESULT_SERIALIZER = "json"

app.autodiscover_tasks(lambda: settings.INSTALLED_APPS)

app.conf.ONCE = {
    "backend": "celery_once.backends.Redis",
    "settings": {"url": settings.BROKER_URL, "default_timeout": 60 * 60},
}


@worker_process_init.connect(weak=False)
def init_celery_tracing(*args, **kwargs):
    CeleryInstrumentor().instrument()


@setup_logging.connect
def config_loggers(*args, **kwargs):
    from logging.config import dictConfig

    from django.conf import settings

    dictConfig(settings.LOGGING)


if __name__ == "__main__":
    app.start()
else:
    CeleryInstrumentor().instrument()
