AUTH_USER_MODEL = "authentication.User"

# Password validation
# https://docs.djangoproject.com/en/3.1/ref/settings/#auth-password-validators

AUTH_PASSWORD_VALIDATORS = [
    {
        "NAME": "django.contrib.auth.password_validation.UserAttributeSimilarityValidator",
    },
    {
        "NAME": "django.contrib.auth.password_validation.MinimumLengthValidator",
    },
    {
        "NAME": "django.contrib.auth.password_validation.CommonPasswordValidator",
    },
    {
        "NAME": "django.contrib.auth.password_validation.NumericPasswordValidator",
    },
]


AUTHENTICATION_BACKENDS = [
    # Needed to login by username in Django admin, regardless of `allauth`
    "django.contrib.auth.backends.ModelBackend",
    # `allauth` specific authentication methods, such as login by e-mail
    "allauth.account.auth_backends.AuthenticationBackend",
]


LOGIN_REDIRECT_URL = "/"


###########
# Allauth #
###########

ACCOUNT_ADAPTER = "metauser.overrides.allauth.AccountAdapter"

ACCOUNT_FORMS = {
    "add_email": "metauser.overrides.allauth.AddEmailForm",
    "login": "metauser.overrides.allauth.LoginForm",
    "signup": "metauser.overrides.allauth.SignupForm",
    "change_password": "metauser.overrides.allauth.ChangePasswordForm",
    "reset_password": "metauser.overrides.allauth.ResetPasswordForm",
    "reset_password_from_key": "metauser.overrides.allauth.ResetPasswordKeyForm",
    "set_password": "metauser.overrides.allauth.SetPasswordForm",
}
