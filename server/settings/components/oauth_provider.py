import os

OAUTH2_PROVIDER = {
    "OIDC_ENABLED": True,
    "OIDC_RSA_PRIVATE_KEY": os.environ.get("OIDC_PRIVATE_KEY") or None,
    "SCOPES": {
        "openid": "OpenID (Identity Information)",
        "personal.profile": "Profile",
        "personal.assets.asteroids": "View Personal Asteroids",
        "personal.assets.crewmates": "View Personal Crewmates",
        "organization.assets.asteroids": "View Organization Asteroids",
        "organization.assets.crewmates": "View Organization Crewmates",
    },
    # ... any other settings you want
}
