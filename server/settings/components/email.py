if "config_file" not in locals():
    config_file = None

email = config_file.get("email", default=None)
if email:
    EMAIL_BACKEND = email["backend"]
    DEFAULT_FROM_EMAIL = email["from"]
    SERVER_EMAIL = email["from"]
    if EMAIL_BACKEND == "django.core.mail.backends.smtp.EmailBackend":
        EMAIL_HOST_USER = email["username"]
        EMAIL_HOST = email["host"]
        EMAIL_PORT = email["port"]
        EMAIL_USE_TLS = email["tls"]
        EMAIL_HOST_PASSWORD = email["password"]
    elif EMAIL_BACKEND == "anymail.backends.mailgun.EmailBackend":
        ANYMAIL = email["anymail"]
else:
    EMAIL_BACKEND = "django.core.mail.backends.console.EmailBackend"
