if "redis_url" not in locals():

    def redis_url(_):
        return None


if "config_file" not in locals():
    config_file = None

BROKER_URL = redis_url(0)
CELERY_TASK_ALWAYS_EAGER = config_file.get("celery", "always_eager", default=False)
CELERY_ALWAYS_EAGER = config_file.get("celery", "always_eager", default=False)
CELERY_EAGER_PROPAGATES = config_file.get("celery", "eager_propagates", default=False)
CELERY_RESULT_BACKEND = redis_url(0)
CELERYBEAT_SCHEDULER = "django_celery_beat.schedulers.DatabaseScheduler"


# CELERY_WORKER_HIJACK_ROOT_LOGGER = False
CELERYBEAT_SCHEDULE = {}
CELERYBEAT_SCHEDULE["asteroid_fetch"] = {
    "task": "my_adalia_id.tasks.fetch_fraction_asteroids",
    "schedule": 10,
}
CELERYBEAT_SCHEDULE["crewmate_fetch"] = {
    "task": "my_adalia_id.tasks.fetch_fraction_crewmates",
    "schedule": 10,
}
