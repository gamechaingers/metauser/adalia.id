PIPELINE = {
    "STYLESHEETS": {
        "main": {
            "source_filenames": (
                "diagnostics/lib/codemirror/codemirror.css",
                "metauser/css/*.css",
                "metauser/scss/*.css",
                "metauser/lib/*/css/**.css",
            ),
            "output_filename": "css/bundle.css",
            "extra_context": {
                "media": "screen,projection",
            },
        },
    },
    "JAVASCRIPT": {
        "main": {
            "source_filenames": (
                "notifications/notify.js",
                "diagnostics/lib/codemirror/codemirror.js",
                "diagnostics/lib/codemirror/modes/*.js",
                "metauser/js/**.js",
                # "metauser/lib/*/js/**.js",
                "ledger/js/templates/**/*.mustache",
            ),
            "output_filename": "js/main.js",
        }
    },
    "TEMPLATE_EXT": ".mustache",
    "TEMPLATE_FUNC": "Mustache.template",
    "TEMPLATE_NAMESPACE": "window.Template",
    "DISABLE_WRAPPER": True,
}
PIPELINE["CSS_COMPRESSOR"] = "pipeline.compressors.NoopCompressor"
PIPELINE["JS_COMPRESSOR"] = "pipeline.compressors.NoopCompressor"

PIPELINE["COMPILERS"] = ("pipeline.compilers.sass.SASSCompiler",)
