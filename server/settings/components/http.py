if "config_file" not in locals():
    config_file = None

ALLOWED_HOSTS = config_file.get("allowed_hosts", default=["localhost"])

INTERNAL_IPS = [
    # ...
    "127.0.0.1",
]
SECURE_SSL_REDIRECT = config_file.get("ssl", "force", default=False)

WSGI_APPLICATION = "server.wsgi.application"

SESSION_ENGINE = "django.contrib.sessions.backends.cached_db"

ROOT_URLCONF = "server.urls"
