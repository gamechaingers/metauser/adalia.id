if "redis_url" not in locals():

    def redis_url(_):
        return None


CACHES = {
    "default": {
        "BACKEND": "redis_cache.RedisCache",
        "LOCATION": redis_url(1),
    }
}

CACHEOPS_REDIS = redis_url(2)


CACHEOPS = {
    "authentication.user": {"ops": "get", "timeout": 60 * 15},
    # Automatically cache all gets and queryset fetches
    # to other django.contrib.auth models for an hour
    "auth.*": {"ops": {"fetch", "get"}, "timeout": 60 * 60},
    # Cache all queries to Permission
    # 'all' is an alias for {'get', 'fetch', 'count', 'aggregate', 'exists'}
    "auth.permission": {"ops": "all", "timeout": 60 * 60},
    "django.content_type": {"ops": {"fetch", "get"}, "timeout": 60 * 60},
    "currency.*": {"ops": {"fetch", "get"}, "timeout": 60 * 60},
    "organizations.*": {"ops": {"fetch", "get"}, "timeout": 60 * 60},
    "nfts.*": {"ops": {"all"}, "timeout": 60 * 60},
    "my_adalia_id.*": {"ops": {"all"}, "timeout": 60 * 60},
    # "*.*": {"timeout": 60 * 60},
    # NOTE: binding signals has its overhead, like preventing fast mass deletes,
    #       you might want to only register whatever you cache and dependencies.
    # Finally you can explicitely forbid even manual caching with:
}
