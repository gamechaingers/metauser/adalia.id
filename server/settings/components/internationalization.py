# Internationalization
# https://docs.djangoproject.com/en/3.2/topics/i18n/


LANGUAGE_CODE = "en-us"

TIME_ZONE = "UTC"

USE_I18N = True

USE_L10N = True

USE_TZ = True

LANGUAGE_COOKIE_NAME = "metauser_language"


LANGUAGES = [
    ("ar-ar", "Arabic"),
    ("de-de", "German"),
    ("en-us", "English (Simplified)"),
    ("en-uk", "English (Traditional)"),
    ("de-de", "German"),
    ("es-es", "Spanish"),
    ("fr-fr", "French"),
    ("ja-jp", "Japanese"),
    ("nl-nl", "Dutch"),
    ("pt-br", "Portuguese"),
    ("zh-cn", "Chinese"),
]
