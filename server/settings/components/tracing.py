# Configure the tracer using the default implementation
from os import environ

from opentelemetry import trace
from opentelemetry.exporter.datadog.exporter import DatadogSpanExporter
from opentelemetry.exporter.jaeger.thrift import JaegerExporter
from opentelemetry.instrumentation.django import DjangoInstrumentor
from opentelemetry.instrumentation.requests import RequestsInstrumentor
from opentelemetry.sdk.resources import Resource
from opentelemetry.sdk.trace import TracerProvider
from opentelemetry.sdk.trace.export import SimpleSpanProcessor

if "config_file" not in locals():
    config_file = {}
try:
    DjangoInstrumentor().instrument()
except Exception:
    pass
service = environ.get("SERVICE", "unknown")
resource = Resource.create({"service.name": service})

trace.set_tracer_provider(TracerProvider(resource=resource))
tracer_provider = trace.get_tracer_provider()

tracing_mode = config_file.get("tracing", "mode", default="jaeger")
if tracing_mode == "jaeger":
    jaeger_exporter = JaegerExporter(
        agent_host_name="localhost",
        agent_port=6831,
    )
    span_processor = SimpleSpanProcessor(jaeger_exporter)
if tracing_mode == "datadog":
    datadog_exporter = DatadogSpanExporter(service=service)
    span_processor = SimpleSpanProcessor(datadog_exporter)

tracer_provider.add_span_processor(span_processor)

RequestsInstrumentor().instrument()
