ORGS_SLUGFIELD = "autoslug.fields.AutoSlugField"
ORGS_INVITATION_BACKEND = (
    "metauser.authentication.organizations_backend.InvitationBackend"
)
ORGS_REGISTRATION_BACKEND = (
    "metauser.authentication.organizations_backend.RegistrationBackend"
)
