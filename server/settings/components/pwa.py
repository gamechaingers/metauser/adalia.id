PWA_APP_NAME = "my.adalia.id"
PWA_APP_DESCRIPTION = "Authentication for Adalia"
PWA_APP_THEME_COLOR = "#23408F"
PWA_APP_BACKGROUND_COLOR = "#000000"
PWA_APP_DISPLAY = "standalone"
PWA_APP_SCOPE = "/"
PWA_APP_ORIENTATION = "any"
PWA_APP_START_URL = "/"
PWA_APP_STATUS_BAR_COLOR = "#36a7cd"
PWA_APP_ICONS = [
    {"src": "/static/my_adalia_id/images/icon/icon-512.png", "sizes": "512x512"},
    {"src": "/static/my_adalia_id/images/icon/icon-256.png", "sizes": "256x256"},
    {"src": "/static/my_adalia_id/images/icon/icon-128.png", "sizes": "128x128"},
]
PWA_APP_ICONS_APPLE = [
    {"src": "/static/my_adalia_id/images/icon/icon-512.png", "sizes": "512x512"},
]
PWA_APP_SPLASH_SCREEN = [
    {
        "src": "/static/my_adalia_id/images/splash.png",
        "media": "(device-width: 320px) and (device-height: 568px) and (-webkit-device-pixel-ratio: 2)",
    }
]
# PWA_APP_DIR = "ltr"
PWA_APP_LANG = "en-US"
# PWA_SERVICE_WORKER_PATH = "metauser/service_worker.js"
