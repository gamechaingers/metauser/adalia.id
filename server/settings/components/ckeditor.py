CKEDITOR_CONFIGS = {
    "profile_editor": {
        "toolbar": "Custom",
        "toolbar_Custom": [
            ["Bold", "Italic", "Underline"],
            ["NumberedList", "BulletedList", "-", "Outdent", "Indent"],
            ["Link", "Unlink"],
            [
                "RemoveFormat",
            ],
        ],
    }
}
