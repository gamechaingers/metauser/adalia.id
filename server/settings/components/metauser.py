if "config_file" not in locals():
    config_file = None

IMPROVMX_API_KEY = config_file.get("improvmx_api_key", default=None)
PRIMARY_DOMAIN = config_file.get("primary_domain", default=None)
FEATURE_EMAIL_FORWARDING_ENABLED = config_file.get(
    "features", "email_forwarding", default=False
)
