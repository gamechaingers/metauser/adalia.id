from django.contrib import messages

if "config_file" not in locals():
    config_file = None
# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = config_file.values["secret_key"]
ENVIRONMENT = config_file.get("environment", default="Production")

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = config_file.values["debug"]


SHELL_PLUS = "bpython"


MESSAGE_TAGS = {messages.ERROR: "danger"}

HOME_TEMPLATE = "my_adalia_id/home.html"
