import sentry_sdk
from sentry_sdk.integrations.celery import CeleryIntegration
from sentry_sdk.integrations.django import DjangoIntegration
from sentry_sdk.integrations.redis import RedisIntegration

if "config_file" not in locals():
    config_file = None

SENTRY_DSN = config_file.get("sentry", "dsn", default=None)
if SENTRY_DSN:
    sentry_sdk.init(
        config_file.get("sentry", "dsn"),
        release=config_file.get("sentry", "release", default=None),
        environment=config_file.get("environment"),
        integrations=[CeleryIntegration(), DjangoIntegration(), RedisIntegration()],
        # Set traces_sample_rate to 1.0 to capture 100%
        # of transactions for performance monitoring.
        # We recommend adjusting this value in production.
        traces_sample_rate=float(
            config_file.get("sentry", "trace_sample_rate", default="1.0")
        ),
    )
