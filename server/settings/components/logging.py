if "config_file" not in locals():
    config_file = None
loggers = {
    k: {"level": v, "handlers": ["console"], "propagate": False}
    for k, v in config_file.get("logging", "loggers").items()
}

LOGGING = {
    "version": 1,
    "disable_existing_loggers": False,
    "formatters": {
        "verbose": {
            "()": "colorlog.ColoredFormatter",
            "format": "{log_color} {levelname} {asctime} {name} {message}",
            "style": "{",
            "log_colors": {
                "DEBUG": "bold_black",
                "INFO": "white",
                "WARNING": "yellow",
                "ERROR": "red",
                "CRITICAL": "bold_red",
            },
        },
    },
    "handlers": {
        "console": {"class": "logging.StreamHandler", "formatter": "verbose"},
    },
    "loggers": loggers,
}
