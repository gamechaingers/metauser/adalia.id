if "config_file" not in locals():
    config_file = None

INVITATIONS_INVITATION_ONLY = config_file.values.get("invitation_only", False)
INVITATIONS_ACCEPT_INVITE_AFTER_SIGNUP = True
INVITATIONS_ADAPTER = "metauser.overrides.allauth.AccountAdapter"
INVITATIONS_BACKEND = "metauser.authentication.invitations_backend.InvitationsBackend"
