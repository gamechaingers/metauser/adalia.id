import os

if "config_file" not in locals():
    config_file = None

if "BASE_DIR" not in locals():
    BASE_DIR = None

proxy = config_file.get("proxy", default={})

os.environ["http_proxy"] = proxy.get("http", "")
os.environ["https_proxy"] = proxy.get("https", "")


if config_file.exists("requests", "ca_bundle"):
    os.environ["REQUESTS_CA_BUNDLE"] = str(
        BASE_DIR / config_file.get("requests", "ca_bundle")
    )
