if "config_file" not in locals():
    config_file = None

provider_config = config_file.get("accounts", "social_providers", default={})

SOCIALACCOUNT_PROVIDERS = {}

if "discord" in provider_config:
    discord = provider_config["discord"]
    SOCIALACCOUNT_PROVIDERS["discord"] = {
        "VERIFIED_EMAIL": True,
        "APP": {"client_id": discord["id"], "secret": discord["secret"]},
    }
if "google" in provider_config:
    google = provider_config["google"]
    SOCIALACCOUNT_PROVIDERS["google"] = {
        "VERIFIED_EMAIL": True,
        "APP": {
            "client_id": google["id"],
            "secret": google["secret"],
        },
        "SCOPE": [
            "profile",
            "email",
        ],
        "AUTH_PARAMS": {
            "access_type": "online",
        },
    }
