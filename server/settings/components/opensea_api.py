if "config_file" not in locals():
    config_file = None

OPENSEA_API_KEY = config_file.get("opensea", "api", "key", default="")
