if "config_file" not in locals():
    config_file = None

COINBASE_BYPASS_NOTIFICATION_SIGNATURE_CHECK = config_file.get(
    "coinbase", "bypass_notification_signature_check", default=False
)
