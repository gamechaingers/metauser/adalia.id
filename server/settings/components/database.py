import dj_database_url

if "config_file" not in locals():
    config_file = None
# Database
# https://docs.djangoproject.com/en/3.1/ref/settings/#databases

DATABASES = {"default": dj_database_url.parse(config_file.values["database"]["url"])}


DEFAULT_AUTO_FIELD = "django.db.models.AutoField"
