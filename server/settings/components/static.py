import os

if "BASE_DIR" not in locals():
    BASE_DIR = None

# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/3.1/howto/static-files/

STATIC_URL = "/static/"
STATIC_ROOT = BASE_DIR / "staticfiles"

os.makedirs(STATIC_ROOT, exist_ok=True)

STATICFILES_STORAGE = "pipeline.storage.PipelineManifestStorage"

STATICFILES_FINDERS = (
    "django.contrib.staticfiles.finders.FileSystemFinder",
    "django.contrib.staticfiles.finders.AppDirectoriesFinder",
    "pipeline.finders.PipelineFinder",
)
