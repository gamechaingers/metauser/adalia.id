"""
Django settings for server project.

For more information on this file, see
https://docs.djangoproject.com/en/3.1/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/3.1/ref/settings/
"""

from split_settings.tools import include

include(
    "config_file.py",
    "common.py",
    "components/*.py",
)
