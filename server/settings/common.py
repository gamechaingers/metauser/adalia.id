from pathlib import Path

if "config_file" not in locals():
    config_file = None

# Build paths inside the project like this: BASE_DIR / 'subdir'.
BASE_DIR = Path(__file__).resolve().parent.parent.parent


def redis_url(index):
    if "urls" in config_file.values["redis"]:
        return config_file.values["redis"]["urls"][index] + "/0"
    return config_file.values["redis"]["url"] + f"/{index}"
