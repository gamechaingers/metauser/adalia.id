import logging
import os
import re
from pathlib import Path

import yaml

BASE_DIR = Path(__file__).resolve().parent.parent.parent

path_matcher = re.compile(r".*\$\{([^}^{]+)\}.*")

logger = logging.getLogger(__name__)


def _path_constructor(loader, node):
    return os.path.expandvars(node.value)


class EnvVarLoader(yaml.SafeLoader):
    pass


EnvVarLoader.add_implicit_resolver("!path", path_matcher, None)
EnvVarLoader.add_constructor("!path", _path_constructor)


undefined = object()


class ConfigFile:
    def __init__(self) -> None:

        with open(BASE_DIR / os.environ.get("CONFIG_FILE", "config/local.yaml")) as f:
            self.values = yaml.load(f, Loader=EnvVarLoader)

    def get(self, *path: str, default=undefined):
        def _default():
            if default is not undefined:
                return default
            else:
                raise KeyError("'" + ".".join(path) + "' not found in config.")

        remaining = self.values
        for element in path:
            if not isinstance(remaining, dict):
                raise KeyError(
                    "'"
                    + ".".join(path)
                    + "' is an invalid config path: trying to index a leaf config node."
                )
            if element in remaining:
                remaining = remaining[element]
            else:
                return _default()
        return remaining

    def exists(self, *path: str):

        remaining = self.values
        for element in path:
            if element not in remaining:
                return False
            remaining = remaining[element]
        return True


config_file = ConfigFile()
