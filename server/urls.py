from django.urls import include, path

from metauser import urls

urlpatterns = [
    path("", include(urls)),
]

handler400 = "metauser.views.bad_request"
handler403 = "metauser.views.permission_denied"
handler404 = "metauser.views.page_not_found"
handler500 = "metauser.views.server_error"
