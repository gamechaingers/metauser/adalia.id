from rest_framework import fields, serializers

from metauser.nfts.serializers import NFTAssetSerializer

from .models import Asteroid, Crewmate


class EnumField(fields.ChoiceField):
    def __init__(self, **kwargs):
        self.choices_forward = {k: v for k, v in kwargs["choices"]}
        self.choices_reverse = {v: k for k, v in kwargs["choices"]}
        kwargs.pop("max_length", None)
        super().__init__(**kwargs)

    def to_internal_value(self, data):
        try:
            return self.choices_reverse[data]
        except (KeyError, ValueError):
            pass

        self.fail("invalid_choice", input=data)

    def to_representation(self, value):
        if not value:
            return None

        return self.choices_forward[value]


class OwnerField(serializers.RelatedField):
    def to_representation(self, value):
        if value.owner_user:
            return str(value.owner_user)
        else:
            return str(value)


class AsteroidSerializer(NFTAssetSerializer):

    most_recent_valuation = serializers.ReadOnlyField()
    spectral_type = serializers.SerializerMethodField()

    def get_spectral_type(self, obj):
        return obj.get_spectral_type_display()

    class Meta:
        model = Asteroid
        fields = "__all__"


class CrewmateSerializer(NFTAssetSerializer):

    most_recent_valuation = serializers.ReadOnlyField()

    class Meta:
        model = Crewmate
        fields = "__all__"


InfluenceAssetSerializer = NFTAssetSerializer(
    {Asteroid: AsteroidSerializer, Crewmate: CrewmateSerializer}
)
