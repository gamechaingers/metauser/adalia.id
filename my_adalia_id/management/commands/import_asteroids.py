import json
import traceback
from decimal import Decimal

from pqdm.threads import pqdm

from django.core.management.base import BaseCommand

from metauser.nfts.models import NFTCollection
from metauser.wallets.models import Blockchain, Wallet

from my_adalia_id.models import Asteroid


class Command(BaseCommand):
    help = "Import asteroids"

    asteroid_coll, _ = NFTCollection.objects.get_or_create(
        symbol="INFA", defaults={"name": "Influence Asteroids"}
    )
    ethereum, _ = Blockchain.objects.get_or_create(
        symbol="ETH", defaults={"name": "Ethereum"}
    )

    def add_arguments(self, parser):
        parser.add_argument("filename", type=str)
        parser.add_argument(
            "--offset",
            type=int,
            help="Offset",
        )
        parser.add_argument(
            "--limit",
            type=int,
            help="limit",
        )

    @staticmethod
    def import_asteroid(line) -> None:
        try:
            asteroid_json = json.loads(line)
            asteroid_serial = int(asteroid_json["i"])
            asteroid_name = asteroid_json["name"]
            asteroid_owner = asteroid_json.get("owner")
            asteroid_radius = Decimal(asteroid_json["r"])
            asteroid_spectral_type = int(asteroid_json["spectralType"])
            semi_major_axis = Decimal(asteroid_json["orbital"]["a"])
            eccentricity = Decimal(asteroid_json["orbital"]["e"])
            inclination = Decimal(asteroid_json["orbital"]["i"])
            asset, _ = Asteroid.objects.get_or_create(
                collection=Command.asteroid_coll,
                serial=asteroid_serial,
                purchase_order=asteroid_serial,
                defaults={
                    "name": asteroid_name,
                    "diameter": asteroid_radius * 2,
                    "spectral_type": asteroid_spectral_type,
                    "semi_major_axis": semi_major_axis,
                    "eccentricity": eccentricity,
                    "inclination": inclination,
                },
            )

            if asteroid_owner:
                wallet, _ = Wallet.objects.get_or_create(
                    address=asteroid_owner.lower(), blockchain=Command.ethereum
                )
                asset.current_owner = wallet
                asset.save()
        except Exception:
            traceback.print_exc()

    def handle(self, *args, **options):

        filename = options["filename"]
        offset = options.get("offset", 0)
        limit = options.get("limit", 0)
        with open(filename, "r") as json_file:
            lines = json_file.readlines()
            if offset and offset > 0:
                self.stdout.write(f"Skipping {offset} records")
                del lines[0 : offset - 1]
            if limit and limit > 0:
                self.stdout.write(f"Importing up to {limit} records")
                del lines[limit:]
            pqdm(
                lines,
                Command.import_asteroid,
                n_jobs=8,
                desc="Importing Asteroids",
                unit="asteroids",
            )
