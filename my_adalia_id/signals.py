from metauser.nfts.signals import asset_refresh_requested
from metauser.utils import log_method

from .models import Asteroid, Crewmate
from .tasks import fetch_asteroid, fetch_crewmate


@log_method
def handle_asteroid_refresh_requested(sender, *, asset, **kwargs):
    fetch_asteroid.delay(asset.pk)


@log_method
def handle_crewmate_refresh_requested(sender, *, asset, **kwargs):
    fetch_crewmate.delay(asset.pk)


asset_refresh_requested.connect(
    handle_asteroid_refresh_requested,
    Asteroid,
    dispatch_uid="my_adalia_id.handle_asset_refresh_requested",
)
asset_refresh_requested.connect(
    handle_crewmate_refresh_requested,
    Crewmate,
    dispatch_uid="my_adalia_id.handle_asset_refresh_requested",
)
