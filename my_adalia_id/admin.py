from django.contrib import admin
from django.utils.translation import gettext_lazy as _

from my_adalia_id.models import Asteroid, Crewmate


@admin.action(description=_("Fetch selected asteroids from the Influence API"))
def fetch_asteroids(modeladmin, request, queryset):
    from . import tasks

    for asteroid in queryset:
        tasks.fetch_asteroid(asteroid_pk=asteroid.pk)


@admin.action(description=_("Fetch selected crewmates from the Influence API"))
def fetch_crewmates(modeladmin, request, queryset):
    from . import tasks

    for crewmate in queryset:
        tasks.fetch_crewmate(crewmate_pk=crewmate.pk)


@admin.register(Asteroid)
class AsteroidAdmin(admin.ModelAdmin):
    """Admin View for Asteroids"""

    actions = [fetch_asteroids]

    list_display = ("serial", "name")
    list_filter = ("spectral_type",)

    raw_id_fields = ("current_owner",)
    readonly_fields = ("orbital_period",)
    search_fields = ("name",)
    ordering = ("serial",)


@admin.register(Crewmate)
class CrewmateAdmin(admin.ModelAdmin):
    """Admin View for Crewmates"""

    actions = [fetch_crewmates]

    list_display = ("serial", "name")
    list_filter = ("crew_class",)

    raw_id_fields = ("current_owner",)
    search_fields = ("name",)
    ordering = ("serial",)
