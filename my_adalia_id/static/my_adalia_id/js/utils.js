function removeTrailingZeros(value) {
  value = value.toString();

  // if not containing a dot, we do not need to do anything
  if (value.indexOf(".") === -1) {
    return value;
  }

  // as long as the last character is a 0 or a dot, remove it
  while ((value.slice(-1) === "0" || value.slice(-1) === ".") && value.indexOf(".") !== -1) {
    value = value.substr(0, value.length - 1);
  }
  return value;
}
