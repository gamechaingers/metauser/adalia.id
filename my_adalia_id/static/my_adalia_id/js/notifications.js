function fill_notification_list_custom(data) {
  var menus = document.getElementsByClassName(notify_menu_class);
  if (menus) {
    var messages = data.unread_list
      .map(function (item) {
        var message = "";
        if (typeof item.actor !== "undefined") {
          message = item.actor;
        }
        if (typeof item.verb !== "undefined") {
          message = message + " " + item.verb;
        }
        if (typeof item.target !== "undefined") {
          message = message + " " + item.target;
        }
        if (typeof item.timestamp !== "undefined") {
          message =
            message +
            '<br><div class="notification-timestamp text-muted">' +
            item.timestamp +
            '</div><div class="dismiss"><a href="javascript:dismiss_notification(\'' +
            item.slug +
            "');\">X</a></div>";
        }
        return '<li class="list-group-item">' + message + "</li>";
      })
      .join("");

    if (data.unread_list.length == 0) {
      messages = '<div class="m-5 text-center">No Notifications</div>';
    }
    for (var i = 0; i < menus.length; i++) {
      menus[i].innerHTML = messages;
    }
  }
}

function dismiss_notification(slug) {
  $.ajax({
    url: Urls["notifications:mark_as_read"](slug),
  });
}
