Mustache.template = function (templateString) {
  return function () {
    if (arguments.length < 1) {
      return templateString;
    } else {
      return Mustache.render(templateString, arguments[0], arguments[1]);
    }
  };
};
