$(document).ready(function () {
  var owner_param = "";
  if (window?.owner_id) {
    owner_param = "&owner=" + window.owner_id;
  }
  var table = $("table#accounts").DataTable({
    serverSide: true,
    ajax: Urls["ledger_api:account_list"]() + "?format=datatables" + owner_param,
    columns: [
      {
        className: "no-border-right icon-column",
        data: "icon_css",
        render: function (data, type) {
          return '<span class="' + data + ' h3"></span>';
        },
      },
      {
        className: "no-border-right",
        data: "name",
      },
      {
        className: "no-border-left action-column",
        data: "id",
        render: function (data, type) {
          return '<a class="btn btn-primary btn-sm btn-square" href="' + Urls["ledger:view_account"](data) + '"><span class="fas fa-ellipsis-h"></span></a>';
        },
      },
    ],
    columnDefs: [{ sortable: false, targets: [0, 2] }],
    order: [[1, "asc"]],
  });
});
