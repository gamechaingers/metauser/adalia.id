import pytest

from django.conf import settings

SITE_NAME = getattr(settings, "SITE_NAME")


@pytest.mark.django_db
@pytest.mark.parametrize(
    "route,expected_content",
    [
        ("/", ""),
        ("/aup", "This acceptable use policy covers the products"),
        ("/changelog", "Changelog"),
        ("/cookies", "We use cookies to help improve your experience"),
        ("/privacy", "Your privacy is important to us."),
        ("/terms", "These Terms of Service govern your use"),
    ],
)
def test_static_endpoint(client, route, expected_content):
    response = client.get(route)
    body = response.content.decode("utf-8")
    assert expected_content in body
    assert SITE_NAME in body


@pytest.mark.django_db
def test_get_asteroids(client):
    response = client.get("/api/adalia_id/asteroids")
    _ = response.json


@pytest.mark.django_db
def test_get_crewmates(client):
    response = client.get("/api/adalia_id/crewmates")
    _ = response.json
