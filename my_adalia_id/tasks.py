from datetime import datetime

from celery import shared_task
from celery.utils.log import get_task_logger
from influence_api import InfluenceClient
from requests.exceptions import HTTPError

from metauser.nfts.ingest_opensea import tasks as ingest_opensea
from metauser.nfts.models import NFTCollection
from metauser.utils import log_exceptions, log_method
from metauser.wallets.models import Blockchain, Wallet

from .app_settings import ASTEROID_REFRESH_FRACTION, CREWMATE_REFRESH_FRACTION
from .models import Asteroid, Crewmate

ASTEROID_REFRESH_INTERVAL = 86400 / ASTEROID_REFRESH_FRACTION
CREWMATE_REFRESH_INTERVAL = 86400 / CREWMATE_REFRESH_FRACTION

logger = get_task_logger("my_adalia_id")

try:
    client = InfluenceClient()
except HTTPError:
    client = None


@shared_task
@log_method
@log_exceptions
def fetch_asteroid(asteroid_pk):
    ethereum = Blockchain.objects.get(symbol="ETH")
    logger.info(f"Fetching asteroid {asteroid_pk}")
    asteroid = Asteroid.objects.get(pk=asteroid_pk)
    api_asteroid = client.get_asteroid(asteroid.serial)
    asteroid.name = api_asteroid["name"]
    asteroid.eccentricity = api_asteroid["orbital"]["e"]
    asteroid.semi_major_axis = api_asteroid["orbital"]["a"]
    asteroid.inclination = api_asteroid["orbital"]["i"]
    asteroid.ascending_node = api_asteroid["orbital"]["o"]
    asteroid.periapsis = api_asteroid["orbital"]["w"]
    asteroid.anomaly = api_asteroid["orbital"]["m"]
    asteroid.purchase_order = api_asteroid.get("purchaseOrder") or -1
    if "owner" in api_asteroid and (
        not asteroid.current_owner
        or asteroid.current_owner.address.lower() != api_asteroid["owner"].lower()
    ):
        asteroid.current_owner, _ = Wallet.objects.get_or_create(
            blockchain=ethereum, address=api_asteroid["owner"].lower()
        )
    asteroid.save()
    ingest_opensea.get_asset_attributes.delay(asset_pk=asteroid.pk)


@shared_task
@log_method
@log_exceptions
def fetch_crewmate(crewmate_pk=None, crewmate_serial=None):
    ethereum = Blockchain.objects.get(symbol="ETH")
    collection = NFTCollection.objects.get(symbol="INFC")
    logger.info(f"Fetching crewmate {crewmate_pk}/{crewmate_serial}")
    if crewmate_pk:
        crewmate = Crewmate.objects.get(pk=crewmate_pk)
    else:
        crewmate, _ = Crewmate.objects.get_or_create(
            serial=crewmate_serial, collection=collection
        )
    api_crewmate = client.get_crewmate(crewmate.serial)
    crewmate.gender = "M" if api_crewmate["sex"] == 1 else "F"
    crewmate.body = api_crewmate["body"]
    crewmate.crew_class = api_crewmate["crewClass"]
    crewmate.title = api_crewmate["title"]
    crewmate.outfit = api_crewmate["outfit"]
    crewmate.hair = api_crewmate["hair"]
    crewmate.facial_feature = api_crewmate["facialFeature"]
    crewmate.hair_color = api_crewmate["hairColor"]
    crewmate.head_piece = api_crewmate["headPiece"]
    crewmate.bonus_item = api_crewmate["bonusItem"]

    crewmate.name = api_crewmate.get("name") or "Unnamed Crewmate"
    if "owner" in api_crewmate and (
        not crewmate.current_owner
        or crewmate.current_owner.address.lower() != api_crewmate["owner"].lower()
    ):
        crewmate.current_owner, _ = Wallet.objects.get_or_create(
            blockchain=ethereum, address=api_crewmate["owner"].lower()
        )
    crewmate.save()
    ingest_opensea.get_asset_attributes.delay(asset_pk=crewmate.pk)


@shared_task
@log_method
@log_exceptions
def fetch_fraction_asteroids():
    current_interval = int(
        int(datetime.now().timestamp())
        // ASTEROID_REFRESH_INTERVAL
        % ASTEROID_REFRESH_FRACTION
    )
    for asteroid in Asteroid.objects.all():
        slot = asteroid.pk % ASTEROID_REFRESH_FRACTION
        if slot == current_interval:
            logger.debug(
                f"Fetching asteroid={asteroid.serial}: Scheduled for slot {slot}, current slot {current_interval}."
            )
            fetch_asteroid.delay(asteroid_pk=asteroid.pk)


@shared_task
@log_method
@log_exceptions
def fetch_fraction_crewmates():
    current_interval = int(
        int(datetime.now().timestamp())
        // CREWMATE_REFRESH_INTERVAL
        % CREWMATE_REFRESH_FRACTION
    )
    for crewmate in Crewmate.objects.all():
        slot = crewmate.pk % CREWMATE_REFRESH_FRACTION
        if slot == current_interval:
            logger.debug(
                f"Fetching crewmate={crewmate.serial}: Scheduled for slot {slot}, current slot {current_interval}."
            )
            fetch_crewmate.delay(crewmate_pk=crewmate.pk)


@shared_task
@log_method
@log_exceptions
def fetch_all_crewmates(cursor=None):
    if cursor is None:
        cursor = 1
    try:
        fetch_crewmate(crewmate_serial=cursor)
        fetch_all_crewmates.apply_async(countdown=1, kwargs={"cursor": cursor + 1})
    except HTTPError:
        pass
