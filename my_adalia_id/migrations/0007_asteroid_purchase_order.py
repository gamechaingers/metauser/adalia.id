# Generated by Django 3.2.12 on 2022-03-03 00:30

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ("my_adalia_id", "0006_alter_asteroid_spectral_type"),
    ]

    operations = [
        migrations.AddField(
            model_name="asteroid",
            name="purchase_order",
            field=models.IntegerField(default=-1, verbose_name="purchase order"),
        ),
    ]
