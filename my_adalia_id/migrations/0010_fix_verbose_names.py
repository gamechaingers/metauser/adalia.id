# Generated by Django 3.2.12 on 2022-04-01 17:10

from decimal import Decimal

import django.core.validators
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ("my_adalia_id", "0009_asteroid_surface_area"),
    ]

    operations = [
        migrations.AlterField(
            model_name="asteroid",
            name="anomaly",
            field=models.DecimalField(
                blank=True,
                decimal_places=3,
                max_digits=6,
                null=True,
                verbose_name="mean anomaly at epoch",
            ),
        ),
        migrations.AlterField(
            model_name="asteroid",
            name="inclination",
            field=models.DecimalField(
                blank=True,
                decimal_places=2,
                max_digits=4,
                null=True,
                validators=[
                    django.core.validators.MaxValueValidator(Decimal("37.77")),
                    django.core.validators.MinValueValidator(Decimal("0.01")),
                ],
                verbose_name="inclination",
            ),
        ),
        migrations.AlterField(
            model_name="asteroid",
            name="orbital_period",
            field=models.DecimalField(
                blank=True,
                decimal_places=3,
                max_digits=7,
                null=True,
                validators=[django.core.validators.MinValueValidator(0)],
                verbose_name="period",
            ),
        ),
        migrations.AlterField(
            model_name="asteroid",
            name="periapsis",
            field=models.DecimalField(
                blank=True,
                decimal_places=3,
                max_digits=6,
                null=True,
                verbose_name="argument of periapsis",
            ),
        ),
        migrations.AlterField(
            model_name="crewmate",
            name="gender",
            field=models.CharField(
                choices=[("M", "Male"), ("F", "Female"), ("-", "Unknown")],
                default=" ",
                help_text="Though the Influence API refers to this as `sex`, we use more inclusive language.",
                max_length=1,
                verbose_name="gender identity",
            ),
        ),
    ]
