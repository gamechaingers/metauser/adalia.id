from cacheops import CacheopsLibrary, cached

register = CacheopsLibrary()


def get_menu_subkey(user):
    if user.is_staff:
        return "staff"
    if user.is_authenticated:
        return "authed"
    return "guest"


@register.decorator_tag(takes_context=True)
def cache_menu(context, menu_name):
    from django.utils import translation

    request = context.get("request")
    # if request and request.user.is_staff:
    #     # Use noop decorator to bypass caching for staff
    #     return lambda func: func

    return cached(
        # Invalidate cache if any menu item or a flag for menu changes
        # Vary for menu name and language, also stamp it as "menu" to be safe
        extra=(
            request.path_info,
            "menu",
            menu_name,
            get_menu_subkey(request.user),
            translation.get_language(),
        ),
        timeout=24 * 60 * 60,
    )
