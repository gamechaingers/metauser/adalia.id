# Create your tests here.
import pytest

from metauser.authentication.models import User
from metauser.currency.models import Currency


@pytest.fixture
def any_currency():
    currency, _ = Currency.objects.get_or_create(
        symbol="USD", name="USD", digits_behind_decimal=2
    )
    return currency


@pytest.fixture
def any_user(any_currency):
    user, _ = User.objects.get_or_create(
        username="Test", password="Password", base_currency=any_currency
    )
    return user
