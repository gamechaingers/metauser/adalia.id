from django.db import models


class BaseModel(models.Model):
    class Meta:
        abstract = True


class CleanOnSave(BaseModel):
    class Meta:
        abstract = True

    def save(self, *args, **kwargs) -> None:
        self.full_clean()
        super().save(*args, **kwargs)
