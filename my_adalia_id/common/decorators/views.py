from functools import wraps

from django.conf import settings
from django.core.exceptions import ObjectDoesNotExist
from django.http.response import Http404
from django.utils.translation import gettext_lazy as _
from django.views.decorators.cache import cache_page

DEBUG = getattr(settings, "DEBUG", False)

STATIC_RESOURCE_CACHE_TTL = getattr(settings, "STATIC_RESOURCE_CACHE_TTL", 3600)


def static_file_cache(view):
    if DEBUG:
        return view
    else:
        return cache_page(STATIC_RESOURCE_CACHE_TTL)(view)


def auto_not_found(func):
    @wraps(func)
    def auto_not_found_wrapper(*args, **kwargs):
        try:
            return func(*args, **kwargs)
        except ObjectDoesNotExist:
            raise Http404(_("Couldn't find requested object."))

    return auto_not_found_wrapper
