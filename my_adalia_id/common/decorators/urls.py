from django.conf.urls import include


def decorate_url_patterns(decorator):
    def func(urls):
        url_list, app_name, namespace = include(urls)

        def process_patterns(url_patterns):
            for pattern in url_patterns:
                if hasattr(pattern, "url_patterns"):
                    # this is an include - apply to all nested patterns
                    process_patterns(pattern.url_patterns)
                else:
                    # this is a pattern
                    pattern.callback = decorator(pattern.callback)

        process_patterns(url_list)
        return url_list, app_name, namespace

    return func
