from django.db.models import Sum
from django.utils.translation import gettext_lazy as _

from django_filters import rest_framework as filters
from rest_framework import viewsets
from rest_framework.decorators import action
from rest_framework.response import Response

from metauser.nfts.viewsets import NFTAssetFilterSet

from my_adalia_id.models import Asteroid, Crewmate

from .constants import ASTEROID_SPECTRAL_TYPES
from .serializers import AsteroidSerializer, CrewmateSerializer


class AsteroidFilterSet(NFTAssetFilterSet):

    name = filters.CharFilter()
    serial = filters.RangeFilter()
    current_owner = filters.CharFilter(
        field_name="current_owner__owner_user__username", lookup_expr="icontains"
    )

    diameter = filters.RangeFilter()
    ascending_node = filters.RangeFilter()
    eccentricity = filters.RangeFilter()
    semi_major_axis = filters.RangeFilter()
    orbital_period = filters.RangeFilter()
    inclination = filters.RangeFilter()
    anomaly = filters.RangeFilter()
    periapsis = filters.RangeFilter()

    ordering = filters.OrderingFilter(
        fields=(
            ("name", "name"),
            ("serial", "serial"),
            ("current_owner", "current_owner"),
            ("diameter", "diameter"),
            ("ascending_node", "ascending_node"),
            ("eccentricity", "eccentricity"),
            ("semi_major_axis", "semi_major_axis"),
            ("orbital_period", "orbital_period"),
            ("inclination", "inclination"),
            ("anomaly", "anomaly"),
            ("periapsis", "periapsis"),
        )
    )

    class Meta:
        model = Asteroid
        fields = (
            "name",
            "serial",
            "current_owner",
            "diameter",
            "ascending_node",
            "eccentricity",
            "semi_major_axis",
            "orbital_period",
            "inclination",
            "anomaly",
            "periapsis",
        )


class AsteroidViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = Asteroid.objects.all()
    serializer_class = AsteroidSerializer
    filterset_fields = [
        "serial",
        "name",
        "ascending_node",
        "eccentricity",
        "orbital_period",
    ]
    search_fields = ["name"]
    filterset_class = AsteroidFilterSet
    lookup_field = "serial"

    @action(detail=False, methods=["get"], name=_("View a summary of asteroids"))
    def summary(self, request, **kwargs):
        queryset = self.get_queryset()
        queryset = self.filter_queryset(queryset)

        queryset = queryset.values("spectral_type").annotate(
            surface_area=Sum("surface_area")
        )

        serialized_q = [
            {
                "spectral_type": ASTEROID_SPECTRAL_TYPES[t["spectral_type"]],
                "surface_area": t["surface_area"],
            }
            for t in list(queryset)
        ]

        return Response(serialized_q)

    @action(detail=False, methods=["get"], name=_("View early-adopter asteroids"))
    def early_adopter(self, request, **kwargs):
        queryset = self.get_queryset().filter(
            purchase_order__gte=0, purchase_order__lt=1861
        )

        queryset = self.filter_queryset(queryset)

        page = self.paginate_queryset(queryset)
        if page is not None:
            serializer = self.get_serializer(page, many=True)
            return self.get_paginated_response(serializer.data)

        serializer = self.get_serializer(queryset, many=True)
        return Response(serializer.data)

    @action(detail=False, methods=["get"], name=_("View Alliance controlled assets"))
    def alliance_control(self, request, **kwargs):
        if request.user.is_authenticated and request.user.allegiance:
            queryset = self.get_queryset().filter(
                current_owner__owner_user__allegiance=request.user.allegiance,
            )
        else:
            queryset = self.get_queryset().none()
        queryset = self.filter_queryset(queryset)

        page = self.paginate_queryset(queryset)
        if page is not None:
            serializer = self.get_serializer(page, many=True)
            return self.get_paginated_response(serializer.data)

        serializer = self.get_serializer(queryset, many=True)
        return Response(serializer.data)

    @action(detail=False, methods=["get"], name=_("View personally owned assets"))
    def personal(self, request, **kwargs):
        if request.user.is_authenticated:
            queryset = self.get_queryset().filter(
                current_owner__owner_user=request.user,
            )
        else:
            queryset = self.get_queryset().none()
        queryset = self.filter_queryset(queryset)

        page = self.paginate_queryset(queryset)
        if page is not None:
            serializer = self.get_serializer(page, many=True)
            return self.get_paginated_response(serializer.data)

        serializer = self.get_serializer(queryset, many=True)
        return Response(serializer.data)


class CrewmateViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = Crewmate.objects.all()
    serializer_class = CrewmateSerializer
    filterset_fields = ["serial", "name"]
    search_fields = ["name"]
    filterset_class = NFTAssetFilterSet
    lookup_field = "serial"

    @action(detail=False, methods=["get"], name=_("View Alliance controlled assets"))
    def alliance_control(self, request, **kwargs):
        if request.user.is_authenticated and request.user.allegiance:
            queryset = self.get_queryset().filter(
                current_owner__owner_user__allegiance=request.user.allegiance,
            )
        else:
            queryset = self.get_queryset().none()
        queryset = self.filter_queryset(queryset)

        page = self.paginate_queryset(queryset)
        if page is not None:
            serializer = self.get_serializer(page, many=True)
            return self.get_paginated_response(serializer.data)

        serializer = self.get_serializer(queryset, many=True)
        return Response(serializer.data)

    @action(detail=False, methods=["get"], name=_("View personally owned assets"))
    def personal(self, request, **kwargs):
        if request.user.is_authenticated:
            queryset = self.get_queryset().filter(
                current_owner__owner_user=request.user,
            )
        else:
            queryset = self.get_queryset().none()
        queryset = self.filter_queryset(queryset)

        page = self.paginate_queryset(queryset)
        if page is not None:
            serializer = self.get_serializer(page, many=True)
            return self.get_paginated_response(serializer.data)

        serializer = self.get_serializer(queryset, many=True)
        return Response(serializer.data)
