from decimal import Decimal

from .constants import FOUR, PI, THIRD_LAW, THREE, TWO


def calculate_orbital_period(semi_major_axis: Decimal) -> Decimal:
    """Calculates the orbital period

    Args:
        semi_major_axis: semi major axis of asteroid

    Returns:
        orbital period
    """
    return Decimal.sqrt(semi_major_axis**THREE / THIRD_LAW)


def calculate_surface_area(diameter):
    return FOUR * PI * ((diameter / TWO) ** TWO)
