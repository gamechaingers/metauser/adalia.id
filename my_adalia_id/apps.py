from importlib import import_module

from my_adalia_id.common.apps import BaseAppConfig


class MyAdaliaIdAppConfig(BaseAppConfig):
    name = "my_adalia_id"
    verbose_name = "my.adalia.id"

    def ready(self) -> None:
        super().ready()
        import_module("my_adalia_id.signals")
