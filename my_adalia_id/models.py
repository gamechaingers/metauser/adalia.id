from decimal import Decimal

from django.core.validators import MaxValueValidator, MinValueValidator
from django.db import models
from django.utils.translation import gettext_lazy as _

from metauser.nfts.models import NFTAsset

from .constants import (
    ASTEROID_ORBITAL_ECCENTRICITY_MAXIMUM,
    ASTEROID_ORBITAL_ECCENTRICITY_MINIMUM,
    ASTEROID_ORBITAL_INCLINATION_MAXIMUM,
    ASTEROID_ORBITAL_INCLINATION_MINIMUM,
    ASTEROID_ORBITAL_SEMI_MAJOR_AXIS_MAXIMUM,
    ASTEROID_ORBITAL_SEMI_MAJOR_AXIS_MINIMUM,
    ASTEROID_SIZE_MAXIMUM,
    ASTEROID_SIZE_MINIMUM,
    ASTEROID_SPECTRAL_TYPES,
)
from .utils import calculate_orbital_period, calculate_surface_area


class Asteroid(NFTAsset):
    diameter = models.IntegerField(
        validators=[
            MaxValueValidator(ASTEROID_SIZE_MAXIMUM),
            MinValueValidator(ASTEROID_SIZE_MINIMUM),
        ],
        verbose_name=_("diameter"),
        null=True,
        blank=True,
    )
    surface_area = models.IntegerField(
        verbose_name=_("surface area"),
        null=True,
        blank=True,
    )

    spectral_type = models.IntegerField(
        choices=list(ASTEROID_SPECTRAL_TYPES.items()),
        verbose_name=_("spectral type"),
        null=True,
        blank=True,
    )

    semi_major_axis = models.DecimalField(
        validators=[
            MaxValueValidator(ASTEROID_ORBITAL_SEMI_MAJOR_AXIS_MAXIMUM),
            MinValueValidator(ASTEROID_ORBITAL_SEMI_MAJOR_AXIS_MINIMUM),
        ],
        verbose_name=_("semi-major axis"),
        null=True,
        blank=True,
        decimal_places=3,
        max_digits=4,
    )

    eccentricity = models.DecimalField(
        validators=[
            MaxValueValidator(ASTEROID_ORBITAL_ECCENTRICITY_MAXIMUM),
            MinValueValidator(ASTEROID_ORBITAL_ECCENTRICITY_MINIMUM),
        ],
        decimal_places=3,
        max_digits=6,
        verbose_name=_("eccentricity"),
        null=True,
        blank=True,
    )

    inclination = models.DecimalField(
        validators=[
            MaxValueValidator(ASTEROID_ORBITAL_INCLINATION_MAXIMUM),
            MinValueValidator(ASTEROID_ORBITAL_INCLINATION_MINIMUM),
        ],
        decimal_places=2,
        max_digits=4,
        verbose_name=_("inclination"),
        null=True,
        blank=True,
    )

    ascending_node = models.DecimalField(
        validators=[
            # MaxValueValidator(ASTEROID_ORBITAL_INCLINATION_MAXIMUM),
            # MinValueValidator(ASTEROID_ORBITAL_INCLINATION_MINIMUM),
        ],
        decimal_places=3,
        max_digits=6,
        verbose_name=_("ascending node"),
        null=True,
        blank=True,
    )

    anomaly = models.DecimalField(
        validators=[
            # MaxValueValidator(ASTEROID_ORBITAL_INCLINATION_MAXIMUM),
            # MinValueValidator(ASTEROID_ORBITAL_INCLINATION_MINIMUM),
        ],
        decimal_places=3,
        max_digits=6,
        verbose_name=_("mean anomaly at epoch"),
        null=True,
        blank=True,
    )

    periapsis = models.DecimalField(
        validators=[
            # MaxValueValidator(ASTEROID_ORBITAL_INCLINATION_MAXIMUM),
            # MinValueValidator(ASTEROID_ORBITAL_INCLINATION_MINIMUM),
        ],
        decimal_places=3,
        max_digits=6,
        verbose_name=_("argument of periapsis"),
        null=True,
        blank=True,
    )
    orbital_period = models.DecimalField(
        validators=[
            # MaxValueValidator(ASTEROID_ORBITAL_INCLINATION_MAXIMUM),
            MinValueValidator(0),
        ],
        decimal_places=3,
        max_digits=7,
        verbose_name=_("period"),
        null=True,
        blank=True,
    )
    purchase_order = models.IntegerField(verbose_name=_("purchase order"), default=-1)

    def _update_orbital_period(self):
        if self.semi_major_axis is not None:
            self.orbital_period = calculate_orbital_period(
                Decimal(self.semi_major_axis)
            )

    def _update_surface_area(self):
        if self.diameter is not None:
            self.surface_area = int(
                calculate_surface_area(Decimal(self.diameter) / 1000)
            )

    def save(self, *args, **kwargs):
        self._update_orbital_period()
        self._update_surface_area()
        return super().save(*args, **kwargs)


class Crewmate(NFTAsset):
    crew_collection = models.IntegerField(verbose_name=_("crew collection"), default=-1)
    gender = models.CharField(
        verbose_name=_("gender identity"),
        max_length=1,
        choices=[("M", _("Male")), ("F", _("Female")), ("-", _("Unknown"))],
        default=" ",
        help_text=_(
            "Though the Influence API refers to this as `sex`, we use more inclusive language."
        ),
    )
    body = models.IntegerField(verbose_name=_("body"), default=-1)
    crew_class = models.IntegerField(
        verbose_name=_("crew class"),
        default=-1,
        choices=[
            (-1, "(Unknown)"),
            (1, "Pilot"),
            (2, "Engineer"),
            (3, "Miner"),
            (4, "Merchant"),
            (5, "Scientist"),
        ],
    )
    title = models.IntegerField(
        verbose_name=_("title"),
        default=-1,
        choices=[
            (-1, "(Unknown)"),
            (
                1,
                "Communications Officer",
            ),
            (
                2,
                "Teaching Assistant",
            ),
            (
                3,
                "Librarian",
            ),
            (
                4,
                "Nurse",
            ),
            (
                5,
                "Public Safety Officer",
            ),
            (6, "Warehouse Worker"),
            (7, "Maintenance Technician"),
            (8, "Systems Administrator"),
            (9, "Structural Engineer"),
            (10, "Farmer"),
            (11, "Line Cook"),
            (12, "Artist"),
            (13, "Block Captain"),
            (14, "Observatory Technician"),
            (15, "Teacher"),
            (16, "Historian"),
            (17, "Physician Assistant"),
            (18, "Security Officer"),
            (19, "Logistics Specialist"),
            (20, "Electrician"),
            (21, "Software Engineer"),
            (22, "Life Support Engineer"),
            (23, "Field Botanist"),
            (24, "Section Cook"),
            (25, "Author"),
            (26, "Delegate"),
            (27, "Cartographer"),
            (28, "Professor"),
            (29, "Archivist"),
            (30, "Resident Physician"),
            (31, "Tactical Officer"),
            (32, "Warehouse Manager"),
            (33, "EVA Technician"),
            (34, "Embedded Engineer"),
            (35, "Propulsion Engineer"),
            (36, "Nutritionist"),
            (37, "Kitchen Manager"),
            (38, "Musician"),
            (39, "Councilor"),
            (40, "Navigator"),
            (41, "Distinguished Professor"),
            (42, "Curator"),
            (43, "Physician"),
            (44, "Intelligence Officer"),
            (45, "Logistics Manager"),
            (46, "Facilities Supervisor"),
            (47, "Systems Architect"),
            (48, "Reactor Engineer"),
            (49, "Plant Geneticist"),
            (50, "Chef"),
            (51, "Actor"),
            (52, "Justice"),
            (53, "Chief Navigator"),
            (54, "Provost"),
            (55, "Chief Archivist"),
            (56, "Chief Medical Officer"),
            (57, "Head of Security"),
            (58, "Chief Logistics Officer"),
            (59, "Chief Steward"),
            (60, "Chief Technology Officer"),
            (61, "Head of Engineering"),
            (62, "Chief Botanist"),
            (63, "Chief Cook"),
            (64, "Entertainment Director"),
            (65, "High Commander"),
        ],
    )
    outfit = models.IntegerField(verbose_name=_("outfit"), default=-1)
    hair = models.IntegerField(verbose_name=_("hair"), default=-1)
    facial_feature = models.IntegerField(verbose_name=_("facial feature"), default=-1)
    hair_color = models.IntegerField(verbose_name=_("hair color"), default=-1)
    head_piece = models.IntegerField(verbose_name=_("head piece"), default=-1)
    bonus_item = models.IntegerField(verbose_name=_("bonus item"), default=-1)
