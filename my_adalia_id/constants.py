from decimal import Decimal
from typing import Final

MU: Final[Decimal] = Decimal("1.328093024E20")
ASTEROID_ORBITAL_ECCENTRICITY_MINIMUM: Final[Decimal] = Decimal("0.007")
ASTEROID_ORBITAL_ECCENTRICITY_MAXIMUM: Final[Decimal] = Decimal("0.392")

ASTEROID_ORBITAL_INCLINATION_MINIMUM: Final[Decimal] = Decimal("0.01")
ASTEROID_ORBITAL_INCLINATION_MAXIMUM: Final[Decimal] = Decimal("37.77")

ASTEROID_ORBITAL_SEMI_MAJOR_AXIS_MINIMUM: Final[Decimal] = Decimal("0.89")
ASTEROID_ORBITAL_SEMI_MAJOR_AXIS_MAXIMUM: Final[Decimal] = Decimal("3.913")

ASTEROID_SIZE_MINIMUM: Final[int] = 2046
ASTEROID_SIZE_MAXIMUM: Final[int] = 750284


ASTEROID_SPECTRAL_TYPES: Final[dict] = {
    -1: "(Unknown)",
    0: "C",
    1: "CM",
    2: "CI",
    3: "CS",
    4: "CMS",
    5: "CIS",
    6: "S",
    7: "SM",
    8: "SI",
    9: "M",
    10: "I",
}

FOUR: Final[Decimal] = Decimal("4")
THREE: Final[Decimal] = Decimal("3")
TWO: Final[Decimal] = Decimal("2")
ONE_HALF: Final[Decimal] = Decimal("0.5")
PI: Final[Decimal] = Decimal("3.141592653589793238462643383")
METERS_IN_AU: Final[Decimal] = Decimal("1.496E11")
SECONDS_IN_DAY: Final[Decimal] = Decimal("86400")
THIRD_LAW: Final[Decimal] = Decimal(
    "0.000007495"
)  # Value refers to Kepler's third law (R^3 / T^2)
