from django.views.decorators.cache import cache_control

from rest_framework.routers import DefaultRouter

from metauser import hooks
from metauser.common.decorators.urls import decorate_url_patterns

from .viewsets import AsteroidViewSet, CrewmateViewSet

cache_control_private = decorate_url_patterns(cache_control(private=True))


@hooks.register("api")
def api():

    router = DefaultRouter()
    router.register(r"asteroids", AsteroidViewSet, basename="asteroid")
    router.register(r"crewmates", CrewmateViewSet, basename="crewmate")

    return hooks.ApiHook("adalia_id", router)
