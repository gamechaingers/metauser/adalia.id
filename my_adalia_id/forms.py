from django import forms
from django.forms.fields import EmailField
from django.utils.translation import gettext_lazy as _

from crispy_forms.helper import FormHelper
from crispy_forms.layout import ButtonHolder, Div, Layout, Submit


class InterestForm(forms.Form):

    email = EmailField()

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_id = "beta-invite"
        self.helper.form_method = "post"
        self.helper.form_action = "thankyou"
        self.helper.layout = Layout(
            "email",
            Div(
                ButtonHolder(
                    Submit("submit", _("Let me know!"), css_class="btn btn-primary")
                ),
                css_class="float-right",
            ),
        )
