# adalia.id

## Getting Started

Read [CONTRIBUTING.md](./CONTRIBUTING.md) before committing code!

### Required Accounts

- GitLab
- GitPod
- Transifex
- Heroku (for Deploys)

### Required Environment Variables

| Folder         | Scope          | Purpose                 |
| -------------- | -------------- | ----------------------- |
| `GITLAB_TOKEN` | _Project Wide_ | Fetching Artifacts      |
| `TX_TOKEN`     | _i18n updates_ | Transifex Communication |

The GitLab token must have `read_*` roles.

### Important Site Templates

- my_adalia_id >> templates
  - bases >> base.html
- my_adalia_id >> content
  - aup.html
  - cookies.html
  - privacy.html
  - terms.html
  - changelog.html

## Common Tasks

1. Add new page and update URLs
1. Update metauser/metauser/urls/**init**.py and metauser/metauser/views.py

### Starting a new feature

1. `git checkout -b {branch name}`
1. `git push -u origin {branch name}`
1. Click link to start PR

### Adding a Language

1. Look up the language code. Should be of the format language_COUNTRY, for example `en_US`
1. Add new language to `languages` in `Makefile`
1. Add new language to `SUPPORTED_LANGUAGES` in `server/settings/components/internationalization.py`
1. `mkdir -p my_adalia_id/locale/{language code}/LC_MESSAGES`
1. `make messages`
1. `make tx-push`
1. Check Transifex and add new language
1. `make tx-pull`
1. `make compile-messages`

### Run a task

Example:

```
celery -A server.worker call my_adalia_id.nfts.ingest_opensea.tasks.scan_collection_assets --kwargs '{"collection_pk": 1}'
```

### Adding a Library

#### Requirements Files

| Requirements File      | Production         | Local              | Unit Tests         |
| ---------------------- | ------------------ | ------------------ | ------------------ |
| `requirements.in`      | :white_check_mark: | :white_check_mark: | :white_check_mark: |
| `requirements-test.in` | :x:                | :x:                | :white_check_mark: |
| `requirements-dev.in`  | :x:                | :white_check_mark: | :x:                |

#### Instructions

1. Add your new library to the appropriate **`requirements*.in`** file. _DO NOT EDIT `requirements*.txt`_
1. `cd {METAUSER_ROOT}`
1. Run `make requirements`
1. Commit all updates.
