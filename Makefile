apps = authentication coingecko currency diagnostics nfts ingest_opensea wallets
languages = en_US de_DE es_ES fr_FR ja_JP pt_BR zh_CN nl_NL ar_AE en_GB
migration_subs=$(patsubst %, _migrate.%, $(apps))
messages_subs=$(patsubst %, _messages.%, $(languages))
compile_messages_subs=$(patsubst %, _compile-messages.%, $(languages))


migrations: $(migration_subs)
messages: $(messages_subs)
compile-messages: $(compile_messages_subs)

requirements:
	@echo "Updating pinned requirements:"
	@echo -n "Compiling requirements.txt... "
	pip-compile -o requirements.txt  -rqU requirements.in 2>&1
	@echo -n "Compiling requirements-test.txt... "
	pip-compile -o requirements-test.txt  -rqU requirements-test.in requirements.txt
	@echo -n "Compiling requirements-dev.txt... "
	pip-compile -o requirements-dev.txt  -rqU requirements-dev.in requirements.txt
	@echo "Done!"

_migrate.%:
	@echo Migrating $*...
	./manage.py makemigrations $*

_messages.%:
	cd my_adalia_id
	django-admin makemessages -l $* --ignore 'build/*'
	cd ../

_compile-messages.%:
	cd my_adalia_id
	django-admin compilemessages -l $*
	cd ../

tx-push:
	tx push --source

tx-pull:
	tx pull -f

test:
	tox
