#!/usr/bin/env python
"""Django's command-line utility for administrative tasks."""
import os
import sys

sys.path.append(os.path.dirname(os.path.dirname(__file__)))


def main():
    """Run administrative tasks."""
    os.environ.setdefault("DJANGO_SETTINGS_MODULE", "server.settings.local")

    try:
        from django.core.management import call_command
    except ImportError as exc:
        raise ImportError(
            "Couldn't import Django. Are you sure it's installed and "
            "available on your PYTHONPATH environment variable? Did you "
            "forget to activate a virtual environment?"
        ) from exc
    import django

    django.setup()

    call_command("migrate")

    call_command("loaddata", "metauser/metauser/wallets/fixtures/ethereum.yaml")

    call_command("loaddata", "metauser/metauser/nfts/fixtures/influence.yaml")
    call_command("import_asteroids", "ingest/asteroids.jsondump", limit=200, offset=0)

    import random
    import string

    from organizations.models import Organization

    from metauser.authentication.models import User
    from metauser.currency.models import Currency
    from metauser.nfts.models import NFTCollection
    from metauser.wallets.models import Blockchain, Wallet

    from my_adalia_id.models import Asteroid, Crewmate

    # Add currencies
    usd, _ = Currency.objects.get_or_create_fiat(symbol="USD", name="US Dollars")
    eth, _ = Currency.objects.get_or_create_crypto(
        symbol="ETH", name="Ethereum", digits_behind_decimal=18
    )
    sway, _ = Currency.objects.get_or_create_crypto(
        symbol="SWAY", name="Sway", digits_behind_decimal=20
    )

    # Create user
    user, _ = User.objects.get_or_create(
        email="my-adalia-id-test@gamechaingers.io",
        username="nigel",
        defaults={"password": "x"},
        is_staff=True,
        is_superuser=True,
        is_active=True,
    )
    user.set_password("blondefish")  # needed separately to hash the password

    # Set up organization
    organization, _ = Organization.objects.get_or_create(name="First Alliance")
    organization.get_or_add_user(user)
    user.allegiance = organization
    user.save()

    # Add wallet
    ethereum_blockchain, _ = Blockchain.objects.get_or_create(
        symbol="ETH", defaults={"name": "Ethereum"}
    )
    user_wallet, _ = Wallet.objects.get_or_create(
        address="0x6f0613f576cbc92b88bf7e7fba7fa2eb72fda557",
        defaults={"blockchain": ethereum_blockchain},
    )
    user_wallet.owner_user = user
    user_wallet.save()

    # Add asteroids to user
    for i in range(1, 100):
        asteroid = Asteroid.objects.get(id=i)
        asteroid.current_owner = user_wallet
        asteroid.save()

    # Add crewmates to user
    collection = NFTCollection.objects.get(symbol="INFC")
    for i in range(1, 100):
        crewmate, _ = Crewmate.objects.get_or_create(serial=i, collection=collection)

        crewmate.name = "".join(
            random.choices(string.ascii_uppercase + string.digits, k=12)
        )
        crewmate.gender = random.choice(["M", "F"])
        crewmate.body = random.randint(1, 12)
        crewmate.crew_class = random.randint(1, 5)
        crewmate.title = random.randint(1, 65)
        crewmate.outfit = random.randint(1, 10)
        crewmate.hair = random.randint(1, 10)
        crewmate.facial_feature = random.randint(1, 10)
        crewmate.hair_color = random.randint(1, 5)
        crewmate.head_piece = random.randint(1, 5)
        crewmate.bonus_item = random.randint(1, 5)

        crewmate.current_owner = user_wallet
        crewmate.save()

    print("dev_shim finished with setup!")


if __name__ == "__main__":
    main()
