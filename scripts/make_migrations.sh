#!/usr/bin/env bash
if [ ! -f manage.py ]; then
    exit 1
fi
echo "Making migrations..."
echo -n "App: my_adalia_id.currency ... "
./manage.py makemigrations currency 2>&1 >> make_migrations.log && echo "OK" || echo "FAILED"
echo -n "App: my_adalia_id.coingecko ... "
./manage.py makemigrations coingecko 2>&1 >> make_migrations.log && echo "OK" || echo "FAILED"
echo -n "App: my_adalia_id.authentication ... "
./manage.py makemigrations authentication 2>&1 >> make_migrations.log && echo "OK" || echo "FAILED"
echo -n "App: my_adalia_id.diagnostics ... "
./manage.py makemigrations diagnostics 2>&1 >> make_migrations.log && echo "OK" || echo "FAILED"
echo "Done!"
