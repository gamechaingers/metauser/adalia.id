#!/bin/bash
curl --fail -X POST "https://api.cloudflare.com/client/v4/zones/$CF_ZONE_ID/purge_cache" \
     -H "Authorization: Bearer $CF_AUTH_TOKEN" \
     -H "Content-Type: application/json" \
     --data '{"purge_everything":true}' && echo "Cloudflare Purged" || echo "Error Purging Cache"
