CREATE DATABASE my_adalia_id;
CREATE USER my_adalia_id WITH PASSWORD 'my_adalia_id';
ALTER ROLE my_adalia_id SET client_encoding TO 'utf8';
ALTER ROLE my_adalia_id SET timezone TO 'UTC';
GRANT ALL PRIVILEGES ON DATABASE my_adalia_id to my_adalia_id;
