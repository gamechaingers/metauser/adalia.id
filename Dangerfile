
MODULE = "my_adalia_id"

def module_path(module_name)
    return Regexp.compile("^#{MODULE}(\/[a-zA-Z0-9\\-_]+)*\/#{module_name}(\.py|(\/[a-zA-Z0-9\\-_]+)*\/(?!test_)[a-z_]+(?<![_\/]tests)\.py)")
end

has_changelog_changes = !git.modified_files.grep(/^my_adalia_id\/templates\/my_adalia_id\/content\/changelog.html/).empty?

has_codeowners_changes = !git.modified_files.grep(/^.gitlab\/CODEOWNERS$/).empty?

has_pre_commit_changes = !git.modified_files.grep(/^.pre-commit-config.yaml$/).empty?

has_dangerfile_changes = !git.modified_files.grep(/^Dangerfile$/).empty?

has_app_changes = !git.modified_files.grep(/^my_adalia_id(\/[a-zA-Z0-9\\-_]+)*\/(?!test_)[a-z_]+(?<![_\/]tests)\.py/).empty?

has_model_changes = !git.modified_files.grep(module_path("models")).empty?

has_migration_changes = !git.modified_files.grep(module_path("migrations")).empty?

has_serializer_changes = !git.modified_files.grep(module_path("serializers")).empty?

has_viewset_changes = !git.modified_files.grep(module_path("viewsets")).empty?

has_test_changes = !git.modified_files.grep(/^my_adalia_id(\/[a-zA-Z0-9\\-_]+)*\/(test_[a-z_]+|[a-z_]+_tests|tests)\.py/).empty?

has_template_changes = !git.modified_files.grep(/^my_adalia_id(\/[a-zA-Z0-9\\-_]+)*\/templates\//).empty?

has_po_changes = !git.modified_files.grep(/^my_adalia_id\/locale\/([a-zA-Z_-]+)\/LC_MESSAGES\/django.po/).empty?

has_mo_changes = !git.modified_files.grep(/^my_adalia_id\/locale\/([a-zA-Z_-]+)\/LC_MESSAGES\/django.mo/).empty?

has_requirements_input_changes = !git.modified_files.grep(/^requirements(-[a-z_]+)?\.in/).empty?

has_requirements_txt_changes = !git.modified_files.grep(/^requirements(-[a-z_]+)?\.txt/).empty?

# Notifies about model changes
if has_model_changes || has_migration_changes
    message('This MR changes models!')
end

if has_requirements_txt_changes
    message('This MR updates requirements!')
end

if has_requirements_input_changes
    warn('This MR changes requirements!')
end

# Warns about CODEOWNERS changes
if has_codeowners_changes
    warn("This MR has `.gitlab/CODEOWNERS` changes!")
end

# Warns about pre-commit changes
if has_pre_commit_changes
    warn("This MR has `.pre-commit-config.yaml` changes!")
end

# Warns about Dangerfile changes
if has_dangerfile_changes
    warn("This MR has `Dangerfile` changes!")
end

# Requests test update
if has_app_changes && !has_test_changes
    warn("Make sure tests are added/updated if there are code changes that warrant it.")
end

# Requests changelog update
if has_app_changes && !has_changelog_changes
    warn("Ensure any public facing changes are captured in the changelog at: `my_adalia_id/templates/my_adalia_id/content/changelog.html`")
end

# Requests language update
if (has_app_changes or has_template_changes) && !has_po_changes
    warn("If you've made any changes to internationalized strings, ensure you've updated i18n files + Transifex with the procedure documented in the `README.md`")
end

# Ensures language files are compiled
if has_po_changes && !has_mo_changes
    fail("Compile altered language files using `make compile-messages`")
end

# Ensure a clean commits history
if git.commits.any? { |c| c.message =~ /^Merge branch/ }
    fail("Rebase to get rid of the merge commits in this PR")
end

# Ensure requirements are updated
if has_requirements_input_changes && !has_requirements_txt_changes
    fail("Run `make requirements` to ensure the proper requirements are pinned")
end

if has_viewset_changes || has_serializer_changes
    warn("This MR may change the external API")
end


TITLE_REGEX = /^(?<draft>Draft: )?(?<full_prefix>(?<prefix>build|chore|ci|docs|feat|fix|perf|refactor|revert|style|test){1}(?<prefix_args>\([\w\-\.]+\))?(!)?): (?<title>[\w ])+([\s\S]*)/

title = gitlab.mr_title.match(TITLE_REGEX)

if title_prefix = title["prefix"] && title_full_prefix = title["full_prefix"]
    message("This is a `#{title_full_prefix}` MR.")
    if title_prefix == "ci" && gitlab.mr_labels.grep(/^(ci(::)?)/).empty?
        fail("Please add the `ci` label to this MR")
    elsif title_full_prefix == "build" && gitlab.mr_labels.grep(/^(build)$/).empty?
        fail("Please add the `build` label to this MR")
    elsif title_full_prefix == "build(gitpod)" && gitlab.mr_labels.grep(/^(build::gitpod)$/).empty?
        fail("Please add the `build::gitpod` label to this MR")
    elsif title_full_prefix == "chore" && gitlab.mr_labels.grep(/^(chore(::)?)/).empty?
        fail("Please add the `chore` label to this MR")
    elsif title_full_prefix == "chore(deps)" && gitlab.mr_labels.grep(/^(chore::dependencies)$/).empty?
        fail("Please add the `chore::dependencies` label to this MR")
    elsif title_full_prefix == "chore(i18n)" && gitlab.mr_labels.grep(/^(chore::i18n)$/).empty?
        fail("Please add the `chore::i18n` label to this MR")
    elsif title_prefix == "docs" && gitlab.mr_labels.grep(/^(documentation(::)?)/).empty?
        fail("Please add the `documentation` label to this MR")
    elsif title_prefix == "feat" && gitlab.mr_labels.grep(/^(feature(::)?)/).empty?
        fail("Please add the `feature` label to this MR")
    # Catch-all
    elsif gitlab.mr_labels.empty?
        fail("Please add labels to this MR")
    end
else
    fail("Invalid PR title. Please prefix your title with: `build: `, `chore: `, `ci: `, `docs: `, `feat: `, `fix: `, `perf: `, `refactor: `, `revert: `, `style: `, or `test: `.")
end
