#!/bin/bash
set -e
. .gitpod/pypi_creds.sh
git submodule update --init --recursive
cd metauser
PIP_USER=0 pip install --no-cache-dir -r ./requirements.txt  -r ./requirements-dev.txt  -r ./requirements-test.txt
PIP_USER=0 pip install -e .
cd /tmp
redis-server --daemonize yes
cd /workspace/adalia.id
PIP_USER=0 pip install --no-cache-dir -r ./requirements.txt  -r ./requirements-dev.txt  -r ./requirements-test.txt
scripts/dev_shim.py
gp sync-done dev-shim
